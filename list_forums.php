<?php
include("includes/prepend.php");
include("includes/header.php");
$aForumCategories = $oUtil->listForumCategories();
echo $oUtil->getBreadcrumbs();
if ($userid){
    $oUser = new user($userid);
    $strPreviousLogin = $oUser->getPreviousLogin();
}
?>
<div class="page-header">
    <h2>Forums</h2>
</div>

<div class="row-fluid">
    <div class="span9">
        <h3>Forums</h3>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                        <th></th>
                        <th>Forum</th>
                        <th>Last post</th>
                        <th>Topics</th>
                        <th>Posts</th>
                </tr>
            </thead>
        <tbody>
                    <?php
                    foreach($aForumCategories as $aCategory) {
                        $aTopicInfo = $oUtil->getTopicInfo($aCategory['categoryID']);
                        $oLatestTopic = new forum_topic($aTopicInfo['latest_topic_id']);
                        $oLatestPoster = new User($aTopicInfo['latest_poster_id']);
                    ?>
                        <tr>
                                <td>
                                <?php
                                if ($strPreviousLogin < $aTopicInfo['latest_added']){
                                ?>
                                <img src="includes/icons/folder_table.png">
                                <?php }
                                else {
                                ?>                                
                                <img src="includes/icons/folder.png">
                                <?php }?>
                            </td>
                            <td><strong>
                            	<a href="list_topics.php?categoryID=<?=$aCategory['categoryID']?>"><?=$aCategory['category_name']?></a>
                            </td>
                            <td>
                            	<a href="view_topic.php?topicID=<?=$oLatestTopic->topicID?>"><?=$oLatestTopic->topic_name?></a>
                                <div style="float:right;">by <a href="view_user.php?userid=<?=$oLatestPoster->id?>"><?=$oLatestPoster->first_name.' '.$oLatestPoster->last_name?></a> <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aTopicInfo['latest_added'])?>"><?=$oUtil->timeagoFormat($aTopicInfo['latest_added'])?></abbr></div>
                            </td>
                            <td><?=$aTopicInfo['topics']?></td>
                            <td><?=$aTopicInfo['posts']?></td>
                        </tr>
                    <?php }?>
                    
            </tbody>
        </table>
    </div>
    <div class="span3">
        <a href="new_topic.php" class="btn btn-large btn-info" style="display: block; ">New Discussion</a>
        <br>

    </div>
</div>      


<script>
$(document).ready(function(){
    $('#forums').addClass("active");
});
</script>

<?php
include("includes/footer.php");
?>
