<?php
include("includes/prepend.php");
include("includes/header.php");
echo $oUtil->getBreadcrumbs();
?>
<div class="row">
    <div class="span8">
        <div class="page-header">
            <h2>Widget
                <small>by an Author or a Manufacturer</small> <span class="label success">New</span>
            </h2>
        </div>
        <p>A widget is a something something blah blah descriptive.</p>
        

      </div><!-- /span8 -->
      <div class="span4">
        <p><strong>2 vote(s),</strong> 40 point bounty <br>
            <strong>Added:</strong> 1 month and 23 minutes ago<br>
        </p>

        <p><a href="tag.html"><span class="label">art</span></a>
                <span class="label">process</span>
                <span class="label">inspiration</span>
                <span class="label">creativity</span>
        </p>

        <h3>Vote</h3>
        <p>If you want to vote for this request to be added to the library, choose an amount of points to contribute.</p>
        <input class="input-small" id="xlInput" name="xlInput" size="10" type="text" placeholder="Points">
        <button type="submit" class="btn btn-primary">Vote</button>
        <h3>Fulfill</h3>
        <p>If you intend to fulfill this request by donating this item to the library, click on the button and bring the item to our physical location.</p>
        <button type="submit" class="btn btn-primary">Fulfill</button>
      </div><!-- /span4 -->
      </div>

      <?php
include("includes/footer.php");
?>
