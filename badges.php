<?php
include("includes/prepend.php");
include("includes/header.php");
echo $oUtil->getBreadcrumbs();
?>

    <div class="page-header">
        <h2>Badges</h2>
    </div>

    <p>This is a list of all possible user badges. It doesn't explain how to get all of them - the discovery is half the fun.</p>
    
    <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th></th>
        <th>Title</th>
        <th>Description</th>
        <th>Added</th>
        <th><img src="includes/icons/user.png" alt="Users"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/rainbow.png" alt="">
        </td>
        <td><strong>New User Badge</strong>
        </td>
        <td>You already have this one</td>
        <td>23 days, 16 hours ago</td>
        <td>6</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/tux.png" alt="">
        </td>
        <td><strong>Linux Badge</strong>
        </td>
        <td>Log in from a Linux computer</td>
        <td>23 days, 16 hours ago</td>
        <td>6</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/book.png" alt="">
        </td>
        <td><strong>Book Badge</strong>
        </td>
        <td>Borrow a book</td>
        <td>23 days, 16 hours ago</td>
        <td>6</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/cog.png" alt="">
        </td>
        <td><strong>Tool Badge</strong>
        </td>
        <td>Borrow a tool</td>
        <td>23 days, 16 hours ago</td>
        <td>6</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/cake.png" alt="">
        </td>
        <td><strong>Birthday Badge</strong>
        </td>
        <td>Log in on your birthday</td>
        <td>23 days, 16 hours ago</td>
        <td>1</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/cake.png" alt="">
        </td>
        <td><strong>Anniversary Badge</strong>
        </td>
        <td>One year anniversary</td>
        <td>23 days, 16 hours ago</td>
        <td>1</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/comment.png" alt="">
        </td>
        <td><strong>First Comment</strong>
        </td>
        <td>Leave a Comment / Topic Reply</td>
        <td>23 days, 16 hours ago</td>
        <td>1</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/user.png" alt="">
        </td>
        <td><strong>Photogenic</strong>
        </td>
        <td>Upload a photo for your profile</td>
        <td>23 days, 16 hours ago</td>
        <td>1</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/comments.png" alt="">
        </td>
        <td><strong>Comments</strong>
        </td>
        <td>Leave ___ Comments</td>
        <td>23 days, 16 hours ago</td>
        <td>1</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/photo.png" alt="">
        </td>
        <td><strong>Upload a Photo</strong>
        </td>
        <td>Upload a Photo to the Site</td>
        <td>23 days, 16 hours ago</td>
        <td>1</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/picture.png" alt="">
        </td>
        <td><strong>Photo Badge</strong>
        </td>
        <td>Contribute a photo to the site</td>
        <td>23 days, 16 hours ago</td>
        <td>1</td>
      </tr>
      <tr>
        <td>
          <img class="thumbnail" src="includes/icons/tag_blue.png" alt="">
        </td>
        <td><strong>Tag Badge</strong>
        </td>
        <td>Add a tag to something</td>
        <td>23 days, 16 hours ago</td>
        <td>1</td>
      </tr>
    </tbody>
  </table>


<?php
include("includes/footer.php");
?>
