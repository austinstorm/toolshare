<?php
include("includes/prepend.php");
include("includes/header.php");

if($oUser) {
    echo $oUtil->getBreadcrumbs();
?>


<div class="row-fluid">
  <div class="span8">
    <div class="page-header">
        <h2><?=$oUser->first_name?> <?=$oUser->last_name?>
            <?php if ($oUser->isNew()) {?><span class="label label-success">New</span><?php }?>
        </h2>
    </div><!-- /.page-header -->
    <p><strong>Class:</strong>
    <?php 
    if($oUser->is_admin==1) {
        echo "Administrator";
    }
    ?>
    <?php 
    if($oUser->is_admin!=1) {
        echo "User";
    }
    ?>
    </p>
    <p><strong>joined</strong> <abbr class="timeago" title="<?=$oUtil->timeagoFormat($oUser->join)?>"><?=$oUser->join?></abbr>, <strong>last seen</strong> <abbr class="timeago" title="<?=$oUtil->timeagoFormat($oUser->access)?>"><?=$oUser->access?></abbr></p>
    <!-- <p><strong>Invited by:</strong> </p> -->
    <?php 
    if(isset($oUser->fb_id)) {
        ?>
        <p><strong>Facebook:</strong> <a target="_blank" href="http://www.facebook.com/profile.php?id=<?=$oUser->fb_id?>">Facebook profile</a></p>
        <?php
    }
    ?>
    <p><?=Markdown($oUser->bio)?></p>

  </div><!-- /.span8 -->
  <div class="span4">      
      <?php
        $default = $website."/images/anonuser_50px.gif";
        $gravatar = new Gravatar($oUser->email, $default);
        $gravatar->size = 50;
      ?>
    <img class="thumbnail" src="<?php echo $gravatar->getSrc(); ?>" width="265" alt="">
  </div>
</div><!-- /.row-fluid -->

<div class="row-fluid">
  <div class="span12">
    <ul class="nav nav-tabs" id="userFeeds">
      <li class="active"><a href="#activity">Activity</a></li>
    </ul>
     
    <div id="userFeedsContent" class="tab-content">
      <div class="tab-pane active" id="activity">
          <?php
            include 'display_activity_feed.php';
          ?>
      </div>
    </div>
  </div><!-- /.span12 -->
</div><!-- /.row-fluid -->

<script>
$(document).ready(function(){

    $('#userFeeds a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })
    })
</script>

<?php
}
else echo("Invalid ID");
include("includes/footer.php");
?>