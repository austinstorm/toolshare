<?php
include("includes/prepend.php");
include("includes/header.php");
echo $oUtil->getBreadcrumbs();
?>


<form id="user_login" class="form-horizontal" action="authenticate_login.php">
    <div class="alert alert-error" style="display:none"></div>
    <fieldset>
    <legend>User Login</legend>
    
    <div class="control-group">
        <label class="control-label" for="input02">Email Address</label>
        <div class="controls">
        <input name="email" type="text" class="input-large" id="input02">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="input02">Password</label>
        <div class="controls">
        <input name="password" type="password" class="input-large" id="input02">
        </div>
    </div>
        <div class="form-actions">
                <button type="submit" class="btn btn-primary">Login</button>
                <button class="btn">Cancel</button>
        </div>
    </fieldset>
</form>
<?php
include("includes/footer.php");
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#user_login').ajaxForm({
            dataType: 'json',
            success: processResult
        });
        
        function processResult(data) {
            if (data.error){
                addError(data.error);
            }
            if (data.userid) {
                window.location = '/Toolbox';
            }
        }
        
        function addError(error) {    
            $('.alert').show().html(error);
            $(window).scrollTop(0);

        }   
        
    });
</script>
    