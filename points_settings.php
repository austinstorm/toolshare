<?php
include("includes/prepend.php");
include("includes/header.php");
if (!$is_admin){
?>
<div class="page-header">
    <h2>Permissions Error</h2>
</div>
    <p>You must be logged in as an admin to view this page. If you think you have received this message in error, please leave a message in the forums.</p>
<?php 
} else {
    echo $oUtil->getBreadcrumbs();
    $aPointsSettings = $oUtil->getPointsSettings();
?>
    
<div class="page-header">
    <h2>Points Settings</h2>
</div>  

<div class="tab-pane" id="staticpages">
    <form id="points_settings_form">
    <?php
    foreach ($aPointsSettings as $aSetting){
        $strAction = ucfirst($aSetting['action']); ?>
        <div id="<?=$aSetting['action']?>" class="control-group">
            <label class="control-label" for="member"><?=$strAction?></label>
            <div class="controls">
                <div class="btn-group" name="reservations" data-toggle="buttons-radio">                     
                    <a class="btn <?= $aSetting['column_name'] == 'positive_points' ? 'active' : ''?>" value="positive_points" href="#">+</a>
                    <a class="btn <?= $aSetting['column_name'] == 'positive_points' ? '' : 'active'?>" value="negative_points" href="#">-</a>
                </div>
                <input name="point_amount" type="text" class="input-xlarge required point_amount" value="<?=$aSetting['point_amount']?>">
            </div>
        </div>        
      <?php }?>
        <button type="button" id="points_settings_form_submit">Submit</button>
    </form>
</div>

<script type="text/javascript">
$(document).ready(function(){
    
    $('#points_settings_form_submit').click(function(){
        var aData = new Object();
        var i=0;
        $('.control-group').each(function(){
            var action = $(this).attr('id');
            var column = $(this).find('.active').attr('value');
            var point_amount = $(this).find('.point_amount').val();
            aData[i] = {
                "action":action, "column":column, "point_amount":point_amount
            }
            i++;
        });
        $.ajax({
            type: "POST",
            url: 'update_points_settings.php',
            dataType: 'json',
            data: aData,
            success: function(res) {
                if (res.success == 'success'){
                    document.location.reload(true);
                }
            }    
        });
    });    
});
</script>
<?php }
include("includes/footer.php");
?>