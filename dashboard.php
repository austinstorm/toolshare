<?php
include("includes/prepend.php");
include("includes/header.php");
echo $oUtil->getBreadcrumbs();
$aCheckedOut = $oUtil->getSortedCheckedOutBooks();
$aReservations = $oUtil->getReservations();
?>
    <div class="page-header">
        <h2>Admin Dashboard <small> <?=Date("l F d, Y")?></small></h2>
    </div>

<div class="row-fluid">
  <div class="span8">
  <h3>Reservations</h3>
  <table class="table table-condensed table-bordered table-striped">
    <thead>
      <tr>
        <th>Item</th>
        <th>Borrower</th>
        <th>Due Date</th>
      </tr>
    </thead>
    <tbody>
              <tr>
                  <td>...</td>
                  <td>...</td>
                  <td>...</td>
              </tr>
    </tbody>
  </table>
</div>

<div class="row-fluid">
              <div class="span8">
                <h2>Checkouts</h2>
                <h3>Current</h3>
                <table class="table table-condensed table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Item</th>
                      <th>Borrower</th>
                      <th>Due Date</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                      if ($aCheckedOut['aCurrent'] == array()){
                          ?>
                            <tr>
                                <td>...</td>
                                <td>...</td>
                                <td>...</td>
                            </tr>
                      <?php
                      }
                      else {
                          foreach ($aCheckedOut['aCurrent'] as $aItem){
                              $oUser = new User($aItem['userid']);
                              $oItem = new Item($aItem['itemid']);
                              ?>
                            <tr>
                                <td><?=$oItem->itemid?>. <a href="http://www.moscowtoolbox.com/view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a></td>
                                <td><?=$oUser->id?>. <a href="http://www.moscowtoolbox.com/profile/<?=$oUser->id?>"><?=$oUser->first_name.' '.$oUser->last_name?></a></td>
                                <td><?=$aItem['due']?></td>
                            </tr>
                            <?php
                          }
                      }
                      ?>                    
                  </tbody>
                </table>
                <h3>Recently Late</h3>
                <table class="table table-condensed table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Item</th>
                      <th>Borrower</th>
                      <th>Due Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      if ($aCheckedOut['aOverdue'] == array()){
                          ?>
                            <tr>
                                <td>...</td>
                                <td>...</td>
                                <td>...</td>
                            </tr>
                      <?php
                      }
                      else {
                          foreach ($aCheckedOut['aOverdue'] as $aItem){
                              $oUser = new User($aItem['userid']);
                              $oItem = new Item($aItem['itemid']);
                              ?>
                            <tr>
                                <td><?=$oItem->itemid?>. <a href="http://www.moscowtoolbox.com/view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a></td>
                                <td><?=$oUser->id?>. <a href="http://www.moscowtoolbox.com/profile/<?=$oUser->id?>"><?=$oUser->first_name.' '.$oUser->last_name?></a></td>
                                <td><?=$aItem['due']?></td>
                            </tr>
                            <?php
                          }
                      }
                      ?>  
                  </tbody>
                </table>
                <h3>Over 30 Days Late</h3>
                <table class="table table-condensed table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Item</th>
                      <th>Borrower</th>
                      <th>Due Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      if ($aCheckedOut['aLongOverdue'] == array()){
                          ?>
                            <tr>
                                <td>...</td>
                                <td>...</td>
                                <td>...</td>
                            </tr>
                      <?php
                      }
                      else {
                          foreach ($aCheckedOut['aLongOverdue'] as $aItem){
                              $oUser = new User($aItem['userid']);
                              $oItem = new Item($aItem['itemid']);
                              ?>
                            <tr>
                                <td><?=$oItem->itemid?>. <a href="http://www.moscowtoolbox.com/view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a></td>
                                <td><?=$oUser->id?>. <a href="http://www.moscowtoolbox.com/profile/<?=$oUser->id?>"><?=$oUser->first_name.' '.$oUser->last_name?></a></td>
                                <td><?=$aItem['due']?></td>
                            </tr>
                            <?php
                          }
                      }
                      ?>  
                  </tbody>
                </table>
                <table class="table table-condensed table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Item</th>
                      <th>Borrower</th>
                      <th>Reservation Date</th>
                    </tr>
                    <?php
                    foreach($aReservations as $aReservation){
                        $oItem = new item($aReservation['itemid']);
                        $oUser = new user($aReservation['userid']);
                       ?>
                    <tr>
                        <td><a href="view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a></td>
                        <td><a href="view_user.php?id=<?=$oUser->id?>"><?=$oUser->first_name.' '.$oUser->last_name?></a></td>
                        <td><?=$aReservation['reservation_date']?></td>
                    </tr>
                    <?php
                    }?>
                  </thead>
                </table>
              </div>
              <div class="span4">
                <h3>Item Type: Books</h3>
                <div class="row-fluid">
                  <div class="span4 center">
                    <h2>6</h2>
                    <p><small>Monthly checkouts</small></p>
                  </div>
                  <div class="span4 center">
                    <h2>$25</h2>
                    <p><small>Est yearly maintenance</small></p>
                  </div>
                  <div class="span4 center">
                    <h2>$5</h2>
                    <p><small>Outstanding fines</small></p>

                  </div>
                </div>
              </div>
            </div>
<script>
$(document).ready(function(){
    $('#admin').addClass("active");
});
</script>
<?php
include("includes/footer.php");
?>
