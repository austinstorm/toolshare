<?php
include("includes/prepend.php");
include("includes/header.php");
echo $oUtil->getBreadcrumbs();
?>


<form id="user_form" class="form-horizontal" action="add_user.php">
    <div class="alert alert-error" style="display:none"></div>
    <fieldset>
    <legend>Add a User</legend>
    <div class="control-group">
        <label class="control-label" for="input01">First Name</label>
        <div class="controls">
        <input name="first_name" type="text" class="input-medium" id="input01">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="input02">Last Name</label>
        <div class="controls">
        <input name="last_name" type="text" class="input-medium" id="input02">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="input02">Address 1</label>
        <div class="controls">
        <input name="address1" type="text" class="input-large" id="input02">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="input02">Address 2</label>
        <div class="controls">
        <input name="address2" type="text" class="input-large" id="input02">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="input02">Phone</label>
        <div class="controls">
        <input name="phone" type="text" class="input-large" id="input02">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="input02">Email Address</label>
        <div class="controls">
        <input name="email" type="text" class="input-large" id="input02">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="input02">Password</label>
        <div class="controls">
        <input name="password" type="password" class="input-large" id="input02">
        </div>
    </div>
        <div class="form-actions">
                <button type="submit" class="btn btn-primary">Add User</button>
                <button class="btn">Cancel</button>
        </div>
    </fieldset>
    <input type="hidden" name="added" value="NOW()">
    <input type="hidden" name="usertypeid" value="2">
</form>
<?php
include("includes/footer.php");
?>
<script type="text/javascript">
$(document).ready(function() {     
    $('#add').addClass("active");
    
    $('#user_form').ajaxForm({
        beforeSubmit: validate,
        dataType: 'json',
        success: function(res){
            window.location.href = 'view_user.php?userid=' + res.userid
        }
    }); 
    
    function validate(formData, jqForm, options) { 
    // jqForm is a jQuery object which wraps the form DOM element 
    // 
    // To validate, we can access the DOM elements directly and return true 
    // only if the values of both the username and password fields evaluate 
    // to true 
 
    var form = jqForm[0];
    if (!form.first_name.value) {
        addError('Please enter a value for First Name'); 
        return false; 
    } 
    if (!form.last_name.value) { 
        addError('Please enter a value for Last Name'); 
        return false; 
    }
    if (!form.address1.value) { 
        addError('Please enter a value for Address 1'); 
        return false; 
    }
    if (!form.address2.value) { 
        addError('Please enter a value for Address 2'); 
        return false; 
    }
    if (!form.phone.value) { 
        addError('Please enter a value for Phone'); 
        return false; 
    }
    if (!form.email.value) { 
        addError('Please enter a value for Email'); 
        return false; 
    }
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(form.email.value)) {
        addError('Please provide a valid email address');
        return false;
    }

    if (!form.password.value) { 
        addError('Please enter a value for Password'); 
        return false; 
    }
    return true;
    }
});

function addError(error) {    
    $('.alert').show().html(error);
    $(window).scrollTop(0);

}    


</script>