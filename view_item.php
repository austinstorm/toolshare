<?php
include("includes/prepend.php");
include("includes/header.php");
if($oItem) {
$aAuthors = $oItem->setAuthors();
$aTags = $oItem->setTags();
$aOtherFields = $oItem->setOtherFields();
$aComments = $oUtil->getComments(array('itemid' => $oItem->itemid));
$aCheckouts = $oItem->getCheckouts();
$iReservationID = $oItem->getReservationID();
$iCheckoutID = $oItem->getCheckoutID();
if ($iCheckoutID){
    $oCheckout= new Checkout($iCheckoutID);
    $returnDate = $oCheckout->due;
    $reserveDate = date('Y-m-d', strtotime($returnDate . ' + 1 day'));
}
else {
    $reserveDate = date("Y-m-d", strtotime("tomorrow"));
}

$aDays = array($aSettings['sunday'], $aSettings['monday'], $aSettings['tuesday'], $aSettings['wednesday'], $aSettings['thursday'], $aSettings['friday'], $aSettings['saturday']);
$i=0;
$strDisabledDays = '';
foreach($aDays as $iDay){
    if(!$iDay){
        if ($strDisabledDays==''){
            $strDisabledDays .= $i;
        }
        else {
            $strDisabledDays .= ','.$i;
        }
    }
    $i++;
}
echo $oUtil->getBreadcrumbs();
?>

<!-- <div class="alert alert-success">
    <a class="close" data-dismiss="alert">&times;</a>
    <strong>Success!</strong> Item added.
</div> -->
<div class="row-fluid">
  <div class="span8">
    <div class="page-header">
        <h2><?=$oItem->title?>
            <small>
                <?php foreach($aAuthors as $aAuthor){
					if ($aAuthor === reset($aAuthors) && $aAuthor['name']!=='')
						echo 'by ';

                    ?><a href="list_items.php?authorid=<?=$aAuthor['authorid']?>"><?=$aAuthor['name']?></a><?php
					if ($aAuthor !== end($aAuthors))
						echo ', ';
                }?></small>
            <?php if ($oItem->isNew()) {?><span class="label label-success">New</span><?php }?>
            <?php

            ?>
        </h2>
    </div><!-- /.page-header -->
    <p id="description"><?=Markdown($oItem->description)?></p>

    <ul class="nav nav-tabs" id="itemFeeds">
      <li><a href="#activity">Activity</a></li>
      <li class="active <?php if (count($aComments) == 0) echo 'disabled' ?>"><a href="#comments">Comments (<?=count($aComments)?>)</a></li>
      <li class="<?php if (count($aCheckouts) == 0) echo 'disabled' ?>"><a href="#checkouts">Checkouts (<?=count($aCheckouts)?>)</a></li>
    </ul>
     
    <div id="itemFeedsContent" class="tab-content">
      <div class="tab-pane active" id="activity"><br>
        <?php
        include 'display_activity_feed.php';
        ?>
      </div>
      <div class="tab-pane active" id="comments"><br>
        <?php
        include("display_comment_form.php");
        ?>
      </div>
      <div class="tab-pane" id="checkouts">
        <br>

        <?php
            if (count($aCheckouts)){
          ?>

          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>User</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
                <?php 

                foreach ($aCheckouts as $aCheckout){
                    $oCheckoutUser = new user($aCheckout['userid']);
                    if ($oCheckoutUser->publish_checkouts){
                 ?>
                    <tr>
                        <td><?=$aCheckout['checkoutid']?></td>
                        <td><strong><a href="profile.php?id=<?=$oCheckoutUser->id?>"><?=$oCheckoutUser->first_name.' '.$oCheckoutUser->last_name?></a></strong></td>
                        <td><abbr class="timeago" title="<?=$oUtil->timeagoFormat($aCheckout['checkout'])?>"><?=$aCheckout['checkout']?></abbr></td>
                    </tr>

                <?php } 
                    else { ?>
                        <tr>
                            <td><?=$aCheckout['checkoutid']?></td>
                            <td><strong>This user doesn't publish checkouts</strong></td>
                            <td><abbr class="timeago" title="<?=$oUtil->timeagoFormat($aCheckout['checkout'])?>"><?=$aCheckout['checkout']?></abbr></td>
                        </tr>
                    <?php }
                } }
                else {
                    ?>
                    <p class=""><strong>No checkouts yet</strong></p>

                <?php }
                ?>

              
            </tbody>
          </table>
      </div>
    </div>

  </div><!-- /.span8 -->
  <div class="span4">
    <img src="<?php echo (empty($oItem->image) ? 'http://placehold.it/275x400' : $oItem->image);  ?>" width="265" class="thumbnail" alt="<?=$oItem->title?>">
    <br>


    <!-- AddThis Button BEGIN -->
    <div class="addthis_toolbox addthis_default_style ">
    <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
    <a class="addthis_button_tweet"></a>
    <a class="addthis_button_pinterest_pinit"></a>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-5024368674eb542b"></script>
    <!-- AddThis Button END -->

    <?php
    if ($oItem->itemtypeid=='2'){
        ?>
        <p><strong>Publisher:</strong> <a href="list_items.php?publisher=<?=str_replace('&','%26',$aOtherFields['publisher'])?>"><?=$aOtherFields['publisher']?></a>, <?=$aOtherFields['year']?> <br/>
        <strong>Added:</strong> <abbr class="timeago" title="<?=$oUtil->timeagoFormat($oItem->added)?>"><?=$oItem->added?></abbr><br/>
                <strong><?=ucwords($aOtherFields['format'])?>:</strong> <?=$aOtherFields['num_pages']?> pages<br/>
                <strong>ISBN:</strong> <?=$aOtherFields['isbn']?>
        </p>
        <?php
    }
    if ($oItem->itemtypeid=='1'){
        ?>
        <p><strong>Brand:</strong> <a href="list_items.php?manufacturer=<?=str_replace('&','%26',$aOtherFields['manufacturer'])?>"><?=$aOtherFields['manufacturer']?></a> <br/>
        <strong>Added:</strong> <abbr class="timeago" title="<?=$oUtil->timeagoFormat($oItem->added)?>"><?=$oItem->added?></abbr><br/>
                <strong>Contains:</strong> <?=$aOtherFields['parts']?><br/>
                <strong>Model:</strong> <?=$aOtherFields['model_number']?>, <?=ucwords($aOtherFields['size'])?>
        </p>
        <?php
    }
    ?>

    <p>
        <?php foreach($aTags as $aTag){
            echo ( "<a class='label label-info' href='list_items.php?tagid={$aTag['tagid']}'>
                    ".$aTag['tag']."</a> ");            
        }?>
    </p>
    <?php
    if ($sesslife && $aSettings['reservations'] == 'Yes'){
    ?>
    <?php if($iReservationID){
        ?>
        <a class="btn btn-large btn-block btn-primary disabled" role="button">Reserved</a>
    <?php }
    else {
    ?>
      <a class="btn btn-large btn-block btn-primary" href="#reserveModal" role="button" data-toggle="modal">Reserve Item</a>
<?php } ?>
      <!-- Modal -->
      <div class="modal hide fade" id="reserveModal" tabindex="-1" role="dialog" aria-labelledby="reserveModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="reserveModalLabel">Place a Reservation</h3>
        </div>
        <div class="modal-body">
          <p>To reserve "<?=$oItem->title?>", select the date you'd like to pick it up from the available open dates below:</p>
          <form class="form-horizontal">
          <fieldset>
            <div class="control-group">
              <label class="control-label" for="reservation">Reservation date:</label>
              <div class="controls">
                <div class="input-prepend">
                  <span class="add-on"><i class="icon-calendar"></i></span><input type="text" name="reservation" id="reservation">
                </div>
              </div>
            </div>
          </fieldset>
        </form>
        </div>
          <form id="reserve_form">
              <input type="hidden" name="userid" value="<?=$userid?>"/>
              <input type="hidden" name="itemid" value="<?=$oItem->itemid?>"/>
              <input type="hidden" name="reserved" value="NOW()"/>
              <input type="text" name="reservation_date" value="" id="datepicker">
          </form>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
          <button id="reserve_submit" class="btn btn-primary">Reserve Item</button>
        </div>
      </div>
      <?php
        }
      ?>
        <?php
        if ($is_admin)
        {
        ?>
            <div class="adminbox">
                <table class="table table-condensed">
                  <thead>
                    <tr>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><strong>SKU:</strong></td>
                      <td><?=$oItem->itemid?></td>
                    </tr>
                    <tr>
                      <td><strong>Value:</strong></td>
                      <td>$ <?=$oItem->item_value?></td>
                    </tr>
                    <tr>
                      <td><strong>Status:</strong></td>
                      <td><?=$oItem->status?></td>
                    </tr>
                    <tr>
                      <td><strong>Location:</strong></td>
                      <td><?=$oItem->location?></td>
                    </tr>
                  </tbody>
                </table>

                <a class="btn btn-info" href="edit_item.php?itemid=<?=$oItem->itemid?>">Edit</a>
            </div>
        <?php }?>

  </div>
</div><!-- /.row-fluid -->

<script>
$('#itemFeeds a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
})

$(document).ready(function(){
    
    $('#description').each(function() {
        $(this).html(linkHashtags($(this).html()));
    });
    var itemtype = <?=$oItem->itemtypeid?>;
    if (itemtype == '2'){
        $('#books').addClass("active");
    }
    if (itemtype == '1'){
        $('#tools').addClass("active");
    }
    
    $('#reserve_submit').click(function(){
        var data = $('#reserve_form').serialize();
         $.ajax({
                type: "POST",
                url: 'reserve_item.php',
                dataType: 'json',
                data: data,
                success: function(res) {
                    if (res.success == 'success'){
                        $('#reserveModal').modal('hide');
                        
                    }
                }    
            });
    });
    
    var d = '<?=$reserveDate?>';
    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        'startDate' : d,        
        daysOfWeekDisabled : '<?=$strDisabledDays?>'        
    });
});
</script>

<?php
}
else {
    echo "Invalid ID";
}
include("includes/footer.php");
?>