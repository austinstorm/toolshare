<?php
//This page creates a table showing fine details, to be ajaxed into the circulation page.
include ('includes/prepend.php');
$iUserID = $_REQUEST['userid'];
$oUser = new User($iUserID);
$aFineDetails = $oUser->getFineDetails();
?>
<table id="fine_detail_table">
    <tr>
        <td>Type</td>
        <td>Item</td>
        <td>Amount</td>
        <td>Total</td>
        <td>Due Date</td>
        <td>Date</td>
    </tr>

<?php
foreach($aFineDetails as $aFine){
    if ($aFine['itemid']){
        $oItem = new Item($aFine['itemid']);
        $title = $oItem->title;
    }
    else $title = '';
    ?>
    <tr>
        <td><?=ucfirst($aFine['type'])?></td>
        <td><?=$title?></td>
        <td><?='$'.$aFine['amount']?></td>
        <td><?='$'.$aFine['balance']?></td>
        <td><?=$aFine['due']?></td>
        <td><?=$aFine['added']?></td>
    </tr>
    <?php
}
?>
</table>