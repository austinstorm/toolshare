<?php
include("includes/prepend.php");
include("includes/header.php");
if (!$is_admin){
?>
<div class="page-header">
    <h2>Permissions Error</h2>
</div>
    <p>You must be logged in as an admin to view this page. If you think you have received this message in error, please leave a message in the forums.</p>
<?php 
} else {
    echo $oUtil->getBreadcrumbs();
$aSettings = $oUtil->getSettings();
?>
    <div class="page-header">
        <h2>Admin Settings <small>Under the Hood</small></h2>
    </div>

<form id="settings_form" class="form-horizontal">
    <fieldset>

    <ul class="nav nav-tabs" id="settingsTabs">
      <li class="active"><a href="#general">General Settings</a></li>
      <li><a href="#policies">Library Policies</a></li>
      <li><a href="#user">User Registration</a></li>
      <li><a href="#website">Website Options</a></li>
      <li><a href="#forum">Forum Options</a></li>
    </ul>
     
    <div id="settingsTabsContent" class="tab-content">
      <div class="tab-pane active" id="general">
        <br>
        <div class="row-fluid">
            <div class="span8">
                <div class="control-group">
                    <label class="control-label" for="member">Library Title</label>
                    <div class="controls">
                        <input name="title" type="text" class="input-large required" id="title" value="<?=$aSettings['title']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="member">Tagline</label>
                    <div class="controls">
                        <input name="tagline" type="text" class="input-xlarge required" id="tagline" value="<?=$aSettings['tagline']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="member">Address</label>
                    <div class="controls">
                        <textarea rows="3" name="address" class="input-xlarge"><?=$aSettings['address']?></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="member">Hours of Operation</label>
                    <div class="controls">
                        <textarea rows="3" name="hours" class="input-xlarge"><?=$aSettings['hours']?></textarea>
                        <span class="help-block">This is what is displayed on the site cosmetically. You can set days open for purposes of checkout period calculation under the "Library Policies" tab</span>
                    </div>
                </div>

            </div>
            <div class="span4">
                <p>Set your library name, hours, location and other branding here.</p>
            </div>
        </div><!-- /.row-fluid -->
      </div><!-- /#general -->
      <div class="tab-pane" id="policies">
        <br>
        <div class="row-fluid">
            <div class="span8">
                <div class="control-group">
                    <label class="control-label" for="member">Days Open</label>
                    <div class="controls">
                      <div class="btn-group" data-toggle="buttons-checkbox">
                     
                        <a class="btn checkbox-btn <?= $aSettings['sunday'] ? 'active' : ''?>" name="sunday" value="1" href="#">Sun</a>
                        <a class="btn checkbox-btn <?= $aSettings['monday'] ? 'active' : ''?>" name="monday" value="1" href="#">Mon</a>
                        <a class="btn checkbox-btn <?= $aSettings['tuesday'] ? 'active' : ''?>" name="tuesday" value="1" href="#">Tues</i></a>
                        <a class="btn checkbox-btn <?= $aSettings['wednesday'] ? 'active' : ''?>" name="wednesday" value="1" href="#">Weds</i></a>
                        <a class="btn checkbox-btn <?= $aSettings['thursday'] ? 'active' : ''?>" name="thursday" value="1" href="#">Thurs</i></a>
                        <a class="btn checkbox-btn <?= $aSettings['friday'] ? 'active' : ''?>" name="friday" value="1" href="#">Fri</i></a>
                        <a class="btn checkbox-btn <?= $aSettings['saturday'] ? 'active' : ''?>" name="saturday" value="1" href="#">Sat</i></a>
                      </div>
                    <span class="help-block">We assume that members can only return things on days when the library
                        is open, so if you select only certain days the software will automatically round due dates
                        up to the next open day. If you do not want this behavior (if you have a drop-box, for instance),
                         select all days.</span>

                    </div>
                </div><!-- /.control-group -->
                <div class="control-group">
                    <label class="control-label" for="member">Allow Reservations</label>
                    <div class="controls">
                      <div class="btn-group" name="reservations" data-toggle="buttons-radio">
                     
                        <a class="btn <?= $aSettings['reservations'] ? 'active' : ''?>" value="1" href="#">Yes</a>
                        <a class="btn <?= $aSettings['reservations'] ? '' : 'active'?>" value="0" href="#">No</a>
                      </div>
                    <span class="help-block">If you allow reservations, users will be able to put things on hold to
                        pick up later. If not, patrons will only be able to borrow things by physically coming to
                        the library during open hours.</span>

                    </div>
                </div><!-- /.control-group -->
                <div class="control-group">
                    <label class="control-label" for="member">Items Out Limit</label>
                    <div class="controls">
                        <input name="max_items" type="text" class="input-large required" id="title" value="<?=$aSettings['max_items']?>">
                        <span class="help-block">The number of items a single patron is allowed to check out at once. Leave blank for no limit.</span>
                    </div>
                </div><!-- /.control-group -->
                <div class="control-group">
                    <label class="control-label" for="member">Cost Out Limit</label>
                    <div class="controls">
                        <input name="max_value" type="text" class="input-large required" id="title" value="<?=$aSettings['max_value']?>">
                        <span class="help-block">The total cost of the items a single patron is allowed to check out at once. For instance, $500. Leave blank for no limit.</span>
                    </div>
                </div><!-- /.control-group -->
            </div>
            <div class="span4">
                <p>These policies affect how patrons will be able to use your library. All of the settings you choose here
                    should be in some kind of help page or flyer for your patrons.</p>
            </div>
        </div><!-- /.row-fluid -->
      </div>
      <div class="tab-pane" id="user">
        <br>
        <div class="row-fluid">
            <div class="span8">
                <div class="control-group">
                    <label class="control-label" for="member">User Verification</label>
                    <div class="controls">
                      <div class="btn-group" name="user_verification" data-toggle="buttons-radio">
                     
                        <a class="btn <?= $aSettings['user_verification'] == 'Yes' ? 'active' : ''?>" value="1" href="#">Yes</a>
                        <a class="btn <?= $aSettings['user_verification'] == 'No' ? 'active' : ''?>" value="0" href="#">No</a>
                      </div>
                    <span class="help-block">This will require users to click the link sent to verify their email in order to use the site.</span>

                    </div>
                </div><!-- /.control-group -->
                <div class="control-group">
                    <label class="control-label" for="member">Invitation Only</label>
                    <div class="controls">
                      <div class="btn-group" name="reservations" data-toggle="buttons-radio">
                     
                        <a class="btn <?= $aSettings['reservations'] ? 'active' : ''?>" value="1" href="#">Yes</a>
                        <a class="btn <?= $aSettings['reservations'] ? '' : 'active'?>" value="0" href="#">No</a>
                      </div>
                    <span class="help-block">If registration is by invitation only, new users will have to be invited by
                        an existing member. If not, anyone can register (open registration).</span>

                    </div>
                </div><!-- /.control-group -->
                <div class="control-group">
                    <label class="control-label" for="member">Unlimited Invitations</label>
                    <div class="controls">
                      <div class="btn-group" name="reservations" data-toggle="buttons-radio">
                     
                        <a class="btn <?= $aSettings['reservations'] ? 'active' : ''?>" value="1" href="#">Yes</a>
                        <a class="btn <?= $aSettings['reservations'] ? '' : 'active'?>" value="0" href="#">No</a>
                      </div>
                    <span class="help-block">If there are unlimited invitations, any existing member can issue
                        an invitation to a prospective member at any time (similar to the way Pinterest managed their 
                        invitation system. If not, there will be a finite number of invitations.</span>

                    </div>
                </div><!-- /.control-group -->
                <div class="control-group">
                    <label class="control-label" for="member">Issue Invitations</label>
                    <div class="controls">
                          <button class="btn btn-primary" type="button">Issue Invitations</button>
                        <span class="help-block">Press this button to issue every member above 1 an invitation.</span>
                    </div>
                </div><!-- /.control-group -->
            </div>
            <div class="span4">
                <p>These settings affect how users can sign up for your library and site.</p>
            </div>
        </div><!-- /.row-fluid -->
      </div><!-- /#user -->
      <div class="tab-pane" id="website">
        <br>
        <div class="row-fluid">
            <div class="span8">
                <div class="control-group">
                    <label class="control-label" for="member">"New" Time Period</label>
                    <div class="controls">
                      <div class="btn-group" name="new_time" data-toggle="buttons-radio">
                     
                        <a class="btn <?= $aSettings['new_time']== '3 days' ? 'active' : ''?>" href="#">3 days</a>
                        <a class="btn <?= $aSettings['new_time']== '1 week' ? 'active' : ''?>" href="#">1 week</a>
                        <a class="btn <?= $aSettings['new_time']== '2 weeks' ? 'active' : ''?>" href="#">2 weeks</i></a>
                        <a class="btn <?= $aSettings['new_time']== '1 month' ? 'active' : ''?>" href="#">1 month</i></a>
                        <a class="btn <?= $aSettings['new_time']== '2 months' ? 'active' : ''?>" href="#">2 months</i></a>
                      </div>
                    <span class="help-block">How long after an item comes in / user signs up should we display the green "New" tag</span>

                    </div>
                </div><!-- /.control-group -->
                <div class="control-group">
                    <label class="control-label" for="member">"Popular" Designation</label>
                    <div class="controls">
                      <div class="btn-group" name="popular_checkouts" data-toggle="buttons-radio">
                     
                        <a class="btn <?= $aSettings['popular_checkouts']== '3' ? 'active' : ''?>" href="#">3</a>
                        <a class="btn <?= $aSettings['popular_checkouts']== '7' ? 'active' : ''?>" href="#">7</a>
                        <a class="btn <?= $aSettings['popular_checkouts']== '21' ? 'active' : ''?>" href="#">21</i></a>
                        <a class="btn <?= $aSettings['popular_checkouts']== '45' ? 'active' : ''?>" href="#">45</i></a>
                        <a class="btn <?= $aSettings['popular_checkouts']== '60' ? 'active' : ''?>" href="#">60</i></a>
                      </div>
                    <span class="help-block">Number of checkouts in past 6 months to display the "Hot" tag</span>

                    </div>
                </div><!-- /.control-group -->
            </div>
            <div class="span4">
                <p>These settings affect cosmetic things in the website.</p>
            </div>
        </div><!-- /.row-fluid -->
      </div><!-- /#website -->
      <div class="tab-pane" id="forum">
        <br>
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Forum Categories</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                    </td>
                    <td><strong>
                        <a href="list_topics.php?categoryID=1">Category Name</a>
                    </strong></td>
                    <td><a class="btn btn-warning btn-mini"><i class="icon-pencil icon-white"></i> Rename</a> <a class="btn btn-danger btn-mini"><i class="icon-white icon-trash"></i> Delete</a></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td><input type="text" placeholder="New Category Name"></td>
                    <td><a class="btn btn-success btn-mini"><i class="icon-file icon-white"></i> Add Category</a></td>
                </tr>
            </tbody>
            </table>
      </div><!-- /#forum -->
    </div>

        <div class="form-actions">
                <button type="submit" class="btn btn-primary">Save Settings</button>
                <button class="btn" id="clear_form" type="reset">Cancel Changes</button>
        </div>
    </fieldset>
</form>

<?php
}
?>

<script>
$('#settingsTabs a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
})
$(document).ready(function(){
    $('#admin').addClass("active");
    
    $('#settings_form').submit(function(e){
        e.preventDefault();
        var datastring="";
        $('.checkbox-btn').each(function(){
            if ($(this).hasClass('active')){
                var val = 1;
            }
            else var val = 0;
            datastring = datastring + "&" + $(this).attr("name") + "=" + val            
        });
        $('[data-toggle="buttons-radio"]').each(function(){
            datastring = datastring + "&" + $(this).attr("name") + "=" + $(this).children(".active").text();
        });
        var data = $('#settings_form').serialize() + datastring;
        $.ajax({
            type: "POST",
            url: 'update_settings.php',
            dataType: 'json',
            data: data,
            success: function(res) {
                if (res.success == 'success'){
                    document.location.reload(true);
                }
            }    
        });
    });
    
});

</script>

<?php
include("includes/footer.php");
?>
