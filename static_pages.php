<?php
include("includes/prepend.php");
include("includes/header.php");
if (!$is_admin){
?>
<div class="page-header">
    <h2>Permissions Error</h2>
</div>
    <p>You must be logged in as an admin to view this page. If you think you have received this message in error, please leave a message in the forums.</p>
<?php 
} else {
    echo $oUtil->getBreadcrumbs();
    $aStaticPages = $oUtil->listStaticPages();
?>
    
<div class="page-header">
    <h2>Static Pages</h2>
</div>  

<div class="tab-pane" id="staticpages">
        <br>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Tagline</th>
                    <th>Added</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($aStaticPages as $iPosition => $aPage) {
                ?>
                <tr>
                    <td><a href="view_static_page.php?pageID=<?=$aPage['pageID']?>"><strong><?=$aPage['title']?></strong></a>
                    </td>
                    <td><?=$aPage['subtitle']?></td>
                    <td><?=$aPage['added']?></td>
                    <td class="<?=$aPage['pageID']?>" position="<?=$iPosition?>">
                        <button class="btn btn-warning btn-small edit_page" type="button">Edit</button>
                        <button class="btn btn-danger btn-small delete_page" type="button">Delete</button>
                        <?php
                        if ($aPage['published']) {
                            echo '<button class="btn btn-danger btn-small unpublish_page" type="button">Unpublish</button>';
                        }
                        else {
                            echo '<button class="btn btn-success btn-small publish_page" type="button">Publish</button>';
                        }
                        ?>
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <button id ="add_page" class="btn btn-success btn-small" type="button">Add a Page</button>
                    </td>
                </tr>
            </tbody>
        </table>

        <div  class="row-fluid">
            <div id="static_page_form" class="span8" style="display:none">
                <div class="control-group">
                    <label class="control-label" for="member">Page Title</label>
                    <div class="controls">
                        <input name="title" type="text" class="input-large required" id="page_title">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="member">Page Subtitle (optional)</label>
                    <div class="controls">
                        <input name="subtitle" type="text" class="input-xlarge required" id="subtitle">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="member">Page Contents</label>
                    <div class="controls">
                        <textarea name="page_contents" id="page_contents" rows="3" class="input-xlarge"></textarea>
                    </div>
                </div>
                <input type="hidden" name="pageID" value="" id="pageID"/>
                <button class="btn" type="button" id="add_page_submit">Add Page</button>
                <button class="btn" type="button" id="edit_page_submit">Edit Page</button>

            </div>
            <div class="span4">
                <p>Create as many static pages as you want for your site or wahtever.</p>
            </div>
            
        </div><!-- /.row-fluid -->
</div>

<script type="text/javascript">
$(document).ready(function(){
    var aStaticPages = <?=json_encode($aStaticPages)?>
    
    $('#add_page').click(function(){
        $('#static_page_form').show();
        $('#add_page_submit').show();
        $('#edit_page_submit').hide();
    });
    
    $('#add_page_submit').click(function(){
        $.ajax({
            type: "POST",
            url: 'add_static_page.php',
            dataType: 'json',
            data: 
                {
                title: $('#page_title').val(),
                subtitle: $('#subtitle').val(),
                contents: $('#page_contents').val()
                },
            success: function(res) {
                if (res.success == 'success'){
                    document.location.reload(true);
                }
            }    
        });
    });
    
    $('.edit_page').click(function(){
        var iPosition = $(this).parent().attr('position');
        var pageID = $(this).parent().attr('class');
        console.log(aStaticPages);
        
        $('#static_page_form').show();
        $('#add_page_submit').hide();
        $('#edit_page_submit').show();
        $('#pageID').val(pageID);
        $('#page_title').val(aStaticPages[iPosition].title);
        $('#subtitle').val(aStaticPages[iPosition].subtitle);
        $('#page_contents').val(aStaticPages[iPosition].contents);
    });
    
    $('#edit_page_submit').click(function(){
        $.ajax({
            type: "POST",
            url: 'edit_static_page.php',
            dataType: 'json',
            data: 
                {
                title: $('#page_title').val(),
                subtitle: $('#subtitle').val(),
                contents: $('#page_contents').val(),
                pageID: $('#pageID').val()
                },
            success: function(res) {
                if (res.success == 'success'){
                    document.location.reload(true);
                }
            }    
        });
    });
    
    $('.delete_page').click(function(){
        var iPageID = $(this).parent().attr('class');
        if (confirm("Are you sure you want to delete this page?")) {
            $.ajax({
                type: "POST",
                url: 'delete_static_page.php',
                dataType: 'json',
                data: 
                    {
                    pageID: iPageID
                    },
                success: function(res) {
                    if (res.success == 'success'){
                        document.location.reload(true);
                    }
                }    
            });
        }
    });
    $('.publish_page').click(function(){
        var iPageID = $(this).parent().attr('class');
        if (confirm("Are you sure you want to publish this page?")) {
            $.ajax({
                type: "POST",
                url: 'publish_static_page.php',
                dataType: 'json',
                data: 
                    {
                    pageID: iPageID
                    },
                success: function(res) {
                    if (res.success == 'success'){
                        document.location.reload(true);
                    }
                }    
            });
        }
    });
    $('.unpublish_page').click(function(){
        var iPageID = $(this).parent().attr('class');
        if (confirm("Are you sure you want to unpublish this page?")) {
            $.ajax({
                type: "POST",
                url: 'unpublish_static_page.php',
                dataType: 'json',
                data: 
                    {
                    pageID: iPageID
                    },
                success: function(res) {
                    if (res.success == 'success'){
                        document.location.reload(true);
                    }
                }    
            });
        }
    });
});
</script>
<?php }
include("includes/footer.php");
?>

    