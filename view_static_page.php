<?php
include("includes/prepend.php");
include("includes/header.php");
$oPage = new static_page($_REQUEST['pageID']);
echo $oUtil->getBreadcrumbs();
?>

<div class="page-header">
    <h2><?=$oPage->title?> <small><?=$oPage->subtitle?></small></h2>
</div>
<p><?=Markdown($oPage->contents)?></p>

<?php 
include("includes/footer.php");
?>