<?php
//session_start();
if ($_SERVER["HTTP_HOST"] == "localhost") {
    define("HTML_ROOT", "http://localhost/toolbox");
    define("FS_ROOT", "/xampp/htdocs/toolbox");
}
else {
    define("HTML_ROOT", "http://www.moscowtoolbox.com");
    define("FS_ROOT", "/home/austinstorm/public/moscowtoolbox.com/public");
}
    
include(FS_ROOT . '/includes/config.php');

include(FS_ROOT . '/includes/classes/mysqlConnection.class.inc');
include(FS_ROOT . '/includes/classes/utility.class.inc');
include(FS_ROOT . '/includes/classes/db_interface.class.inc');
include(FS_ROOT . '/includes/classes/user.class.inc');
include(FS_ROOT . '/includes/classes/checkout.class.inc');
include(FS_ROOT . '/includes/classes/comment.class.inc');
include(FS_ROOT . '/includes/classes/item.class.inc');
include(FS_ROOT . '/includes/classes/tag.class.inc');
include(FS_ROOT . '/includes/classes/author.class.inc');
include(FS_ROOT . '/includes/classes/reservation.class.inc');
include(FS_ROOT . '/includes/classes/forum_topic.class.inc');
include(FS_ROOT . '/includes/classes/forum_category.class.inc');
include(FS_ROOT . '/includes/classes/static_page.class.inc');
include(FS_ROOT . '/includes/classes/item_type.class.inc');
include(FS_ROOT . '/includes/markdown.php');

$oUtil = new Utility();

//$_REQUEST_RAW = $_REQUEST;
//$_REQUEST = $oUtil->mysql_clean_array($_REQUEST);
//$_POST = $oUtil->mysql_clean_array($_POST);
//$_GET = $oUtil->mysql_clean_array($_GET);

//if (isset($_SESSION['userid']) && (user::isValidID($_SESSION['userid']))) { 
//    $oUser = new user($_SESSION['userid']);   
//}

if (isset($_REQUEST['userid']) && (user::isValidID($_REQUEST['userid']))) { 
    $oUser = new user($_REQUEST['userid']);   
}

//if (strpos($_SERVER['PHP_SELF'], 'admin')!==false) {
//    if ($oUser->aUser['usertype']!=='admin'){
//        header("Location:". HTML_ROOT."/index.php");
//    }
//}

//if (!isset($_SESSION['userid'])){
//    if (strpos($_SERVER['PHP_SELF'], 'account')!==false && strpos($_SERVER['PHP_SELF'], 'log')==false 
//        && strpos($_SERVER['PHP_SELF'], 'new_user')==false && strpos($_SERVER['PHP_SELF'], 'insert_user')==false){   
//        header("Location:". HTML_ROOT."/account/login_page.php");
//    }
//}
//if (isset($_REQUEST['authorid'])) { 
//    $oAuthor = new author($_REQUEST['authorid']);   
//}
//else {
//    $oAuthor = new author();
//}
if (isset($_REQUEST['itemid']) && (item::isValidID($_REQUEST['itemid']))) { 
    $oItem = new item($_REQUEST['itemid']);   
}
//else {
//    $oItem = new item();
//}
//if (isset($_REQUEST['publisherid'])) { 
//    $oPublisher = new publisher($_REQUEST['publisherid']);   
//}
//else {
//    $oPublisher = new publisher();
//}
//if (isset($_REQUEST['tagid'])) { 
//    $oTag = new tag($_REQUEST['tagid']);   
//}
//else {
//    $oTag = new tag();
//}
include(FS_ROOT . '/init.php');

$aSettings = $oUtil->getSettings();
$last_reminders = $aSettings['reminder_emails_sent'];
if ($last_reminders < date('Y-m-d')){    
    $oUtil->sendReminderEmails();
}
?>
