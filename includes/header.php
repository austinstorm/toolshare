<!DOCTYPE html>
<?php 
global $webtitle, $website, $sesslife, $userid, $_setting, $first_name, $last_name, $username, $is_admin;
if ($sesslife){
    $bLoggedIn = true;
}
else $bLoggedIn = false;
$aSettings = $oUtil->getSettings();
?>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<title><?=$aSettings['title']?> - <?=$aSettings['tagline']?></title>
	<meta name="description" content="<?=$aSettings['tagline']?>">
	<meta name="author" content="Austin Storm">

	<!-- Le styles -->
	<link href="<?=HTML_ROOT?>/includes/css/bootstrap.css" rel="stylesheet">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="<?=HTML_ROOT?>/includes/img/toolbox.png">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Carter+One' rel='stylesheet' type='text/css'>

	<link href="<?=HTML_ROOT?>/includes/css/overrides.css" rel="stylesheet">
        <link href="<?=HTML_ROOT?>/includes/css/datepicker.css" rel="stylesheet">
        <script type="text/javascript" src="<?=HTML_ROOT?>/includes/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<?=HTML_ROOT?>/includes/js/jquery.ajaxform.js"></script>
        <script type="text/javascript" src="<?=HTML_ROOT?>/includes/js/timeago.js"></script>
        <script type="text/javascript" src="<?=HTML_ROOT?>/includes/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?=HTML_ROOT?>/includes/js/textext.js.js"></script>
  </head>

  <body>
    <?php if($_setting['enable_facebook'] == 1) { ?>
    <div id="fb-root"></div>
    <script>
    window.fbAsyncInit = function() {
            FB.init({
                    appId      : '<?php echo $_setting['facebook_api']; ?>',
                    channelUrl : '//<?php echo $website; ?>/channel.html',
                    status     : true,
                    cookie     : true,
                    xfbml      : true
            });
    };

    (function(d) {
            var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            d.getElementsByTagName('head')[0].appendChild(js);
    }(document));
    </script>
    <?php } ?>
      
<div id="wrapper">
    <div id="wrapcontainer">
  <div class="container">

	<div class="content">
		<div class="main-header">
			<h1><?=$aSettings['title']?>
				<small><?=$aSettings['tagline']?></small></h1>

		<form class="pull-right" style="margin: 3px 0 0 0;" action="list_items.php">
			<input class="xlarge" type="text" name="search" placeholder="Search">
		</form>

	<ul class="nav nav-pills" style="padding-bottom: 7px;">
        <li id="home"><a href="<?=HTML_ROOT?>/index.php">Home</a></li>
        <li id="books"><a href="<?=HTML_ROOT?>/list_items.php?itemtypeid=2">Books</a></li>
        <li id="tools"><a href="<?=HTML_ROOT?>/list_items.php?itemtypeid=1">Tools</a></li>
        <li id="tools"><a href="<?=HTML_ROOT?>/list_items.php?itemtypeid=9">Costumes</a></li>
        <!-- <li><a href="<?=HTML_ROOT?>/list_requests.php">Requests</a></li> -->
        <li id="forums"><a href="<?=HTML_ROOT?>/list_forums.php">Forums</a></li>

				<ul class="nav nav-pills pull-right">
<?php if($sesslife == false) { ?>
					<li><a href="<?php echo $website."/".USER_DIRECTORY; ?>/login"><?php echo _("Login"); ?></a></li>
					<li><a href="<?php echo $website."/".USER_DIRECTORY; ?>/register"><?php echo _("Register"); ?></a></li>
<?php } else {
/*
displaying gravatar photo over here if email is associated with a gravatar account.
*/
$default = $website."/images/anonuser_50px.gif";
$gravatar = new Gravatar($username, $default);
$gravatar->size = 50;
?>
					<img src="<?php echo $gravatar->getSrc(); ?>" class="profile-photo">
					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $first_name." ".$last_name; ?>&nbsp;<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?=HTML_ROOT?>/view_user.php?userid=<?php echo $userid; ?>">View Profile</a></li>
                            <li><a href="<?=HTML_ROOT?>/invites.php">Invitations</a></li>
							<li><a href="<?php echo $website."/".USER_DIRECTORY; ?>/changepassword"><?php echo _("Change Password"); ?></a></li>
							<li><a href="<?php echo $website."/".USER_DIRECTORY; ?>/editprofile"><?php echo _("Edit Profile & Settings"); ?></a></li>
							<li class="divider"></li>
							<li>
						<?php if(!empty($_SESSION["code"])) { ?>
								<a href="https://www.facebook.com/logout.php?next=<?php echo urlencode($website."/".USER_DIRECTORY."/logout"); ?>&access_token=<?php echo $_SESSION["access_token"]; ?>"><?php echo _("Logout"); ?></a>
						<?php } else { ?>
							<a href="<?php echo $website."/".USER_DIRECTORY; ?>/logout"><?php echo _("Logout"); ?></a>
						<?php } ?>
							</li>
						</ul>
					</li>
<?php } ?>
				</ul>
                    <?php
        if ($is_admin)
        {
        ?>
        <li class="dropdown pull-right" id="admin">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#admin">Admin Tools <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?=HTML_ROOT?>/dashboard.php">Dashboard</a></li>
            <li><a href="<?=HTML_ROOT?>/new_checkout.php">Circulation</a></li>
            <li><a href="<?=HTML_ROOT?>/admin_users.php">Users</a></li>
            <li class="divider"></li>
            <li><a href="<?=HTML_ROOT?>/email_templates.php">Email Templates</a></li>
            <li><a href="<?=HTML_ROOT?>/static_pages.php">Static Pages</a></li>
            <li><a href="<?=HTML_ROOT?>/points_settings.php">Points Settings</a></li>
            <li><a href="<?=HTML_ROOT?>/settings.php">Settings</a></li>            
          </ul>
        </li>
        <li class="dropdown pull-right" id="add">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#add">Add Items <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?=HTML_ROOT?>/new_item.php">Add Item</a></li>
            <li><a href="<?=HTML_ROOT?>/new_user.php">Add User</a></li>
          </ul>
        </li>
        <?php }?>
      </ul>

		</div><!-- /.main-header -->