<?php

/*
 * This is an abstract class used to setup the basic interface between database and php classes.
 */

/*
 * Big To Do:
 * 
 * - Add data cleansing and filtering based on $this->aFields.
 *		We'll need to change that arrow to be multi-dimensional, so the verification can be flexible enough.
 *		Allow type to be enum, which means we need an array of possible values that match the db's values
 *		For strings and integers, just do simple cleansing and intval
 *		For enum, make sure the value is possible
 *		For date strings, make sure they're SQL formatted DATETIME's or DATE's
 * - What about objects with indexes based on multiple fields instead of a single unique id?
 */

abstract class DbInterface extends Utility {

    /*
     * The class name of the item we're dealing with. This serves generically as the name
	 * of the item, and is used in certain places to call up new classes. See especially the
	 * way children and siblings are handled. Technically, it's just a quick way of accessing get_class($this)
     */
    protected $strClass;

    /*
     * The table name in the database corresponding to this item.
     * This can be customized if set in the child class.
     * Otherwise, by default, it's set in the construct to strtolower($this->strClass) . "s"
     *
     */
    protected $tData;
	
	/*
     * The id field in the database for this item.
     * This can be customized if set in the child class.
     * Otherwise, by default, it's set in the construct to $this->strClass . "ID"
     */
    protected $strIDField;

	//TODO: what about objects with fields spread across multiple tables. Are those siblings?
	// build in functionality to handle those cases?
    /*
     * This is 1-D array of fields in this object's db table. Each entry, keyed
	 * to the field name, should be the type of that field.
	 * If cleansing and verification gets more elaborate, this will most likely become
	 * a multi-dimensional array, since we can store certain verification settings here
	 * for each field. For now, though, type will do.
     */
    protected $aFields;

    /*
     * This is the unique id of the currently loaded instance.
     * This should be the same as $this->{$this->strIDField}, so it's a little redundant, but easier to access.
     */
    protected $strID;

    /*
     * This is an array representation of this row from the database.
     * In other words, field values keyed to field name.
     */
    protected $aData;

    /*
     * This is a 2-D array to keep track of any mapping relationships in the db.
	 * Each entry in the array should be an array containing the following possible values
	 *		"table" - *required*. this specifies the table where the mapping takes place
	 *				This may not be the core data table for the mapped object.
	 *				The getMappedObjects function takes that into account.
	 *		"class" - The class that corresponds to the mapped object. 
	 *				This is required when returning objects. The class must also be
	 *				a DbInterface class if you want to pull the default ID field for the mapped object
	 *		"thisIDField" - the unique id field on the mapping table that stores the unique id for this object
	 *				If this is not set, we pull from $this->strIDField. So it's only needed
	 *				if the mapping table uses a unique id field name.
	 *		"mappedIDField" - the unique id field on the mapping table the stores the unique id for the mapped object
	 *				Required if class does not point to a db-interface class.
	 *				If this is not set, and class is set to a db-interface class, we pull the id field
	 *				from the mapped object's strIDField.
     */
    protected $aMappings;
    

	/*
	 * setups up various essential class variables according to what's set in the parent class.
	 * If an array of info is passed to the contructor, it sets up class properties according to that data.
	 * This saves us the step of quering the db in cases where we already have the data.
	 * Otherwise, if pmData is not an array, we assume it's the unique id and pull from the db.
	 */
    public function __construct($pmData){
        parent::__construct();
		
		$strClass = get_class($this);
		$this->strClass = $strClass;
		
		//set up fields and mappings
		//TODO: add a getFieldsFromDB function as a fallback to getFields (use the getfieldsish thing in $this->oData)
		//just set mappings to an empty array if not set
		$this->aFields = $strClass::getFields();
		$this->aMappings = $strClass::getMappings();
		
		//set up id field and data table
        $this->strIDField = defined("$strClass::ID_FIELD") ? $strClass::ID_FIELD : $this->strClass . "ID";
        $this->tData = defined("$strClass::DATA_TABLE") ? $strClass::DATA_TABLE : $this->strClass . "s";
		
		//validate the id passed in $pmData
		$strID = null;
		if( !is_array($pmData) ){
			$strID = $pmData;
		}
		elseif( isset($pmData[$this->strIDField]) ){
			$strID = $pmData[$this->strIDField];
		}
		if( $strID == null || !self::isValidID($strID, $this->strClass) ){
			throw new Exception("Invalid ID ($strID) passed to $this->strClass.");
		}
		
		//if we've made it this far, we're good. Set up the object properties
		$this->setup($pmData);
    }
        
	//if an array is passed for $mID, will check all id's
	public static function isValidID( $mID, $strClass = null ){
		if( $strClass == null || !class_exists($strClass) ){
			$strClass = get_called_class();
		}
		
		if( !defined("$strClass::ID_FIELD") ){
			throw new Exception("validateID() requires an ID_FIELD. None found in $strClass");
		}
		if( !defined("$strClass::DATA_TABLE") ){
			throw new Exception("validateID() requires an DATA_TABLE. None found in $strClass");
		}
		
		$tID = $strClass::ID_FIELD;
		$tData = $strClass::DATA_TABLE;

		if( is_array($mID) ){
			$strID = "'".implode("','",$mID)."'";
			$iValidCount = count($mID);
		}
		else {
			$strID = "'$mID'";
			$iValidCount = 1;
		}

		$strQuery = "SELECT COUNT(*) as count FROM $tData WHERE $tID IN ($strID)";
		
		global $oUtil;
        $aResult = $oUtil->oData->getFirstRow($strQuery);
		
		return $aResult['count'] == $iValidCount;
	}

	/*
	 * returns an array of data for this object, based on its fields in the database.
	 * @return array -- 1-d, value from database, keyed to field name
	 */
	public function getData(){
		return $this->aData;
	}

	/*
	 * returns an array of objects from an array of unique id's. Does everything in a single query to speed things up.
	 * @param aIDs - array of IDs
	 * @param - $strClass - the class we're basing this on. If null, pulls from the called class
	 * @return array - 1-d array of objects, keyed to ID
	 */
	public static function getObjectsFromIDs( array $aIDs, $strClass = null ){
		if( $strClass == null || !class_exists($strClass) ){
			$strClass = get_called_class();
		}
		
		if( !defined("$strClass::ID_FIELD") ){
			throw new Exception("validateID() requires an ID_FIELD. None found in $strClass");
		}
		if( !defined("$strClass::DATA_TABLE") ){
			throw new Exception("validateID() requires an DATA_TABLE. None found in $strClass");
		}
		
		$tID = $strClass::ID_FIELD;
		$tData = $strClass::DATA_TABLE;
		$strIDs = "'" . implode("','",$aIDs) . "'";

		$strQuery = "SELECT * FROM $tData WHERE $tID IN ($strIDs)";

		global $oUtil;
        $aResults = $oUtil->oData->getArray($strQuery);

        $aObjects = array();
        foreach( $aResults as $aResult ){
        	$strID = $aResult[$tID];
        	$aObjects[$strID] = new $strClass($aResult);
        }

        return $aObjects;
	}
	
	/*
	 * gets an array of ids based on a field-value match for any of the fields in the class's fields array
	 * 
	 * @param strField - the field we are running the query against
	 * @param strValue - the value we're looking for
	 * @param strClass - the class we're basing this on. If null, pulls form the called class
	 * @return array - 1-d array of ids matching the field-value search
	 */
	public static function getIDsFromField( $strField, $strValue, $strClass = null ) {
		if( $strClass == null || !class_exists($strClass) ){
			$strClass = get_called_class();
		}
		
		if( !defined("$strClass::ID_FIELD") ){
			throw new Exception("getIdsFromField() requires an ID_FIELD. None found in $strClass");
		}
		if( !defined("$strClass::DATA_TABLE") ){
			throw new Exception("getIdsFromField() requires a DATA_TABLE. None found in $strClass");
		}
		if( !method_exists($strClass, 'getFields') ){
			throw new Exception("getIdsFromField() requires a getFields static function. None found in $strClass");
		}
		
		$tID = $strClass::ID_FIELD;
		$tData = $strClass::DATA_TABLE;
		$aFields = $strClass::getFields();
		
		if( !isset($aFields[$strField]) ){
			throw new Exception("Field $strField not found in $strClass::getFields()");
		}
		
		$strValue = Utility::smartEscape($strValue);
		$strQuery = "SELECT $tID FROM $tData WHERE $strField = '$strValue'";
		
		global $oUtil;
		$aIDs = $oUtil->oData->getArray($strQuery, '', true);
		
		return count($aIDs) ? $aIDs : array();
	}
	
	
	
	public static function search() {
		//pull from Dave's id classes to make this nice and flexible
	}
	
	


    /*
     * This function sets up the classes info properties matching $this->aFields
     *
     * $pmData should either be null, an id, or an array of info for an item from the database.
     * If an id or null, the function will pull $this->aData from the database.
     * If an array, it will set $this->aData to the passed array.
     */
    protected function setup($pmData = null) {
        //if the operator is null, set it to this class's id so we can pull info from the database
        if ($pmData == null) {
            $pmData = $this->strID;
        }
        
        if (is_array($pmData)) {
			//TODO: check if they sent all the info. If not, it's no good, and we'll need to query
            $aData = $pmData;
        }
        else {
			$strSelect = '`'.implode('`, `', array_keys($this->aFields)). '`';
            $strQuery = "SELECT $strSelect
				FROM $this->tData
				WHERE $this->strIDField = {$pmData}";
				
            $aData = $this->oData->getFirstRow($strQuery);
        }
        
        if( !count($aData) )
        	return false;
        $this->aData = $aData;
        $this->strID = $aData[$this->strIDField];
        //setup individual object properties for easy access
		foreach( $aData as $strField => $strValue ){
			$this->$strField = $strValue;
		}
        return true;
    }
	
	/*
	 * returns $this->strClass
	 */
	public function getClass() {
		return $this->strClass;
	}
	
	/*
	 * returns $this->strID
	 */
	public function getID() {
		return $this->strID;
	}
	
	/*
	 * this function can be defined in the parent. It filter's data passed to the insert or update functions.
	 * 
	 * @param paData - the data, keyed to fields in the database
	 * @param strContext - either 'insert' or 'update', so filtering can be modified by context
	 * @return array - 1-D array of filtered data
	 */
	//public function filterData($paData, $strContext){}
	
    /*
     * This function returns a string of SQL code to be used after "SET" in INSERT and UPDATE commands
     * Pass the function an array of info for this item. It will filter according to $this->aFields.
     */
    public static function getSetSql(array $paData, array $aFields) {
		//returns only the data keyed to field names for this ote,
        $aData = array_intersect_key($paData, $aFields);

        $aSet = array();
        foreach ($aData as $key => $value) {
			$strType = $aFields[$key];
			
			switch($strType) {
				case 'float':
					if( $value == "" )
						$value = 0;
					$value = Utility::safeFloat($value);
					$aSet[] = "`{$key}` = {$value}";
					break;
				case 'int':
					if( $value == "" )
						$value = 0;
					$value = Utility::safeInt($value);
					$aSet[] = "`{$key}` = {$value}";
					break;
				case 'datetime':
					//TODO: validate this string as a datetime
					$value = Utility::safeString($value, true);
					$aSet[] = $value == 'NOW()' ? "`{$key}` = {$value}" : "`{$key}` = '{$value}'";
					break;
				case 'text':
					$value = self::cleanseText($value);
					$aSet[] = "`{$key}` = '{$value}'";
					break;
				default:
					$value = Utility::safeString($value, true);
					$aSet[] = "`{$key}` = '{$value}'";
			}
			
            $bFirst = false;
        }
		
		$strSet = implode(', ',$aSet);
        return $strSet;
    }

    /*
     * cleanses text destined for a text field in the database. allows limited html and
     * converts text line breaks to html line breaks.
     */
    //TODO: allow for wysiwyg? when we increase the functionality for this class and allow for settings for each field,
    // it should be a setting there what to allow in text fields.
    public static function cleanseText( $strText ){
    	//first, get rid of all html we don't want.
    	$strText = Utility::safeString($strText, true);

    	//now convert text line breaks to <br />
        //With markdown, this is unnecessary
//    	$strText = nl2br($strText);

    	return $strText;
    }

    public static function insert(array $paData, $strClass = null) {
		if( $strClass == null || !class_exists($strClass) ){
			$strClass = get_called_class();
		}
		
		if( !method_exists($strClass, 'getFields') ){
			throw new Exception('No valid class passed to DbInterface for insert command.
				Either pass a valid DbInterface child class or call from a parent class (e.g. User::insert()).');
		}
		if( !defined("$strClass::DATA_TABLE") ){
			throw new Exception('No valid class passed to DbInterface for insert command.
				Either pass a valid DbInterface child class or call from a parent class (e.g. User::insert()).');
		}
		
		$tData = $strClass::DATA_TABLE;
		$aFields = $strClass::getFields();
		$aData = method_exists($strClass, 'filterData') ? $strClass::filterData($paData, 'insert') : $paData;
		
        $strSet = self::getSetSql($aData, $aFields);
		if( !strlen($strSet) ){
			throw new Exception("No matching fields in passed data for inserting a new $strClass.");
		}
        $strQuery = "INSERT INTO $tData SET $strSet";
            
		//yes, this is sloppy
		global $oUtil;
        if ( $oUtil->oData->query($strQuery) ){
            return $oUtil->oData->getLastID();
        }
        else {
            return false;
        }
    }

    public function update($paData) {
		$strClass = $this->strClass;
		$aData = method_exists($strClass, 'filterData') ? $strClass::filterData($paData, 'update') : $paData;
        
		$strSet = self::getSetSql($aData, $this->aFields);

        $strQuery = "UPDATE {$this->tData} SET {$strSet}
			WHERE `$this->strIDField` = {$this->strID} LIMIT 1";

        if ($this->oData->query($strQuery)) {
			//set object properties to match new info
            return $this->setup();
        }
        else {
            return false;
        }
    }
    
    

    /*
     * Deletes this item from the database.
     *
     * If $bSoft is set to true, it sets the deleted field to '1' (assuming an enum).
     * If false, it deletes the row from the databse.
     */
	//TODO: the $pbSoft assumes to much out of the deleted field. We may want to make it more flexible.
	//TODO: the soft/hard delete default setting should be overrideable by the child class
    public function delete($pbSoft = false) {
        if ($pbSoft == true && isset($this->aFields['deleted'])) {
            $strQuery = "UPDATE {$this->tData} SET deleted = '1'
				WHERE `{$this->strIDField}` = {$this->strID} LIMIT 1";
				
            $this->oData->query($strQuery);
            return $this->setup();
        }
        else {
            $strQuery = "DELETE FROM {$this->tData}
				WHERE `{$this->strIDField}` = {$this->strID} LIMIT 1";
				
            return $this->oData->query($strQuery);
        }
    }
	
	/*
	 * This just returns a 1-D array of unique ids from a mapping table.
	 * 
	 * @param pstrMapping - this is the name of the mapping, matching a key in $this->aMappings
	 * @return array - 1-D numeric array of unique ids
	 */
	public function getMappedIDs( $pstrMapping ){
		if( !in_array($pstrMapping, array_keys($this->aMappings)) ){
			throw new Exception('Requested mapping not set in $this->aMappings.');
		}
		
		$aMapping = $this->aMappings[$pstrMapping];
		
		//confirm we have the mapped id field (it's what we're returning)
		if( !isset($aMapping['mappedIDField']) || !$aMapping['mappedIDField'] ){
			//it's not set, so check if the mapping is connected to a db-interface class
			if( defined("{$aMapping['class']}::ID_FIELD") ){
				$aMapping['mappedIDField'] = $aMapping['class']::ID_FIELD;
			}
			else {
				throw new Exception('mappedIDField not set for requested mapping.');
			}
		}
		
		//call getMappedData and request just the mapped id field
		$aFields = array($aMapping['mappedIDField']);
		$aMappedData = $this->getMappedData($pstrMapping, $aFields);
		
		//simplify the array to 1-d and return
		$aMappedIDs = $aMappedData = parent::getSingleColumn($aMappedData, $aMapping['mappedIDField']);
		return $aMappedIDs;
	}
	
	/*
	 * This grabs an array of mapped objects from a mapping table
	 * 
	 * @param pstrMapping - this is the name of the mapping, atching a key in $this->aMappings
	 * @return array - array of objects, keyed to the unique id of the object
	 */
	//TODO: enable this for non db-interface objects, too
	public function getMappedObjects( $pstrMapping ){
		if( !in_array($pstrMapping, array_keys($this->aMappings)) ){
			throw new Exception('Requested mapping not set in $this->aMappings.');
		}
		
		$aMapping = $this->aMappings[$pstrMapping];
		
		//set $oMapped, if it's a db-interface object. We need it to produce the objects
		if( !isset($aMapping['class']) || !is_subclass_of($aMapping['class'], 'DbInterface') ){
			throw new Exception('Mapping objects requested, but no DbInterface class given in $this->aMappings');
		}
		
		//get the mappedIDField
		if( !isset($aMapping['mappedIDField']) || !$aMapping['mappedIDField'] ){
			$aMapping['mappedIDField'] = $aMapping['class']::ID_FIELD;
		}
		
		//we need all the fields so we can create the objects quickly from the db
		$aFields = array_keys($aMapping['class']::getFields());
		$mDataJoin = false;
		
		//add prefixing if the fields come from another table (if the mapping table is different from the data table)
		if( $aMapping['class']::DATA_TABLE !== $aMapping['table'] ){
			foreach( $aFields as &$strField ){
				$strField = 'd.' . $strField;
			}
			
			if( $aMapping['mappedIDField'] == $aMapping['class']::ID_FIELD ){
				$mDataJoin = $aMapping['class']::DATA_TABLE;
			}
			else {
				$mDataJoin = array(
					'table' => $aMapping['class']::DATA_TABLE,
					'idField' => $aMapping['class']::ID_FIELD
				);
			}
			
		}
		
		//call getMappedData and request all the info for the object->aFields
		$aMappedData = $this->getMappedData($pstrMapping, $aFields, $mDataJoin);
		
		//re-key to unique id and return
		$aMappedObjects = array();
		foreach ($aMappedData as $aResult) {
			$strIdField = $aMapping['class']::ID_FIELD;
			$strID = $aResult[$strIdField];

			//pass the returned data so we don't have to make a separate query for that data
			$aMappedObjects[$strID] = new $aMapping['class']($aResult);
		}
		
		return $aMappedObjects;
	}
	
	
	
	/*
     * This is the core function for grabbing mapping data. Grabs the requested data from the mapping table
	 * as set up in $this->aMappings.
	 * 
	 * @param pstrMapping - this is the name of the mapping, matching a key in $this->aMappings
	 * @param paFields - the fields we want returned. If these fields come from a table aside from
	 *		the mapping table, then use $pmData. If you need to make sure you pull from the correct table
	 *		with these fields (to avoid duplicates), "m" is the prefix for the mapping table
	 *		and "d" is the prefix for the data table.
	 * @param pmData - the data table of the mapped object, if we need to join an additional table to get the data
	 *		The mapping table is "m" in the query, and the data table is "d".
	 *		*Note* If $pmData is set to a string, it's taken as a table name and joined
	 *		on the mapping id field. If the ID field in the mapping table is different,
	 *		set $pmData to an array. 'table' should be the table name, and 'idField' should be the id field.
	 * #param paOrderBy - array of order by statements. 1-d. key should be field, and value should be "ASC" or "DESC"
	 *		
	 * @return array -- 1-D array. if bReturnObjects is false, a numeric array of unique ids.
	 *		If bReturnObjects is true, an associative array of objects keyed to unique id.
     */
	//TODO: add filtering so we can only get the objects we want. Or we can make a separate search function that handles that.
	//TODO: what about mappings based on multiple fields instead of just individual unique id's?
	public function getMappedData($pstrMapping, Array $paFields, $pmData = false, array $paOrderBy = array()) {
        if( !in_array($pstrMapping, array_keys($this->aMappings)) ){
			throw new exception('Requested mapping not set in $this->aMappings.');
		}
		
		$aMapping = $this->aMappings[$pstrMapping];
		
		//verify that we have the table set
		if( !isset($aMapping['table']) ){
			throw new Exception('Requested mapping does not have a table set in $this->aMappings.');
		}
		
		//make we sure we've set this objects id field
		if( !isset($aMapping['thisIDField']) || !$aMapping['thisIDField'] ){
			$aMapping['thisIDField'] = $this->strIDField;
		}
		
		//setup query defaults
		$strSelect = '`'.implode('`, `', $paFields).'`';
		$strJoin = "";
		$strOrderBy = "";
		
		//if we're returning objects, we need to add the correct fields to the query
		if( $pmData ){
			//array needed to join when idfield of the mapped object is different than the id field of the mapping (should be rare)
			if( is_array($pmData) ){
				if( !isset($pmData['table']) || !isset($pmData['idField']) ){
					throw new Exception('Not enough information passed to join data table on mapping table.');
				}
				$strJoin = "INNER JOIN {$pmData['table']} d
					ON d.`{$pmData['idField']}` = m.`{$aMapping['mappedIDField']}`";
			}
			else {
				$strJoin = "INNER JOIN $pmData d USING (`{$aMapping['mappedIDField']}`)";
			}
		}

		//calculate orderby
		if( count($paOrderBy) ){
			$aOrderBy = array();
			foreach( $paOrderBy as $strField => $strOrder ){
				//TODO: add a check to make sure this field is going to be in the query
				//TODO: make sure $strOrder is either asc and desc
				$aOrderBy[] = $strField . " " . $strOrder;
			}
			$strOrderBy = "ORDER BY " . implode(",",$aOrderBy);
		}
        
		$strQuery = "SELECT $strSelect FROM {$aMapping['table']} m $strJoin
			WHERE m.`{$aMapping['thisIDField']}` = '$this->strID' $strOrderBy";
			
		$aResults = $this->oData->getArray($strQuery);
		
		return count($aResults) ? $aResults : array();
    }

    

    /*
     * inserts a row into the sibling index with this object's id field set.
     * The sibling's id field is set to $pstrSiblingID.
     */
	/***
	 * !!WARNING!!
	 * this function is still being evaluated. We may remove it and force this to be 
	 * coded for each individual parent class, so it's best not to rely on for now.
	***/
    public function insertSiblingIndex($pstrSiblingClass, $pstrSiblingID) {
        $strMapIDField = $this->aSiblings[$pstrSiblingClass]['siblingIDField'];
		$strIDField = $this->aSiblings[$pstrSiblingClass]['thisIDField'];
		$strTable = $this->aSiblings[$pstrSiblingClass]['table'];

        $strQuery = "INSERT INTO {$strTable}
            SET {$strIDField} = '{$this->strID}',
            {$strMapIDField} = '{$pstrSiblingID}'";

        return parent::query($strQuery);
    }


	
	
	
	
	
	/**************DEPRECATED FUNCTIONS***************/
	
	/*
     * This function returns the selected children. 
	 * 
	 * @param pstrChildClass - this is the name of the child, matching a key in $this->aChildren
	 * @param bReturnObjects - if true, this function returns an array of child objects instead of just unique id's.
	 * @param pstrOrderby - if needed, this should be a SQL ORDER BY statement.
	 *		No "order by" needed. Just field names and ASC/DESC
	 * @return array -- 1-D array. if bReturnObjects is false, a numeric array of unique ids.
	 *		If bReturnObjects is true, an associative array of child objects keyed to unique id.
     */
	//TODO: add filtering to getChildren and getSiblings so we can only get the children/siblings we want
	//TODO: tweak the bReturnObjects option so that it doesn't require the child/sibling class to be a db_interface parent class.
    //TODO: enable returning of selected fields in a 2-D array if we just need quick data and not the full object structure.
	//		this is important if we the data being returned doesn't have a corresponding object.
	/*public function getChildren($pstrChildClass, $pbReturnObjects = false, $pstrOrderby = null) {
        if( !in_array($pstrChildClass, array_keys($this->aChildren)) ){
			throw new exception('Requested child not set in $this->aChildren.');
		}
		
		$strIDField = $this->aChildren[$pstrChildClass]['thisIDField'];
		$strMapTable = $this->aChildren[$pstrChildClass]['table'];
		$strMapIDField = $this->aChildren[$pstrChildClass]['childIDField'];
		
		$strOrderby = $pstrOrderby !== null ? 'ORDER BY ' . $pstrOrderby : '';
		
		if( $pbReturnObjects ){
			$oChild = new $pstrChildClass();
			$strChildTable = $oChild->getTableName();
			$strChildIDField = $oChild->getIDField();
			
			if( $strMapTable == $strChildTable ){
				$strSelect = implode(', ', $oChild->getFields());
				$strJoin = "";
			}
			//if not a match, grab fields from the child's actual table
			else {
				$strSelect = "c." . implode(', c.', $oChild->getFields());
				$strJoin = "INNER JOIN c.$strChildTable ON c.$strChildIDField = m.$strMapIDField";
			}
			
			
		}
		else {
			$strSelect = $strMapIDField;
			$strJoin = "";
		}
        
		$strQuery = "SELECT $strSelect FROM $strMapTable m $strJoin
			WHERE m.$strIDField = '$this->strID' $strOrderby";
        
		$aChildren = array();
        if ($aResults = $this->oData->getArray($strQuery)) {
			if( !$pbReturnObjects ){
				$aChildren = parent::getSingleColumn($aResults, $strMapIDField);
			}
			else {
				foreach ($aResults as $aResult) {
					$oTempChild = new $pstrChildClass($aResult);
					
					$iChildID = $oTempChild->getID();
					$aChildren[$iChildID] = clone $oTempChild;
				}
			}
        }
		return $aChildren;
    }*/

    /*
     * This function returns the selected siblings.
	 * 
	 * @param pstrSiblingClass - this is the name of the sibling, matching a key in $this->aSiblings
	 * @param bReturnObjects - if true, this function returns an array of sibling objects instead of just unique id's.
	 * @param pstrOrderby - if needed, this should be a SQL ORDER BY statement.
	 *		No "order by" needed. Just field names and ASC/DESC
	 * @return array -- 1-D array. if bReturnObjects is false, a numeric array of unique ids.
	 *		If bReturnObjects is true, an associative array of sibling objects keyed to unique id.
     */
    /*public function getSiblings($pstrSiblingClass, $bReturnObjects = false, $pstrOrderby = null) {
        if( !in_array($pstrSiblingClass, array_keys($this->aSiblings)) ){
			throw new exception('Requested sibling not set in $this->aSiblings.');
		}
		
		$strIDField = $this->aSiblings[$pstrSiblingClass]['thisIDField'];
		$strMapTable = $this->aSiblings[$pstrSiblingClass]['table'];
		$strMapIDField = $this->aSiblings[$pstrSiblingClass]['siblingIDField'];
		
		$strOrderby = $pstrOrderby !== null ? 'ORDER BY ' . $pstrOrderby : '';
		
		if( $bReturnObjects ){
			$oSibling = new $pstrSiblingClass();
			$strSiblingTable = $oSibling->getTableName();
			$strSiblingIDField = $oSibling->getIDField();
			
			if( $strMapTable == $strSiblingTable ){
				$strSelect = implode(', ', $oSibling->getFields());
				$strJoin = "";
			}
			//if not a match, grab fields from the sibling's actual table
			else {
				$strSelect = "c." . implode(', c.', $oSibling->getFields());
				$strJoin = "INNER JOIN c.$strSiblingTable ON c.$strSiblingIDField = m.$strMapIDField";
			}
			
			
		}
		else {
			$strSelect = $strMapIDField;
			$strJoin = "";
		}
        
		$strQuery = "SELECT $strSelect FROM $strMapTable m $strJoin
			WHERE m.$strIDField = '$this->strID' $strOrderby";
        
		$aSiblings = array();
        if ($aResults = $this->oData->getArray($strQuery)) {
			if( !$bReturnObjects ){
				$aSiblings = parent::getSingleColumn($aResults, $strMapIDField);
			}
			else {
				foreach ($aResults as $aResult) {
					$oTempSibling = new $pstrSiblingClass($aResult);
					
					$iSiblingID = $oTempSibling->getID();
					$aSiblings[$iSiblingID] = clone $oTempSibling;
				}
			}
        }
		return $aSiblings;
    }*/
	

}
?>