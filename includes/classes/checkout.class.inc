<?php
class Checkout extends DbInterface {
    const DATA_TABLE = 'checkouts';
    const ID_FIELD = 'checkoutid';
    
    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
    
    public static function getFields() {
		return array(
                    'checkoutid' => 'int',
                    'itemid' => 'int',
                    'userid' => 'int',
                    'checkout' => 'datetime',
                    'checkin' => 'datetime',
                    'due' => 'date',
                    'renews' => 'int'
		);
	}
	
    public static function getMappings() {
            return array(
            );
    }
    
    public function checkIn() {
        global $oUtil;
        //First, calculate fines.
        $oItem = new item($this->itemid);
        $fFine = $oUtil->calculateItemFine($oItem->itemid);
        if ($fFine > 0){
            $sqlInsert = "INSERT INTO fines SET userid = '{$this->userid}', checkoutid = '{$this->checkoutid}', amount = '{$fFine}', type = 'fine', added = NOW()";
            $oUtil->oData->query($sqlInsert);
        }
        $sqlUpdate = "UPDATE checkouts SET checkin = NOW(), renews = '0' WHERE checkoutid = '{$this->checkoutid}'";
        $oUtil->oData->query($sqlUpdate);
        
        $oItem->updateItemStatus('in');       
        
    }
    
    public function renew() {
        global $oUtil;
        $due = $oUtil->getDueDate();
        $iNewRenew = $this->renews + 1;
        $sqlUpdate = "UPDATE checkouts SET renews = {$iNewRenew}, due = '$due' WHERE checkoutid = {$this->checkoutid}";
        $oUtil->oData->query($sqlUpdate);
        $oItem = new item($this->itemid);
    }
}

?>