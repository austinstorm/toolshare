<?php
class item_type extends DbInterface {
    const DATA_TABLE = 'item_types';
    const ID_FIELD = 'ItemTypeID';
    
    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
    
    public static function getFields() {
		return array(
                    'ItemTypeID' => 'int',
                    'Name' => 'varchar',
                    'Description' => 'varchar',
                    'CheckoutPeriod' => 'varchar',
                    'Fine' => 'varchar'
		);
	}
	
    public static function getMappings() {
            return array(
            );
    }
}

?>
