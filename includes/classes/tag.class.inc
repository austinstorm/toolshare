<?php
class tag extends DbInterface {
    
    const DATA_TABLE = 'tags';
    const ID_FIELD = 'tagid';
    public $aBooks;
    
    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
    
    public static function getFields() {
		return array(
			'tagid' => 'int',
			'tag' => 'varchar'
		);
	}
	
	public static function getMappings() {
		return array(
		);
	}
    
    public function addTag ($aInfo) {
		$sqlInsert = "INSERT INTO tags SET tag = '{$aInfo['tag']}'";
		if ($oUtil->oData->query($sqlInsert)) {
			$iTagid = parent::getLastId();
			$this->setup($iTagid);
			return true;
		}
		else {
			return false;
		}	
	}
     public function editTag ($aInfo) {
                $sqlInsert = "UPDATE tags SET tag = '{$aInfo['tag']}' WHERE tagid='{$this->aTag['tagid']}'";
		if (parent::query($sqlInsert)) {
			$iTagid = parent::getLastId();
			$this->setup($iTagid);
			return true;
		}
		else {
			return false;
		}	
     }
    
     public function deleteTag() {
                $sqlDelete = "UPDATE tags SET deleted = CURRENT_TIMESTAMP WHERE tagid='{$this->aTag['tagid']}'";
                if (parent::query($sqlDelete)) {
			return true;
		}
		else {
			return false;
                }
    }
        public function setBooks () {
        $strQuery = "SELECT bookid
            FROM booktag
            WHERE tagid = {$this->aTag['tagid']}";
        
        $aResults = $this->queryAll($strQuery);
        $aBooks = array();
        if (count($aResults) > 0) {
            foreach ($aResults as $aResult) {
                $iBookid = $aResult['bookid'];
                $aBooks[$iBookid] = new book($iBookid);
            }
        }
        
        $this->aBooks = $aBooks;
        return $this->aBooks;
    }
}  
?>
