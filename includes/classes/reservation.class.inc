<?php
class Reservation extends DbInterface {
    const DATA_TABLE = 'reservations';
    const ID_FIELD = 'reservationid';
    
    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
    
    public static function getFields() {
		return array(
                    'reservationid' => 'int',
                    'userid' => 'int',
                    'itemid' => 'int',
                    'reserved' => 'datetime',
                    'reservation_date' => 'date',
                    'met' => 'datetime'
		);
	}
	
    public static function getMappings() {
            return array(
            );
    }
    
    public function meetReservation() {
        global $oUtil;
        $strUpdate = "UPDATE reservations SET met = NOW() WHERE reservationid = $this->reservationid";
        $oUtil->oData->query($strUpdate);
    }
}

?>
