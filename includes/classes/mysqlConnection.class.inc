<?php
/** This class simplifies the use of the MySQL(TM) functions in PHP through the
 * use of a class.
 *
 * This class may not be used except with the express written permission of the
 * author.
 *
 * Author: Jonathan Crapuchettes (jonathan@crapuchettes.com)
 */

class mysqlConnection {
    public $link;
    public $result;
    public $dbConnection;
    public $host;
    public $username;
    public $password;
    public $db;
    public $strQuery;
    public $bHasError = false;

	public $thread;
	private $cachekey = null;
	private $killable = true; // default to allowing this thread to be killed, any call to query/exec will irrevocably set it to false

    public function __construct($host = DB_HOST, $username = DB_USER, $password = DB_PASS, $db = "") {
        // in case the schema has not been set for the module give it the hub
        if( !strlen(trim($db)) ) {
            $db = defined("DB_SCHEMA") ? DB_SCHEMA : HUB_DB;
        }

        $this->link = mysql_connect($host, $username, $password);

        if( $this->link === false ){
            throw new Exception("Database Server Connection Invalid");
        }

        $this->thread = mysql_thread_id($this->link);
        $this->dbConnection = mysql_select_db($db, $this->link) or die("Unable to select database! Database: $db might be invalid");

//        if (!$this->dbConnection){
//            throw new exception("Unable to select database! Database: $db might be invalid");
//        }

        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->db = $db;
		
		/*
		 * php mysql_connect sets characters set to latin1 by default, 
		 * so we need to reset it to utf8. I heard rumors this is supposed 
		 * to be fixed in PHP 6.
		 */
		mysql_set_charset('utf8');

		// kill our old threads (if allowed, ajaxim requests shouldn't be allowed to kill any connections, for instance)
		if( defined('MYSQL_KILLABLE') && MYSQL_KILLABLE ){
			$this->killLoggedThreads();
		}

    } //end constructor

    /********************************************************************************
     * This is needed when an object is serialized
     * @access public
     *******************************************************************************/
    public function __wakeup()
    {
        $this->__construct();
    }

    /********************************************************************************
     * Log this connections thread id with memcache so that we can keep track of it
     * and kill it if necessary
     * @return none
     * @access public
     *******************************************************************************/
//    public function logThreadID()
//    {
//        // we can log only if we have a cache key
//        if( isset($this->cachekey) ){
//
//            // Grab the users thread ids from memcache
//            $aThreadIDs = Cache::key_exists($this->cachekey) ? Cache::get($this->cachekey) : array();
//
//            // Insert our current thread and remove any duplicate/inactive threads
//            $aThreadIDs[] = $this->thread;
//            $aThreadIDs = array_unique(array_intersect($aThreadIDs, $this->getOpenThreadIDs()));
//
//            // Put the new list of threads back in the cache
//            Cache::set($this->cachekey, $aThreadIDs, false, 18000); // 5hr expiration
//        }
//    }

	// add the thread on to the stack of killable threads
	private function addThread()
	{
		// we can log only if we have a cache key
        if( $this->killable && isset($this->cachekey) ){

            // Grab the users thread ids from memcache
            $aThreadIDs = Cache::key_exists($this->cachekey) ? Cache::get($this->cachekey) : array();

            // Insert our current thread
            $aThreadIDs[$this->thread] = 1;

            // Put the new list of threads back in the cache
            Cache::set($this->cachekey, $aThreadIDs, false, 18000); // 5hr expiration
			// TODO: 5hrs may be a bit extreme, we probably want to expire these pretty quickly, maybe 5-15 minutes max?
        }
	}

	// remove the thread from the stack of killable threads
	private function removeThread()
	{
		// we're executing a query that does some sort of INSERT/UPDATE/DELETE
		// don't want to let this thread get killed EVER in case we have multiple
		// such queries to run
		$this->bKillable = false;

		// remove from the killable stack
		$aThreads = Cache::key_exists($this->cachekey) ? Cache::get($this->cachekey) : array();
		if( count($aThreads) && isset($aThreads[$this->iThreadID]) ){
			unset($aThreads[$this->iThreadID]);
			Cache::set($this->cachekey, $aThreads);
		}
	}

    /********************************************************************************
     * Kill all of the users mysql threads (except for the current thread) that were
     * logged with $this->logThreadID()
     * @return none
     * @access public
     *******************************************************************************/
    public function killLoggedThreads()
    {
        // we can log only if we have a cache key
        if( isset($this->cachekey) ){

            // see if we have any values for this key
            if( Cache::key_exists($this->cachekey) ){

                // Loop through the user's cached threads
                $aThreadIDs = Cache::get($this->cachekey);
                foreach($aThreadIDs as $iKey => $iThreadID){

                    // Kill every active thread except the current one
                    if( $iThreadID != $this->thread ){
                        $result = mysql_query('KILL CONNECTION ' . $iThreadID, $this->link);
                        mysql_free_result($result);
                        unset($aThreadIDs[$iKey]);
                    }
                }

                // Cache the leftovers (should just be the current thread if it was logged)
                Cache::set($this->cachekey, array_unique($aThreadIDs), false, 18000); // 5hr expiration
            }
        }
    }

    /********************************************************************************
     * This is an error message
     * @param string query -- This is the SQL query
     * [@param string errOnFail]-- This is an Error message
     * @return mixed -- on success bool true, on failure bool false
     * @access public
     *******************************************************************************/
    public function query($query, $errOnFail = 'Error!') {
        $this->strQuery = $query;
        $this->bHasError = false;
        $this->result = mysql_query($query, $this->link) or ($this->bHasError = true);
        $this->debugSQL($this->strQuery);

            if ($this->result && !$this->bHasError) {
            return true;
        } else {
            $this->throwError($errOnFail);
            //echo($errOnFail . ' Mysql Error:' . $this->getErrorMsg());
            return false;
        }

    } //end query

    /********************************************************************************
     * Allows user to change databases
     * @param string pstrDatabaseName -- This is the name of the Database
     * @access public
     *******************************************************************************/
    public function chooseDB($pstrDatabaseName) {
        mysql_select_db($pstrDatabaseName, $this->link);
    }
    public function lastQuery() {
        return $this->lastStrQuery;
    }


    public function connectionError() {
            return mysql_error($this->link);
    }


    public function errorNum() {
            return mysql_errno($this->link);
    }


    public function getNumberOfRows() {
            return mysql_num_rows($this->result);
    }


    /********************************************************************************
     * This gets a row in the Database
     * @param row -- This is the array you get
     * [@param num] -- This is the Fetch type
     * @return mixed -- on success returns array, on failure return bool false
     * @access public
     *******************************************************************************/
    public function getRow($row, $num = false) {
            if (mysql_num_rows($this->result) && mysql_data_seek($this->result, $row)) {
                    $row = null;
        $mysqlFetchType = null;

        if (is_int($num))
            $mysqlFetchType = $num;
        elseif (!$num)
            $mysqlFetchType = MYSQL_ASSOC;
        else
            $mysqlFetchType = MYSQL_NUM;

        $row = mysql_fetch_array($this->result, $mysqlFetchType);

        return $row;
            } else
                    return false;
    } //end getRow

    /********************************************************************************
     * This gets the next row in the Database
     * [@param num] -- This is the Fetch type
     * @return [] -- on success returns the array
     * @access public
     *******************************************************************************/
    public function getNextRow($num = false) {
            $row = null;
    $mysqlFetchType = null;

    if (is_int($num))
        $mysqlFetchType = $num;
    elseif (!$num)
        $mysqlFetchType = MYSQL_ASSOC;
    else
        $mysqlFetchType = MYSQL_NUM;

    $row = mysql_fetch_array($this->result, $mysqlFetchType);

    return $row;
    } //end getNextRow

    /********************************************************************************
     * This gets the last ID
     * @return int -- This returns the inserted ID
     * @access public
     *******************************************************************************/
    public function getLastId() {
            return mysql_insert_id($this->link);
    }


    /********************************************************************************
     * This gets the number of fields in the Database
     * @return int -- This returns the number of fields
     * @access public
     *******************************************************************************/
    public function getNumberOfFields() {
            return mysql_num_fields($this->result);
    }

    /********************************************************************************
     * This gets the Field names in the Database
     * @param table --This is the table in the Database
     * @param db -- This is the Database that the Field name is in
     * @return [] -- This is the field names
     * @access public
     *******************************************************************************/
    public function getFieldNames($table, $db) {
		$this->query("SHOW COLUMNS FROM $db.$table");
		if( mysql_num_rows($this->result) > 0 ){
			while( $row = mysql_fetch_assoc($this->result) ) {
				$names[] = $row['Field'];
			}
		}
		return $names;
    } //end getFieldNames

    /********************************************************************************
     * Gets the number of affected rows in the previous MySQL operation
     * @return int
     * @access public
     ********************************************************************************/
    public function numOfAffectedRows() {
            return mysql_affected_rows($this->link);
    }

    /********************************************************************************
     * This moves data to a row
     * @param row -- this is the row the data is placed in
     * @return bool
     * @access public
     *******************************************************************************/
    public function moveToRow($row) {
            if (mysql_num_rows($this->result) && mysql_data_seek($this->result, $row)) {
                    return true;
            } else {
                    return false;
            }
    } //end moveToRow


    /********************************************************************************
     * This gives you the result
     * [@param num] -- This is the Fetch type
     * @return [] -- This return the array
     * @access public
     *******************************************************************************/
public function &getResultSet($num = false) {
    $array = array();
    $mysqlFetchType = null;

    if (is_int($num))
        $mysqlFetchType = $num;
    elseif (!$num)
        $mysqlFetchType = MYSQL_ASSOC;
    else
        $mysqlFetchType = MYSQL_NUM;

    for ($i = 0; $i < mysql_num_rows($this->result); $i++) {
        $array[] = mysql_fetch_array($this->result, $mysqlFetchType);
    } //end for loop

    return $array;

} //end getResultSet

/********************************************************************************
 * This Gets the field in the Database
 * @param string pstrQuery -- This is the query that the field is in
 * [@param string pstrErr] -- This is and Error message
 * [@param column] -- This is the column number
 * [@param row] -- This is the row number
 * @return [] -- This returns an array of a row and column
 * @access public
 *******************************************************************************/
public function getField($pstrQuery, $pstrErr = 'Error!', $column = 0, $row = 0) {
    $this->query($pstrQuery, $pstrErr);
    if (mysql_num_rows($this->result))
        return mysql_result($this->result, $row, $column);
} //end getField


//Alias of getField
public function queryone($pstrQuery){
    return $this->getField($pstrQuery);
}

/*
 * Note for Jonathan Crapuchettes *
 * This function was recived from James Hill at CCBenefits, Inc. with
 * changes made by Jonathan Crapuchettes for faster execution.
 * The second parameter is kept for backwards compatability with their code.
 */
/*
public function getArray($query, $errOnFail = 'Error!', $isOneD = false, $column = 0){
    $this->strQuery = $query;
    $this->result = mysql_query($query, $this->link);
    $this->debugSQL($this->strQuery);

    if(!$this->result) {
        return false;
    } else {

        if ($isOneD) {    //If they only want a 1D array
            $aRes = array();

            for ($i = 0; $i < mysql_num_rows($this->result); $i++) {
                $aReturn[] = mysql_result($this->result, $i, $column);
            } //end for loop

            return $aReturn;
        } else {
            return $this->getResultSet();
        }
    }

} //end getArray
*/

/********************************************************************************
 * This gets an Array
 * @param query -- This is the SQL query
 * [@param errOnFail] -- This is an Error message
 * [@param isOneD] -- This is a one dimensional array
 * [@param debug] -- This is a debugger
 * @return mixed -- on success returns an Array, on failure returns bool Error
 * @access public
 *******************************************************************************/
    public function getArray($query, $errOnFail = 'Error!', $isOneD = false, $debug = false){
		$this->strQuery = $query;
		$this->bHasError = false;
		$this->result = mysql_query($query, $this->link) or ($this->bHasError = true);

        if(!$this->bHasError){
			if($isOneD){        //If they only want a 1D array
				$aRes = array();
				for ($i = 0; $i < mysql_num_rows($this->result); $i++) {
					$aRes[] = mysql_fetch_array($this->result, MYSQL_ASSOC);
				} //end for loop

				$aReturn = array();
				for($i = 0; $i < count($aRes); $i++){
					$aReturn[$i] = array_shift($aRes[$i]);//Pop the first element off the front
				}

				return $aReturn;
			}
			else{
				return $this->getResultSet();
			}
        }
        else{
			$this->throwError($errOnFail);
			//echo($errOnFail . ' Mysql Error:' . $this->getErrorMsg());
			return false;
        }

    }
	
/********************************************************************************
 * This gets the first row as an associative array
 * @param query -- This is the SQL query
 * @return mixed -- on success returns an Array, on failure returns bool Error
 * @access public
 *******************************************************************************/
    public function getFirstRow($query){
        $aData = $this->getArray($query);

        //Just pull the first row
        if(count($aData)){
            return $aData[0];
        }
        else{
            return $aData;
        }
    }

/**
 * Get the result of a query back as a vector keyed to the first column
 * returned by the query.
 *
 * @param string $sQuery SQL query to be executed
 * @param string $errOnFail Error string
 * @return array 1D array with values from second field, keyed to the first.
 */
    public function getVector($sQuery, $errOnFail = 'Error!')
    {
        $this->sQuery = $sQuery;
        $this->bHasError = false;
        $this->result = mysql_query($sQuery, $this->link) or ($this->bHasError = true);

        $aVector = array();
        if (!$this->bHasError)
        {
            $iRows = mysql_num_rows($this->result);
            for ($i = 0; $i < $iRows; ++$i)
            {
                $aRow = mysql_fetch_array($this->result, MYSQL_NUM);
                $aVector[$aRow[0]] = $aRow[1];
            }

            if (count($aVector) != $iRows)
            {
                $this->throwError($errOnFail);
                return false;
            }
        }
        else
        {
            $this->throwError($errOnFail);
            return false;
        }

        return $aVector;
    }


/********************************************************************************
 * This gets an array of Objects
 * @param query -- This is the SQL query
 * [@param $strClassName
 * [@param errOnFail] -- This is an Error message

 * [@param debug] -- This is a debugger
 * @return mixed -- on success returns an Array, on failure returns bool Error
 * @access public
 *******************************************************************************/
    public function getObjects($query, $className = 'stdClass', $errOnFail = 'Error!', $debug = false){
        $this->strQuery = $query;
        $this->bHasError = false;
        $this->result = mysql_query($query, $this->link) or ($this->bHasError = true);

        if(!$this->bHasError)
        {
            $array = array();
            $iNumRows = mysql_num_rows($this->result);
            for ($i = 0; $i < $iNumRows; $i++) {
                $array[] = mysql_fetch_object($this->result, $className);
            } //end for loop
            return $array;

        } else {
            $this->throwError($errOnFail);
            //echo($errOnFail . ' Mysql Error:' . $this->getErrorMsg());
            return false;
        }

    }



    //Alias of getArray, except makes all the return columns lower case
    public function queryall($pstrQuery){
        $aData = $this->getArray($pstrQuery);

        if(count($aData)){
            $aFinal = array();
            foreach($aData as $iKey => $aRow){
                $aFinal[$iKey] = array_change_key_case($aRow, CASE_LOWER);
            }
            return $aFinal;
        }
        else{
            return $aData;
        }
    }

    //Returns a single row of data
    public function queryrow($pstrQuery){
        $aData = $this->getArray($pstrQuery);

        //Just pull the first row
        if(count($aData)){
            $aData = $aData[0];
            return array_change_key_case($aData, CASE_LOWER);
        }
        else{
            return $aData;
        }
    }

  /*****************************************************************************************
   * This will reopen the Database
   * @return bool -- on success of reconnection returns bool true, on Failure returns bool false
   * @access public
   ****************************************************************************************/
    public function reopen() {
        if (!isset($this->link)) {
            $this->link = mysql_connect($this->host, $this->username, $this->password);
            $this->dbConnection = mysql_select_db($this->db, $this->link);
            return true;
        } else
            return false;
    } //end open function

	/*********************************************************************************************
	 * This will close the connection to the Database
	 * @return bool -- on success of exiting the Database return bool true, on failure return bool false
	 * @access public
	 ********************************************************************************************/
    public function close() {
        if (isset($this->link)) {
            mysql_close($this->link);
            unset($this->link);
            return true;
        } else
            return false;
    } //end close function


    /********************************************************************************
     * This throws an error on Failure
     * @param errOnFail -- This is an error Message
     * @access private
     *******************************************************************************/
    private function throwError($errOnFail) {

        $strErr = ("\n***********************************************\n");
        $strErr .= ("MySQL Error: \n\n");
        $strErr .= ("SQL Error: " . $this->getErrorMsg()  . "\n\n");
        $strErr .= ("Custom Error: " . $errOnFail . "\n\n");
        $strErr .= ("Query: " . $this->strQuery  ."\n");
        $strErr .= ("***********************************************\n");
        
	throw new Exception($strErr, $this->errorNum());

    }
    /********************************************************************************
     * This gets the Error message
     * @return string -- This returns the SQL message
     * @access public
     *******************************************************************************/
    public function getErrorMsg() {
        return $this->connectionError();
    }

    /* Function added by Scott Hieronymus for use with EMSI products */
    /********************************************************************************
     * This is a String debugger
     * @param string pstrQuery -- This is the query the debugger is in
     * access private
     ********************************************************************************/
    private function debugSQL($pstrQuery) {
    	//make sure we are supposed to spit out queries
    	if (isset($_SESSION['DEBUG']['SHOW_SQL']) && $_SESSION['DEBUG']['SHOW_SQL'] == 1) {
	    	$strDebug = "MySQL Query:\n";
	    	$strDebug .= $pstrQuery;
	    	$oUtil = new Utility();
	    	$oUtil->debugMsg($strDebug);
    	}
    }

    /********************************************************************************
     * Gets an array of the open mysql threads
     * @return [] -- Array of thread ids
     * @access private
     *******************************************************************************/
    private function getOpenThreadIDs()
    {
        $result = mysql_list_processes($this->link);
        $aIDs = array();
        while( $row = mysql_fetch_assoc($result) ){
            $aIDs[] = $row['Id'];
        }
        mysql_free_result($result);
        return $aIDs;
    }

} //end mysqlConnection class

class mysqlConnectionException extends exception {

    public function __construct($message, $code = 0) {
        parent::__construct($message, $code);
    }

} //end exception mysqlConnectionException
?>