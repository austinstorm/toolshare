<?php
/*This class was written by Jonathan Crapuchettes. As of 2009, he was over at http://www.economicmodeling.com/*/
class database {
	private $link;
	private $result;
	private $dbConnection;
    private $host;
    private $username;
    private $password;
    private $db;

	public function __construct($host = DBhost, $username = DBusername, $password = DBpassword, $db = DBname) {
		$this->link = mysql_connect($host, $username, $password);
		$this->dbConnection = mysql_select_db($db);
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->db = $db;
	}

	function query($query) {
        
        $this->result = mysql_query($query);
		if ($this->result) {
			return true;
		} else {
			//echo $query . "<br /><br />";
			return false;
		}
	}
	
	//This should be more elaborate.
	function mysql_clean ($data) {
		$data = mysql_real_escape_string($data);
		return $data;
	}
	
	//Supports cleaning up to three arrays deep. Ignores any array with the "nondb" key.
	function mysql_clean_array ($arr) {
		foreach ($arr as $key => $val) {
			if ($key != "nondb") {
				if (is_array($val)) {
					foreach ($val as $key2 => $val2) {
						if (is_array($val2)) {
							foreach ($val2 as $key3 => $val3) {
								$arr[$key][$key2][$key3] = $this->mysql_clean($val3);
							}
						}
						else {
							$arr[$key][$key2] = $this->mysql_clean($val2);
                                                }
					}
				}
				else {
					$arr[$key] = $this->mysql_clean($val);
                                }
			}
		}
		return $arr;
	}

	function connectionError() {
		return mysql_error();
	}

	function errorNum() {
		return mysql_errno();
	}

	function getNumberOfRows() {
		return mysql_num_rows($this->result);
	}

	function getRow($row, $num = false) {
		if (mysql_data_seek($this->result, $row)) {
			$row = null;
			if (!$num)
				$row = mysql_fetch_array($this->result, MYSQL_ASSOC);
			else
				$row = mysql_fetch_array($this->result, MYSQL_NUM);
			return $row;
		} else
			return false;
	}

	function getNextRow($num = false) {
		$row = null;
		if (!$num) {
			$row = mysql_fetch_array($this->result, MYSQL_ASSOC);
		} else {
			$row = mysql_fetch_array($this->result, MYSQL_NUM);
		}
		return $row;
	}

	function getLastId() {
		return mysql_insert_id();
	}

	function getNumberOfFields() {
		return mysql_num_fields($this->result);
	}

	function getFieldNames($table, $db = "Students") {
		$this->result = mysql_list_fields($db, $table, $this->link);
		for ($i = 0; $i < mysql_num_fields($this->result); $i ++) {
			$names[] = mysql_field_name($this->result, $i);
		}
		return $names;
	}

	function numOfAffectedRows() {
		return mysql_affected_rows();
	}

	function moveToRow($row) {
		if (mysql_data_seek($this->result, $row)) {
			return true;
		} else {
			return false;
		}
	}
    
    function reopen() {
        if (!isset($this->link)) {
            $this->link = mysql_connect($this->host, $this->username, $this->password);
            $this->dbConnection = mysql_select_db($this->db, $this->link);
            return true;
        } else
            return false;
    } //end open function

	function close() {
        if (isset($this->link)) {
            mysql_close($this->link);
            unset($this->link);
            return true;
        } else
            return false;
	} //end close function
    
    function getResultSet($num = false) {
        $array = array();
        
        for ($i = 0; $i < mysql_num_rows($this->result); $i++) {
            if (!$num)
                $array[] = mysql_fetch_array($this->result, MYSQL_ASSOC);
            else
                $array[] = mysql_fetch_array($this->result, MYSQL_NUM);
        } //end for loop
        
        return $array;
        
    } //end getResultSet
    
    
    function getField($column = 0, $row = 0) {
        return mysql_result($this->result, $row, $column);
    } //end getRowSingle

} //end mysqlConnection class

?>