<?php
class forum_topic extends DbInterface {
    const DATA_TABLE = 'forum_topics';
    const ID_FIELD = 'topicID';
    public $aTags;
    
    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
    
    public static function getFields() {
		return array(
                    'topicID' => 'int',
                    'categoryID' => 'int',
                    'topic_name' => 'varchar',
                    'added' => 'datetime'
		);
	}
	
    public static function getMappings() {
            return array(
            );
    }
    
    public function addTagTopicIndex($aInfo) {
        $sqlInsert = "INSERT INTO tag_topic_index SET topicID= '{$aInfo['topicID']}', tagid= '{$aInfo['tagid']}'";
        global $oUtil;
        if ($oUtil->oData->query($sqlInsert)) {
			return true;
		}
		else {
			return false;
		}
    }
    
    public function getDistinctCommenters() {
        global $oUtil;
        $sqlSelect = "SELECT added, userid FROM 
            (SELECT * FROM comments ORDER BY added DESC) as comments 
            WHERE assignedtoid = '$this->topicID' AND assignedtotable = 'post' GROUP BY userid ORDER BY added DESC";
        $aResults = $oUtil->oData->queryall($sqlSelect);       
        return $aResults;
        
    }
    
    public function setTags(){
        global $oUtil;
        $strQuery = "SELECT tagid
            FROM tag_topic_index
            WHERE topicID = {$this->topicID}";
        
        $aResults = $oUtil->oData->getArray($strQuery);
        $aTags = array();
        if (count($aResults) > 0) {
            foreach ($aResults as $aResult) {
                $iTagid = $aResult['tagid'];
                $oTag = new tag($iTagid);
                $aTags[] = $oTag->aData;
            }
        }
        
        $this->aTags = $aTags;
        return $this->aTags;
    }
}

?>