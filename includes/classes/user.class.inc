<?php
class User extends DbInterface {
    
    const DATA_TABLE = 'members';
    const ID_FIELD = 'id';

    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
    
    public static function getFields() {
		return array(
                    'id' => 'int',
                    'first_name' => 'varchar',
                    'last_name' => 'varchar',
                    'address1' => 'varchar',
                    'address2' => 'varchar',
                    'phone' => 'varchar',
                    'email' => 'varchar',
                    'password' => 'varchar',
                    'bio' => 'longtext',
                    'key' => 'varchar',
                    'verified' => 'tinyint',
                    'join' => 'datetime',
                    'access' => 'datetime',
                    'fb_id' => 'bigint',
                    'is_admin' => 'tinyint',
                    'login_attempt' => 'tinyint',
                    'banned' => 'tinyint',
                    'pass_reset_key' => 'varchar',
                    'expires_at' => 'int',
                    'publish_checkouts' => 'tinyint',
                    'publish_contributions' => 'tinyint',
                    'photo_id_type' => 'varchar',
                    'id_number' => 'varchar',
                    'address_proof' => 'varchar',
                    'agreement_signed' => 'tinyint'
		);
	}
	
	public static function getMappings() {
		return array(
		);
	}

    public function addUser($aInfo){
		$sqlUserInsert = "INSERT INTO members SET first_name = '{$aInfo['first_name']}', last_name = '{$aInfo['last_name']}', address1 = '{$aInfo['address1']}', address2 = '{$aInfo['address2']}', phone = '{$aInfo['phone']}', email = '{$aInfo['email']}', password = '{$aInfo['password']}', added = CURRENT_TIMESTAMP";
        if (parent::query($sqlUserInsert)) {
			return true;
		}
		else {
			return false;
		}

    }
    
    public function getCheckouts() {
        global $oUtil;
        $strQuery = "SELECT * FROM checkouts WHERE userid = $this->id and checkin = '0000-00-00 00:00:00'";
        $aCheckouts = $oUtil->oData->getArray($strQuery);
        return $aCheckouts;
    }
    
    public function getCheckoutsAndItems() {
        global $oUtil;
        $strQuery = "SELECT * FROM checkouts c INNER JOIN items i ON c.itemid = i.itemid WHERE userid = $this->id and checkin = '0000-00-00 00:00:00'";
        $aCheckouts = $oUtil->oData->getArray($strQuery);
        return $aCheckouts;
    }
    
    public function isNew() {
        global $oUtil;
        $aSettings = $oUtil->getSettings();
        $strNewTime = $aSettings['new_time'];
        $strNewUntilDate = date('Y-m-d',strtotime($this->join.'+'.$strNewTime));
        if (date('Y-m-d') < $strNewUntilDate){
            return true;
        }
        else return false;
    }
    
    public function getActivity() {
            //includes checkouts (if published), comments, joins, and contributes (if published)
            global $oUtil;
            $strQuery ="SELECT NULL as itemid, commentid, NULL as checkoutid, NULL AS id, added FROM comments 
                    WHERE userid = '$this->id'
                UNION ";
            if ($oUser->publish_contributions){
            $strQuery .=
                "SELECT itemid, NULL as commentid, NULL as checkoutid, NULL AS id, added FROM items
                    WHERE contributorid = '$this->id'
                UNION ";
            }
            if ($oUser->publish_checkouts){
            $strQuery .=
                "SELECT NULL as itemid, NULL as commentid, checkoutid, NULL AS id, checkout FROM checkouts
                    WHERE userid = '$this->id'
                UNION ";
            }
            $strQuery .=
                "SELECT NULL as itemid, NULL as commentid, NULL AS checkoutid, id, `join` FROM members
                    WHERE id = '$this->id' 
                ORDER BY added DESC
                LIMIT 100";
            $aActivity = $oUtil->oData->getArray($strQuery);
            return ($aActivity);
    }
    
    public function addPoints($strAction) {
        global $oUtil;
        $strQuery = "SELECT * FROM points_settings WHERE action = '$strAction'";
        $aAction = $oUtil->oData->queryall($strQuery);
        $strColumn = $aAction[0]['column_name'];
        $iAddedPoints = $aAction[0]['point_amount'];
        $strQuery = "SELECT $strColumn FROM members WHERE id = $this->id";
        $iPreviousPoints = $this->oData->queryone($strQuery);
        $iPoints = $iPreviousPoints + $iAddedPoints;
        $strUpdate = "UPDATE members SET $strColumn = $iPoints WHERE id = $this->id";
        $oUtil->oData->query($strUpdate);
        
        
    }
    
    public function calculateFines(){
        global $oUtil;
        $strQuery = "SELECT * FROM fines WHERE userid = '$this->id'";
        $aFines = $oUtil->oData->queryall($strQuery);
        $fTotalFines = 0;
        $fTotalPaid = 0;
        $fTotalForgiven = 0;
        foreach ($aFines as $aFine){
            switch ($aFine['type']){
                case 'fine':
                    $fTotalFines += $aFine['amount'];
                    break;
                case 'paid':
                    $fTotalPaid += $aFine['amount'];
                    break;
                case 'forgiven':
                    $fTotalForgiven += $aFine['amount'];
                    break;                
            }
        }
        $fOutstanding = $fTotalFines - $fTotalPaid - $fTotalForgiven;
        $aResults = array(
            'totalFines' => number_format($fTotalFines, 2),
            'totalPaid' => number_format($fTotalPaid, 2),
            'totalForgiven' => number_format($fTotalForgiven, 2),
            'outstanding' => number_format($fOutstanding, 2)
        );
        return $aResults;
    }
    
    public function getFineDetails() {
        global $oUtil;
        $strQuery = "SELECT * FROM fines f LEFT JOIN checkouts c ON c.checkoutid = f.checkoutid WHERE f.userid = '$this->id' ORDER BY f.added";
        $aFines = $oUtil->oData->queryall($strQuery);
        $fBalance = 0;
        foreach($aFines as &$aFine){
            $aFine['amount'] = number_format($aFine['amount'], 2);
            switch ($aFine['type']){
                case 'fine':
                    $fBalance += $aFine['amount']; 
                    break;
                case 'paid':
                case 'forgiven':
                    $fBalance -= $aFine['amount'];
                    break;
            }
            $aFine['balance'] = number_format($fBalance, 2);
        }
        return $aFines;
    }
    
    public function getPreviousLogin(){
        global $oUtil;        
        $strQuery = "SELECT * FROM `access` WHERE userid = '$this->id' ORDER BY datetime DESC LIMIT 2";
        $aAccess = $oUtil->oData->queryall($strQuery);
        $strPreviousAccess = $aAccess[1]['datetime'];
        return ($strPreviousAccess);
    }
    
}

?>