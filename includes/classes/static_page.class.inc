<?php
class static_page extends DbInterface {
    const DATA_TABLE = 'static_pages';
    const ID_FIELD = 'pageID';
    
    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
    
    public static function getFields() {
		return array(
                    'pageID' => 'int',
                    'title' => 'varchar',
                    'subtitle' => 'varchar',
                    'contents' => 'text',
                    'added' => 'datetime',
                    'published' => 'int',
		);
	}
	
    public static function getMappings() {
            return array(
            );
    }
}

?>