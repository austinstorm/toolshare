<?php
class Comment extends DbInterface {
    const DATA_TABLE = 'comments';
    const ID_FIELD = 'commentid';
    
    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
    
    public static function getFields() {
		return array(
                    'commentid' => 'int',
                    'assignedtoid' => 'int',
                    'assignedtotable' => 'text',
                    'topic_created' => 'int',
                    'userid' => 'int',
                    'comment' => 'text',
                    'added' => 'datetime',
                    'deleted' => 'datetime',
		);
	}
	
    public static function getMappings() {
            return array(
            );
    }
    public function deleteComment(){
        $strDelete = "UPDATE comments SET deleted = NOW() WHERE commentid={$this->commentid}";
        $oUtil->oData->query($strDelete);
    }
}

?>