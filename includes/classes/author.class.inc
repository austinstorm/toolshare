<?php

class author extends DbInterface {
    
    const DATA_TABLE = 'authors';
    const ID_FIELD = 'authorid';
    public $aBooks;
    
    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
    
    public static function getFields() {
		return array(
			'authorid' => 'int',
			'name' => 'varchar'
		);
	}
	
	public static function getMappings() {
		return array(
		);
	}
    
//    public function setup($iAuthorid = null) {
//        global $oUtil;
//        if ($iAuthorid == null) {
//			$iAuthorid = $this->aAuthor['authorid'];
//		}	
//        $sqlSelect = "SELECT * FROM authors WHERE authorid = '{$iAuthorid}'";
//		if ($aResult = $oUtil->oData->queryone($sqlSelect)) {
//			$this->aAuthor = $aResult;
//			return true;
//		}
//		else {
//			return false;
//		}
//    }
    
    public function addAuthor ($aInfo) {
		$sqlInsert = "INSERT INTO authors SET name = '{$aInfo['name']}'";
                global $oUtil;
		if ($oUtil->oData->query($sqlInsert)) {
			$iAuthorid = parent::getLastId();
			$this->setup($iAuthorid);
			return true;
		}
		else {
			return false;
		}	
	}
     public function editAuthor ($aInfo) {
                $sqlInsert = "UPDATE authors SET name = '{$aInfo['name']}' WHERE authorid='{$this->aAuthor['authorid']}'";
		if (parent::query($sqlInsert)) {
			$iAuthorid = parent::getLastId();
			$this->setup($iAuthorid);
			return true;
		}
		else {
			return false;
		}	
     }
    
     public function deleteAuthor() {
                $sqlDelete = "UPDATE authors SET deleted = CURRENT_TIMESTAMP WHERE authorid='{$this->aAuthor['authorid']}'";
                if (parent::query($sqlDelete)) {
			return true;
		}
		else {
			return false;
                }
    }
    
    public function setBooks () {
        $strQuery = "SELECT bookid
            FROM bookauthor
            WHERE authorid = {$this->aAuthor['authorid']}";
        
        $aResults = $this->queryAll($strQuery);
        $aBooks = array();
        if (count($aResults) > 0) {
            foreach ($aResults as $aResult) {
                $iBookid = $aResult['bookid'];
                $aBooks[$iBookid] = new book($iBookid);
            }
        }
        
        $this->aBooks = $aBooks;
        return $this->aBooks;
    }
    
}    
?>
