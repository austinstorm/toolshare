<?php

class Item extends DbInterface {
    
    const DATA_TABLE = 'items';
    const ID_FIELD = 'itemid';
    public $aAuthors;
    public $aTags;
    public $aOtherFields;
    
    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
	
	public static function getFields() {
		return array(
			'itemid' => 'int',
			'itemtypeid' => 'int',
			'location' => 'varchar',
			'title' => 'varchar',
                        'image' => 'varchar',
			'description' => 'text',
                        'contributorid' => 'int',
                        'item_value' => 'float',
			'added' => 'datetime',
			'deleted' => 'datetime',
                        'status' => 'varchar'
		);
	}
	
	public static function getMappings() {
		return array(
		);
	}
    
    public function addOtherFields($aInfo){
        global $oUtil;
        $iItemid = $this->itemid;
        $aTags = json_decode($aInfo['tags']);
        $aItemTypeFields = $oUtil->getItemTypeFields($aInfo['itemtypeid']);
        $aItemInfo = array();
        foreach ($aItemTypeFields as $aField){
            $value = htmlentities($aInfo[$aField['Name']], ENT_QUOTES);
            $sqlInsert = "INSERT INTO item_type_field_values SET ItemID='{$this->itemid}', FieldID='{$aField['FieldID']}', Value='{$value}'";
            $oUtil->oData->query($sqlInsert);
        }
        if ($aTags[0] !== ''){
            foreach ($aTags as $strTag) {
                        $sqlFind = "SELECT tagid FROM tags WHERE tag = '{$strTag}'";
                        $iTagID = $oUtil->oData->queryone($sqlFind);
                        if ($iTagID==null){
                            $aTagInfo['tag'] = $strTag;
                            $iTagID=tag::insert($aTagInfo, 'tag');
                            $oTag = new tag($iTagID);
                        }
                        else $oTag=new tag($iTagID);
                        $aTagIndexInfo = array(
                            'itemid' => $iItemid,
                            'tagid' => $oTag->tagid
                        );
                        $this->addTagItemIndex($aTagIndexInfo);
            }
        }
        if ($aInfo['itemtypeid'] == 2){
            $aAuthors = explode(", ", $aInfo['authors']);
            foreach ($aAuthors as $strAuthor) {
                    $strAuthor = htmlentities($strAuthor, ENT_QUOTES);
                    $sqlFind = "SELECT authorid FROM authors WHERE name = '{$strAuthor}'";
                    $iAuthorID = $oUtil->oData->queryone($sqlFind);
                    if ($iAuthorID==null){
                        $aAuthorInfo['name'] = $strAuthor;
                        $iAuthorID=author::insert($aAuthorInfo, 'author');
                        $oAuthor = new author($iAuthorID);
                    }
                    else $oAuthor=new author($iAuthorID);
                    $aAuthorIndexInfo = array(
                        'itemid' => $iItemid,
                        'authorid' => $oAuthor->authorid
                    );
                    $this->addAuthorItemIndex($aAuthorIndexInfo);
                }
        }
    }
    
    public function updateOtherFields($aInfo){
        global $oUtil;
        $aTags = json_decode($aInfo['tags']);
        $aItemTypeFields = $oUtil->getItemTypeFields($aInfo['itemtypeid']);
        $aItemInfo = array();
        $iItemid = $this->itemid;
        foreach ($aItemTypeFields as $aField){
            $value = htmlentities($aInfo[$aField['Name']], ENT_QUOTES);
            $sqlInsert = "UPDATE item_type_field_values SET Value='{$value}' WHERE ItemID='{$this->itemid}' AND FieldID='{$aField['FieldID']}'";
            $oUtil->oData->query($sqlInsert);
        }
        
        $strRemoveTagIndex = "DELETE FROM tag_item_index WHERE itemid= '{$iItemid}'";
        $oUtil->oData->query($strRemoveTagIndex);

        foreach ($aTags as $strTag) {
            $sqlFind = "SELECT tagid FROM tags WHERE tag = '{$strTag}'";
            $iTagID = $oUtil->oData->queryone($sqlFind);
            if ($iTagID==null){
                $aTagInfo['tag'] = $strTag;
                $iTagID=tag::insert($aTagInfo, 'tag');
                $oTag = new tag($iTagID);
            }
            else $oTag=new tag($iTagID);
            $aTagIndexInfo = array(
                'itemid' => $iItemid,
                'tagid' => $oTag->tagid
            );
            $this->addTagItemIndex($aTagIndexInfo);
        }
        if ($aInfo['itemtypeid'] == 2){
            $aAuthors = explode(", ", $aInfo['authors']);
            $strRemoveAuthorIndex = "DELETE FROM author_item_index WHERE itemid= '{$iItemid}'";
            $oUtil->oData->query($strRemoveAuthorIndex);

            foreach ($aAuthors as $strAuthor) {
                $sqlFind = "SELECT authorid FROM authors WHERE name = '{$strAuthor}'";
                $iAuthorID = $oUtil->oData->queryone($sqlFind);
                if ($iAuthorID==null){
                    $aAuthorInfo['name'] = $strAuthor;
                    $iAuthorID=author::insert($aAuthorInfo, 'author');
                    $oAuthor = new author($iAuthorID);
                }
                else $oAuthor=new author($iAuthorID);
                $aAuthorIndexInfo = array(
                    'itemid' => $iItemid,
                    'authorid' => $oAuthor->authorid
                );
                $this->addAuthorItemIndex($aAuthorIndexInfo);
            }
        }
    }


//    public function deleteItem() {
//                $sqlDelete = "UPDATE items SET deleted = CURRENT_TIMESTAMP WHERE id='{$this->aItem['id']}'";
//                if (parent::query($sqlDelete)) {
//			return true;
//		}
//		else {
//			return false;
//                }
//    }
//    
    public function addTagItemIndex($aInfo) {
        $sqlInsert = "INSERT INTO tag_item_index SET itemid= '{$aInfo['itemid']}', tagid= '{$aInfo['tagid']}'";
        global $oUtil;
        if ($oUtil->oData->query($sqlInsert)) {
			return true;
		}
		else {
			return false;
		}
    }
    
    
    public function addAuthorItemIndex($aInfo) {
        $sqlInsert = "INSERT INTO author_item_index SET itemid= '{$aInfo['itemid']}', authorid= '{$aInfo['authorid']}'";
        global $oUtil;
        if ($oUtil->oData->query($sqlInsert)) {
			return true;
		}
		else {
			return false;
		}
    }
    
    public function setAuthors () {
        global $oUtil;
        $strQuery = "SELECT authorid
            FROM author_item_index
            WHERE itemid = {$this->itemid}";        
        $aResults = $oUtil->oData->getArray($strQuery);
        $aAuthors = array();
        if (count($aResults) > 0) {
            foreach ($aResults as $aResult) {
                $iAuthorid = $aResult['authorid'];
                $oAuthor = new author($iAuthorid);
                $aAuthors[] = $oAuthor->aData;
            }
        }
        $this->aAuthors = $aAuthors;
        return $this->aAuthors;
    }
 

    /*
    $oBook = new book($iBookid);
    
    $oBook->setAuthors();
    foreach ($oBook->aAuthors as $oAuthor) {
        echo "<tr><td>{$oAuthor->aAuthor['firstname']}</td><td>{$oAuthor->aAuthor['lastname']}</td></tr>";
    }
    */
    public function setTags () {
        global $oUtil;
        $strQuery = "SELECT tagid
            FROM tag_item_index
            WHERE itemid = {$this->itemid}";
        
        $aResults = $oUtil->oData->getArray($strQuery);
        $aTags = array();
        if (count($aResults) > 0) {
            foreach ($aResults as $aResult) {
                $iTagid = $aResult['tagid'];
                $oTag = new tag($iTagid);
                $aTags[] = $oTag->aData;
            }
        }
        
        $this->aTags = $aTags;
        return $this->aTags;
    }
    
    public function setOtherFields () {
        global $oUtil;
        $strQuery = "SELECT v.ItemID, v.Value, f.Name 
            FROM item_type_field_values v INNER JOIN item_type_fields f ON v.FieldID=f.FieldID
            WHERE itemid = {$this->itemid}";
        
        $aResults = $oUtil->oData->getArray($strQuery);
        $aOtherFields = array();
        if (count($aResults) > 0) {
            foreach ($aResults as $aResult) {
                $key = $aResult['Name'];
                $value = $aResult['Value'];
                $aOtherFields[$key] = $value;
            }
        }
        
        $this->aOtherFields = $aOtherFields;
        return $this->aOtherFields;
    }
    
    //Acceptable stati: out, in, lost, overdue
    public function updateItemStatus ($pstrStatus) {
        global $oUtil;
        $strQuery = "UPDATE items SET status = '$pstrStatus' WHERE itemid = {$this->itemid}";
        $oUtil->oData->query($strQuery);
    }
    
    public function getCheckoutID() {
        global $oUtil;
        $strQuery = "SELECT checkoutid FROM checkouts WHERE itemid = '$this->itemid' AND checkin = '0000-00-00 00:00:00'";
        $iCheckoutID = $oUtil->oData->queryone($strQuery);
        return $iCheckoutID;
    }
    
    public function getReservationID() {
        global $oUtil;
        $strQuery = "SELECT reservationid FROM reservations WHERE itemid = '$this->itemid' AND met = '0000-00-00 00:00:00'";
        $iReservationID = $oUtil->oData->queryone($strQuery);
        return $iReservationID;
    }
    
    public function getActivity() {
            //includes checkouts, comments,and contributes
            global $oUtil;
            $strQuery ="SELECT itemid, NULL as commentid, NULL as checkoutid, NULL AS id, added FROM items
                    WHERE itemid = '$this->itemid'
                UNION
                SELECT NULL as itemid, commentid, NULL as checkoutid, NULL AS id, added FROM comments 
                    WHERE assignedtoid = '$this->itemid' AND assignedtotable = 'item'
                UNION 
                SELECT NULL as itemid, NULL as commentid, checkoutid, NULL AS id, checkout FROM checkouts
                    WHERE itemid = '$this->itemid'
                ORDER BY added DESC
                LIMIT 100"; 
            $aActivity = $oUtil->oData->getArray($strQuery);
            return ($aActivity);
        }
    
    public function getCheckouts() {
        global $oUtil;
        $strCheckoutQuery = "SELECT * FROM checkouts WHERE itemid = $this->itemid";
        $aCheckouts = $oUtil->oData->getArray($strCheckoutQuery);
        return $aCheckouts;
    }
    
    public function isNew() {
        global $oUtil;
        $aSettings = $oUtil->getSettings();
        $strNewTime = $aSettings['new_time'];
        $strNewUntilDate = date('Y-m-d',strtotime($this->added.'+'.$strNewTime));
        if (date('Y-m-d') < $strNewUntilDate){
            return true;
        }
        else return false;
    }
    
//    
//        
//    public function setComments() {
//        $strQuery = "SELECT commentid
//            FROM comments
//            WHERE bookid = {$this->aBook['bookid']}";
//    
//        $aResults = $this->queryAll($strQuery);
//        $aComments = array();
//        if (count($aResults) > 0) {
//            foreach ($aResults as $aResult) {
//                $iCommentid = $aResult['commentid'];
//                $aComments[$iCommentid] = new comment($iCommentid);
//            }
//        }
//        $this->aComments = $aComments;
//        return $this->aComments;
//    }
//       public function setBookmarks() {
//            $strQuery = "SELECT bookmarkid
//            FROM bookmarks
//            WHERE bookid = {$this->aBook['bookid']}";
//        
//        $aResults = $this->queryAll($strQuery);
//        $aBookmarks = array();
//        if (count($aResults) > 0) {
//            foreach ($aResults as $aResult) {
//                $iBookmarkid = $aResult['bookmarkid'];
//                $aBookmarks[$iBookmarkid] = new bookmark($iBookmarkid);
//            }
//        }
//        
//        $this->aBookmarks = $aBookmarks;
//        return $this->aBookmarks;
//        }
//        
//        public function updateImage($filename) {
//            $sqlUpdate="UPDATE books SET image_name='$filename' WHERE bookid = {$this->aBook['bookid']}";
//            $this->query($sqlUpdate);
//            $this->setup();
//        }
}   
?>