<?php
class forum_category extends DbInterface {
    const DATA_TABLE = 'forum_categories';
    const ID_FIELD = 'categoryID';
    
    public function __construct($pmData = null) {
        parent::__construct($pmData);
    }
    
    public static function getFields() {
		return array(
                    'categoryID' => 'int',
                    'category_name' => 'varchar'
		);
	}
	
    public static function getMappings() {
            return array(
            );
    }
}

?>