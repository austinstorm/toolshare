<?php
include("includes/prepend.php");
include("includes/header.php");
?>
<div style="display:none" class="alert alert-error">
</div>
<div class="row-fluid">
    <div class="span8">
        <form id="checkin_form" class="form-horizontal" action="checkin_items.php">
            <fieldset>
            <div class="control-group">
                <label class="control-label" for="input03">Scan Items</label>
                <div class="controls">
                        <input name="itemids[]" type="text" class="input-medium checkout_item">
                </div>
            </div>
                <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Check In Items</button>
                        <button class="btn" id="clear_form" type="reset">Clear Form</button>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="span4">
        <h3>Item Circulation</h3>
        <p>Here's some text on how circulation works. Scan or enter a user, and then enter some items. Later we'll have a circulation period or other stuff like that or something.</p>
    </div>
</div>
<?php
include("includes/footer.php");
?>
<script type="text/javascript">
$(document).ready(function() {

    $('#checkin_form').ajaxForm({
        beforeSubmit: validate,
        success: function(){window.location.reload()}
    }); 
    
    function validate(formData, jqForm, options) { 
    // jqForm is a jQuery object which wraps the form DOM element 
    // 
    // To validate, we can access the DOM elements directly and return true 
    // only if the values of both the username and password fields evaluate 
    // to true 
    //TODO pre-validate items?
    var form = jqForm[0];
    return true;
    }

    function addError(error) {    
        $('.alert').show().html(error);
        $(window).scrollTop(0);

    }
});    
    
    $(".checkout_item").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            e.preventDefault();
            var $clone = $(".checkout_item:last").parents('.control-group').clone(true);
            $('.checkout_item:last').parents('.control-group').after($clone);
            $('.checkout_item:last').val('');
            $('.checkout_item:last').focus();
        }
    });
</script>