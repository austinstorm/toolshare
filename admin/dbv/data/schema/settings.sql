CREATE TABLE `settings` (
  `option_id` int(4) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8_bin NOT NULL,
  `autoload` char(3) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin