CREATE TABLE `reservations` (
  `userid` int(10) NOT NULL,
  `itemid` int(10) NOT NULL,
  `reserved` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `met` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1