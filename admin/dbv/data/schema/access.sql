CREATE TABLE `access` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(39) COLLATE utf8_bin NOT NULL,
  `userid` int(10) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin