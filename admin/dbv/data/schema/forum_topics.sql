CREATE TABLE `forum_topics` (
  `topicID` int(10) NOT NULL AUTO_INCREMENT,
  `categoryID` int(10) DEFAULT NULL,
  `topic_name` varchar(50) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  PRIMARY KEY (`topicID`),
  KEY `Column 2` (`categoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1