CREATE TABLE `item_type_fields` (
  `FieldID` int(10) NOT NULL AUTO_INCREMENT,
  `ItemTypeID` int(10) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `DisplayName` varchar(50) DEFAULT NULL,
  `InputType` enum('textfield','textblock','dropdown') DEFAULT NULL,
  `Default` varchar(50) DEFAULT NULL,
  `Options` varchar(250) DEFAULT NULL,
  `HelpText` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`FieldID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1