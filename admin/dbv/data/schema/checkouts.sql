CREATE TABLE `checkouts` (
  `checkoutid` int(10) NOT NULL AUTO_INCREMENT,
  `itemid` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `checkout` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `checkin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `due` date NOT NULL DEFAULT '0000-00-00',
  `renews` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`checkoutid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1