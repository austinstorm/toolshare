CREATE TABLE `items` (
  `itemid` int(10) NOT NULL AUTO_INCREMENT,
  `itemtypeid` int(10) NOT NULL,
  `location` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text,
  `contributorid` int(11) DEFAULT NULL,
  `item_value` decimal(10,2) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `status` enum('out','in','lost','overdue') DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1