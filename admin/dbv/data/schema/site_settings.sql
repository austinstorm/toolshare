CREATE TABLE `site_settings` (
  `optionID` int(10) DEFAULT NULL,
  `option_name` varchar(50) DEFAULT NULL,
  `option_value` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1