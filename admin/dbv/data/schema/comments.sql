CREATE TABLE `comments` (
  `commentid` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `assignedtoid` int(10) NOT NULL,
  `assignedtotable` enum('item','post') NOT NULL,
  `comment` text NOT NULL,
  `added` datetime NOT NULL,
  `deleted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`commentid`),
  KEY `userid` (`userid`),
  KEY `itemid` (`assignedtoid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1