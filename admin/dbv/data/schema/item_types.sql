CREATE TABLE `item_types` (
  `ItemTypeID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(10) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `CheckoutPeriod` varchar(50) DEFAULT '2 weeks',
  `Fine` varchar(50) NOT NULL DEFAULT '1.00',
  PRIMARY KEY (`ItemTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1