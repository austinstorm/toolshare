CREATE TABLE `item_type_field_values` (
  `ItemID` int(10) DEFAULT NULL,
  `FieldID` int(10) DEFAULT NULL,
  `Value` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1