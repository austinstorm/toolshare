CREATE TABLE `sessions` (
  `session_id` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `http_user_agent` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `session_data` blob NOT NULL,
  `session_expire` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin