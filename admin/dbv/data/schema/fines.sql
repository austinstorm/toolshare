CREATE TABLE `fines` (
  `userid` int(10) NOT NULL,
  `checkoutid` int(10) DEFAULT NULL,
  `amount` float NOT NULL,
  `type` enum('fine','paid','forgiven') NOT NULL,
  `added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1