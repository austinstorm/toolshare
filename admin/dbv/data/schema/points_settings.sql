CREATE TABLE `points_settings` (
  `action` varchar(50) NOT NULL,
  `column_name` enum('positive_points','negative_points') NOT NULL DEFAULT 'positive_points',
  `point_amount` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1