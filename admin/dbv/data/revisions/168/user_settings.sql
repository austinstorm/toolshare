ALTER TABLE `members`  
  ADD `photo_id_type` varchar(35) COLLATE utf8_bin DEFAULT NULL,
  ADD `id_number` varchar(35) COLLATE utf8_bin DEFAULT NULL,
  ADD `address_proof` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  ADD `agreement_signed` tinyint(1) NOT NULL DEFAULT '0';