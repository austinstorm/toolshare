ALTER TABLE `emails`
	ADD COLUMN `name` VARCHAR(30) NOT NULL AFTER `slug`;

UPDATE `emails` SET `name`='New User' WHERE  `id`=1 LIMIT 1;
UPDATE `emails` SET `name`='Verify Email' WHERE  `id`=2 LIMIT 1;
UPDATE `emails` SET `name`='Forgot Password' WHERE  `id`=3 LIMIT 1;
UPDATE `emails` SET `name`='New Password' WHERE  `id`=4 LIMIT 1;
UPDATE `emails` SET `name`='Checkout' WHERE  `id`=5 LIMIT 1;
UPDATE `emails` SET `name`='Return' WHERE  `id`=6 LIMIT 1;
UPDATE `emails` SET `name`='Reminder' WHERE  `id`=7 LIMIT 1;