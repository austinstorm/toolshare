<?php
include("includes/prepend.php");
include("includes/header.php");
$aForumCategories = $oUtil->listForumCategories();
if($sesslife == true){
?>

<div class="page-header">
    <h2>Forums</h2>
</div>

<div class="row-fluid">
    <div class="span9">
        <h3>New Topic</h3>

        <form class="" id="comment_form" action="add_comment.php" style="background: #f5f5f5; border: 1px solid #E3E3E3; padding-top: 10px;">
            <div class="row-fluid">
            <div class="span1">
            </div>

                <div class="span10">
                <fieldset>
                    <label><strong>Category</strong></label>
                    <select name="categoryID">
                        <?php 
                        foreach ($aForumCategories as $aCategory){
                            ?>
                        <option value="<?=$aCategory['categoryID']?>"><?=$aCategory['category_name']?></option>
                        <?php
                        }
                        ?>
                    </select>

                    <label><strong>Topic Title</strong></label>
                    <input class="span10" type="text" name="topic_name">

                    <textarea class="span12" rows="7" name="comment"></textarea>
                    <span class="help-block">You can use <a href="http://daringfireball.net/projects/markdown/syntax" target="_blank">Markdown</a> syntax to format your post.</span>

                    <label><strong>Tags</strong></label>
                    <textarea name="tags" type="text" class="input-xlarge" id="tags"></textarea>
                    <span class="help-block">Press enter to add tag</span>
                    <input type="hidden" name="userid" value="<?=$userid?>" />
                    <input type="hidden" name="added" value="NOW()" />
                    <input type="hidden" name="assignedtotable" value="post" />
                    
                    <input class="btn" name="submit" type="submit" value="Post Topic"/><br /><br />
                </fieldset>
                </div><!-- /.span10 -->
            <div class="span1">
            </div>

            </div><!-- /.row-fluid -->
        </form>
    </div>
    <div class="span3">
        <br>

    </div>
</div>      


<script>
$(document).ready(function(){
    $('#tags').textext({
        plugins : 'tags prompt focus autocomplete ajax',
        prompt : 'Add one...',
        ajax : {
            url : 'get_tags_list.php',
            dataType : 'json',
            cacheResults : true
        }
    });
    
    $('#forums').addClass("active");
    
    $('#comment_form').submit(function(e){
    e.preventDefault();
    var strData = ($(this).serialize());
    
    $.ajax({
            type: "POST",
            url: 'add_topic.php',
            dataType: 'json',
            data: strData,
            success: function(res) {
                if (res.success == 'success'){
                    window.location.href = 'view_topic.php?topicID='+res.id
                }
            }    
        });
    })
});
</script>

<?php
include("includes/footer.php");
}
else{
echo "<meta http-equiv=\"refresh\" content=\"0;url={$website}/".USER_DIRECTORY."/login\" />";
}
?>
