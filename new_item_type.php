<?php
include("includes/prepend.php");
include("includes/header.php");
if (!$is_admin){
?>
<div class="page-header">
    <h2>Permissions Error</h2>
</div>
    <p>You must be logged in as an admin to view this page. If you think you have received this message in error, please leave a message in the forums.</p>
<?php 
} else {
    echo $oUtil->getBreadcrumbs();

?>
<div class="page-header">
    <h2>Add An Item Type</h2>
</div>
    <div>
        <form id="add_item_type_form">
            <div class="control-group">
                <label class="control-label" for="member">Name</label>
                <div class="controls">
                    <input name="Name" type="text" class="input-xlarge required" id="Name">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="member">Description</label>
                <div class="controls">
                    <input name="Description" type="text" class="input-xlarge required" id="Description">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="member">Checkout Period</label>
                <div class="controls">
                    <div class="btn-group" name="CheckoutPeriod" data-toggle="buttons-radio">

                    <a class="btn active" value="2 weeks" href="#">2 weeks</a>
                    <a class="btn" value="3 weeks" href="#">3 weeks</a>
                    <a class="btn" value="1 month" href="#">1 month</a>
                    </div>
                <span class="help-block">This is how long this item type can be checked out.</span>

                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="member">Fine</label>
                <div class="controls">
                    <input name="Fine" type="text" class="input-xlarge required" id="Fine">
                </div>
            </div>
            <div id="item_type_fields">
                <div class="item_type_field">
                    <label class="control-label" for="member">Field Name</label>
                    <input type="text" name="fieldName[]" class="input-xlarge required">
                    <label class="control-label" for="member">Field Type</label>
                    <select class="field_type" name="InputType[]">
                        <option value="textfield">Text</option>
                        <option value="textblock">Text Block</option>
                        <option value="dropdown">Dropdown</option>
                    </select>
                    <div class="dropdown_options">
                        <label class="control-label" for="member">Dropdown Options</label>
                        <input type="text" name ="Options[]" class="input-xlarge">
                        <span class="help-block">Separate options with a comma, e.g. Hardback, Paperback</span>
                    </div>
                    <label class="control-label" for="member">Help Text</label>
                    <input type="text" name ="HelpText[]" class="input-xlarge">                    
                </div>
                <button type="button" id="add_field" class="btn btn-primary">Add Field</button>
            </div>
            <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Add Item Type</button>
                    <button class="btn">Cancel</button>
            </div>
        </form>
    </div>
<script type="text/javascript">
$(".field_type").change(function () {
        if ($(this).val() == 'dropdown'){
            $(this).siblings('[class="dropdown_options"]').show()
        }
        else {$(this).siblings('[class="dropdown_options"]').hide().val('')}
    })
    .change();    
    
$('#add_field').click(function(){
    var $clone = $(".item_type_field:last").clone(true);
    $(".item_type_field:last").after($clone);
    $(".item_type_field:last").find($('input')).val('');
    
    $(".field_type").change(function () {
        if ($(this).val() == 'dropdown'){
            $(this).siblings(('[class="dropdown_options"]')).show()
        }
        else {$(this).siblings(('[class="dropdown_options"]')).hide().val('')}
    })
    .change();
});   

$('#add_item_type_form').submit(function(e){
    e.preventDefault();
    var strData = ($(this).serialize());
    strData = strData + '&CheckoutPeriod=' + $('.btn-group').children('.active').text()
    $.ajax({
            type: "POST",
            url: 'add_item_type.php',
            dataType: 'json',
            data: strData,
            success: function(res) {
                if (res.success == 'success'){
                    window.location.href = 'item_types_settings.php'
                }
            }    
        });
})
    
</script>
    
<?php }
include("includes/footer.php");
?>    