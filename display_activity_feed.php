<?php
//Include this wherever there is an activity feed:
//view_item.php
//view_user.php
//index.php
if (isset($_REQUEST['itemid'])){
    $aActivity = $oItem->getActivity();
}
elseif (isset($_REQUEST['userid'])){
    $aActivity = $oUser->getActivity();
}
else {
    $aActivity = $oUtil->getGlobalActivity();
}
$aPointValues = $oUtil->getPointsSettingsArray();
?>
<div id="actvity_feed" class="feed"><br>
<?php
$iRowCount = 1;
$iRowsPerPage = 20;
$iTotalPages = ceil(count($aActivity)/$iRowsPerPage);
if (count($aActivity) > $iRowsPerPage){
    $bPaginate = true;
}
else $bPaginate = false;
foreach ($aActivity as $aAction){
    if ($aAction['itemid']){
        $oItem = new item($aAction['itemid']);
        $oContributor = new user($oItem->contributorid);
        ?>
        <div class="row-fluid comments tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>">
            <div class="span1">
                <?php
                $contributor_email = '';
                if ($oContributor->publish_contributions){
                    $contributor_email = $oContributor->email;
                }
                $default = $website."/images/anonuser_50px.gif";
                $gravatar = new Gravatar($contributor_email, $default);
                $gravatar->size = 50;
                ?>
                <img src="<?php echo $gravatar->getSrc(); ?>">
            </div>
            <div class="span11">
                <?php if ($oContributor->publish_contributions){ ?>
                <a href="view_user.php?userid=<?=$oContributor->id?>" class="user"><?=$oContributor->first_name.' '.$oContributor->last_name?></a> contributed <a href="view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a>
                    <?php if ($oItem->image != ''){ ?>
                      <img src="<?=$oItem->image?>" width="265" class="thumbnail" alt="<?=$oItem->title?>">
                    <?php }?>
                  <?php } else {?>
                <strong>Anonymous</strong> contributed <a href="view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a>
                  <?php if ($oItem->image != ''){ ?>
                    <img src="<?=$oItem->image?>" width="265" class="thumbnail" alt="<?=$oItem->title?>">
                  <?php }?>
                <?php }?>
                <p class="meta">
                  <small><i class="icon-time"></i> 
                    <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aAction['added'])?>"><?=$aAction['added']?></abbr>
                    <i class="icon-upload"></i> 
                      <?=$aPointValues['add item']['sign'].$aPointValues['add item']['amount']?> Points
                </small>
                </p>
            </div>
        </div>
    <hr class="tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>"/>
    <?php
    }
    if ($aAction['commentid']){
        $oComment = new comment($aAction['commentid']);
        $oUser = new user($oComment->userid);
        switch ($oComment->assignedtotable){
                            case 'item':
                                $oCommentedOn = new item($oComment->assignedtoid);
                                $strLink = 'view_item.php?itemid='.$oComment->assignedtoid;
                                $strLinkText = $oCommentedOn->title;
                                $strPointAction = 'item comment';
                                break;
                            case 'post':
                                $oCommentedOn = new forum_topic($oComment->assignedtoid);
                                $strLink = 'view_topic.php?topicID='.$oComment->assignedtoid;
                                $strLinkText = $oCommentedOn->topic_name;
                                if ($oComment->topic_created == 1){
                                    $strPointAction = 'add topic';
                                }
                                else {
                                    $strPointAction = 'forum comment';
                                }                                
                                break;
                        }
        ?>
        <div class="row-fluid comments tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>">
            <div class="span1">
                <?php
                $grav_email = $oUser->email;
                $default = $website."/images/anonuser_50px.gif";
                $gravatar = new Gravatar($grav_email, $default);
                $gravatar->size = 50;
                ?>
                <img src="<?php echo $gravatar->getSrc(); ?>">
            </div>
            <div class="span11">
                <a href="view_user.php?userid=<?=$oUser->id?>" class="user"><?=$oUser->first_name.' '.$oUser->last_name?></a> commented on <a href="<?=$strLink?>"><?=$strLinkText?></a>
                <blockquote>
                    <p><?=$oComment->comment?></p>
                </blockquote>
                <p class="meta">
                <small><i class="icon-time"></i> 
                    <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aAction['added'])?>"><?=$aAction['added']?></abbr>
                    <i class="icon-upload"></i> 
                      <?=$aPointValues[$strPointAction]['sign'].$aPointValues[$strPointAction]['amount']?> Points
                </small>
                </p>
            </div>
        </div>
    <hr class="tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>"/>
        <?php
    }
    if ($aAction['id']){
        $oJoinedUser = new user($aAction['id']);
        ?>
        <div class="row-fluid comments tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>">
            <div class="span1">
                <?php
                $grav_email = $oJoinedUser->email;
                $default = $website."/images/anonuser_50px.gif";
                $gravatar = new Gravatar($grav_email, $default);
                $gravatar->size = 50;
                ?>
                <img src="<?php echo $gravatar->getSrc(); ?>">
            </div>
            <div class="span11">
                <a href="view_user.php?userid=<?=$oJoinedUser->id?>" class="user"><?=$oJoinedUser->first_name.' '.$oUser->last_name?></a> joined <?=$aSettings['title']?>
                <p class="meta">
                <small>
                  <i class="icon-time"></i> 
                    <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aAction['added'])?>"><?=$aAction['added']?></abbr>
                </small>
                </p>
            </div>
        </div>
    <hr class="tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>"/>
        <?php
    }
    if ($aAction['checkoutid']){
        $oCheckout = new checkout($aAction['checkoutid']);
        $oItem = new item($oCheckout->itemid);
        $oCheckoutUser = new user($oCheckout->userid);
        ?>
        <div class="row-fluid comments tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>">
            <div class="span1">
                <?php
                $checkout_email = '';
                if ($oCheckoutUser->publish_checkouts){
                    $checkout_email = $oContributor->email;
                }
                $default = $website."/images/anonuser_50px.gif";
                $gravatar = new Gravatar($checkout_email, $default);
                $gravatar->size = 50;
                ?>
                <img src="<?php echo $gravatar->getSrc(); ?>">
            </div>
            <div class="span11">
                <?php if ($oCheckoutUser->publish_checkouts) {?>
                <a href="view_user.php?userid=<?=$oCheckoutUser->id?>" class="user"><?=$oCheckoutUser->first_name.' '.$oCheckoutUser->last_name?></a> checked out <a href="view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a>
                <?php } else {?>
                <strong>Anonymous</strong> checked out <a href="view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a>
                <?php } ?>
                <p class="meta">
                  <small>
                    <i class="icon-time"></i> 
                      <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aAction['added'])?>"><?=$aAction['added']?></abbr>
                    <i class="icon-upload"></i> 
                      <?=$aPointValues['item checkout']['sign'].$aPointValues['item checkout']['amount']?> Points
                  </small>
                </p>
            </div>
        </div>
    <hr class="comments tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>"/>
        <?php
    }
    $iRowCount++;                   
}
if ($bPaginate){
    $i = 1;
?>
    <div class="pagination" num_pages="<?=$iTotalPages ?>">
      <ul>
        <li class="disabled"><a href="#">&laquo; Prev</a></li>
        <?php
        while ($i <= $iTotalPages) {
        ?>
        <li class="<?= $i == 1 ? 'active' : ''?>"><a href="#"><?=$i?></a></li>
        <?php
        $i++;
        }
        ?>
        <li class=""><a href="#">Next &raquo; </a></li>
      </ul>
    </div>

<?php
}
?>
         
	</div><!-- /#activity -->
<script type="text/javascript">
    $('.tablerow').hide();
    $('.page1').show();
    $('.previous_page').hide();
    
    $('.pagination a').click(function(e){
        var tab = $(this).parents('.tab-pane');
        e.preventDefault();
        if ($(this).parent().hasClass('disabled')){
            return;
        }
        else {
            var text = $(this).text();
            if (text.search('Next') !== -1){
                var page = parseInt($('.pagination').find('.active').text());
                var new_page = parseInt(page + 1);
                goToPage(new_page, tab);
                return;
            }

            else {
                if (text.search('Prev') !== -1){
                var page = parseInt($('.pagination').find('.active').text());
                var new_page = parseInt(page - 1);
                goToPage(new_page, tab);
                return;
                }
                else {
                    goToPage(parseInt(text), tab);
                }
            }
        }
    });
    
    function goToPage(page_number, tab){
        $('.tablerow').hide();
        $('.page' + page_number).show();
        $('.pagination ul li').removeClass('active');
        $('.pagination ul li:nth-child('+ parseInt(page_number + 1) +')').addClass('active');
        if (page_number == 1){
            $('.pagination li:first').addClass('disabled');
        }
        else {
            $('.pagination li:first').removeClass('disabled');
        }
        
        if (page_number == $('.pagination').attr('num_pages')){
            $('.pagination li:last').addClass('disabled');
        }
        else {
            $('.pagination li:last').removeClass('disabled');
        }
    }
</script>
