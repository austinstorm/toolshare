<?php
include("includes/prepend.php");
include("includes/header.php");
if (!$is_admin){
?>
<div class="page-header">
    <h2>Permissions Error</h2>
</div>
    <p>You must be logged in as an admin to view this page. If you think you have received this message in error, please leave a message in the forums.</p>
<?php 
} else {
    echo $oUtil->getBreadcrumbs();
$aEmails = $oUtil->getEmails();
if ($_REQUEST['success']){
?>    
    <div class="alert alert-success">
Templates saved.
</div>
<?php } ?>
    <div class="page-header">
        <h2>Email Templates</h2>
    </div>

<form id="settings_form" class="form-horizontal">
    <fieldset>

    <ul class="nav nav-tabs" id="settingsTabs">
        <?php
        $i = 0;
        foreach ($aEmails as $aEmail){
            $strActive = '';
            if ($i==0){
                $strActive = "class='active'";
            }
            echo "<li ".$strActive."><a href='#".$aEmail['slug']."'>".$aEmail['name']."</a></li>";
            $i++;
        }
        ?>
    </ul>
     
    <div id="emailsTabsContent" class="tab-content">
        <?php
        $i=0;
        foreach ($aEmails as $aEmail){
        ?>
      <div class="tab-pane <?=$i==0 ? 'active' :''?>" id="<?=$aEmail['slug']?>">
        <br>
        <div class="row-fluid">
            <div class="span8">
                <div class="control-group">
                    <label class="control-label" for="member">Subject</label>
                    <div class="controls">
                        <input name="<?=$aEmail['slug']?>[subject]" type="text" class="input-xlarge required" id="title" value="<?=$aEmail['subject']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="member">Message</label>
                    <div class="controls">
                        <textarea rows="10" name="<?=$aEmail['slug']?>[message]" class="input-xxlarge"><?=$aEmail['message']?></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls input-xxlarge">
                        <pre><?=$aEmail['helptext']?>
                        </pre>
                    </div>
                </div>
            </div>
        </div><!-- /.row-fluid -->
      </div><!-- /#general -->
      <?php 
      $i++;
      }?>      
      </div><!-- /#itemtypes -->

        <div class="form-actions">
                <button type="submit" class="btn btn-primary">Save Settings</button>
                <button class="btn" id="clear_form" type="reset">Cancel Changes</button>
        </div>
    </fieldset>
</form>

<?php
}
?>

<script>
$('#settingsTabs a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
})
$(document).ready(function(){
    $('#admin').addClass("active");
    
    $('#settings_form').submit(function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'update_email_templates.php',
            dataType: 'json',
            data: $(this).serialize(),
            success: function(res) {
                if (res.success == 'success'){
                    var url = window.location.href;    
                    if (url.indexOf('?') > -1){
                    url += '&success=success'
                    }else{
                    url += '?success=success'
                    }
                    window.location.href = url;
                }
            }    
        });
    });
    
});

</script>

<?php
include("includes/footer.php");
?>