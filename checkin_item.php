<?php
include("includes/prepend.php");
$iItemID = $_REQUEST['itemid'];

    if (item::isValidID($iItemID)){        
        $oItem = new item($iItemID);
        $iCheckoutID = $oItem->getCheckoutID();
        if ($iCheckoutID >= 1){
            $oCheckout = new Checkout($iCheckoutID);
            $oCheckout->checkIn();
            $oUser = new User($oCheckout->userid);
            return_email($oUser->email, $iItemID);
        }
    }
echo json_encode(array('success' => 'success'));
?>
