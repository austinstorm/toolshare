<?php
include("includes/prepend.php");
//Should probably move this to a class
if (strlen($_REQUEST['email']) > 0 && strlen($_REQUEST['password']) > 0) {
        $strEmail = $_REQUEST['email'];
        $strPassword = $_REQUEST['password'];

        $strQuery = "SELECT userid, password
            FROM users
            WHERE email = '{$strEmail}'";

        $aUser = $oUtil->oData->queryrow($strQuery);
        if (isset($aUser['userid'])) {
            $oUser=new user($aUser['userid']);
            if ($aUser['password'] === $strPassword) {
                $_SESSION['userid'] = $aUser['userid'];
                $oUser->update(array('last_login' => 'NOW()'), 'user');
                returnResult(array(userid => $aUser['userid']));
            }
            else {
                //wrong password
                returnResult(array(error => 'Incorrect password'));
            }
        }
        else {
            //no user with that email
            returnResult(array(error => 'Invalid email'));
        }
}
else {
    //empty password or email input
    returnResult(array(error => 'Please enter both an email and password'));
}

function returnResult($aResult){
    echo json_encode($aResult);
    die();
}
?>
