<?php
/*
	@! AuthManager v3.0
	@@ User authentication and management web application
-----------------------------------------------------------------------------	
	** author: StitchApps
	** website: http://www.stitchapps.com
	** email: support@stitchapps.com
	** phone support: +91 9871084893
-----------------------------------------------------------------------------
	@@package: am_authmanager3.0
*/
include("../includes/prepend.php");
include("../includes/header.php");
//
?>
<script src="../includes/js/jquery.hashchange.js"></script>
<script src="../includes/js/admin.base.js"></script>
<script type="text/javascript">
$(function() {
	$(".micro").timeago();
});
</script>

	<ul class="breadcrumb">
        <li><a href="index.php">Home</a>
            <span class="divider">/</span></li>
        <li><a href="userid">Profile</a>
            <span class="divider">/</span></li>
        <li class="active">Edit Profile</li>
    </ul>
    
<?
//subheader(_("Edit Profile"), null, $js);

if($sesslife == true) {
	if(isset($_POST["editprofile"])) {
		$first_name = cleanInput($_POST["first_name"]);
		$last_name = cleanInput($_POST["last_name"]);
		$bio = cleanInput($_POST["bio"]);
                $address1 = cleanInput($_POST["address1"]);
                $address2 = cleanInput($_POST["address2"]);
                $phone = cleanInput($_POST["phone"]);
                $publish_checkouts = cleanInput($_POST["publish_checkouts"]);
                $publish_contributions = cleanInput($_POST["publish_contributions"]);

		try {
			$update_user = "UPDATE `members` SET `first_name` = :first_name, `last_name` = :last_name, `bio` = :bio, `address1` = :address1, `address2` = :address2, `phone` = :phone, `publish_checkouts` = :publish_checkouts, `publish_contributions` = :publish_contributions WHERE `id` = :userid";
			$update_user_do = $db->prepare($update_user);
			$update_user_do->bindParam(':first_name', $first_name, PDO::PARAM_STR);
			$update_user_do->bindParam(':last_name', $last_name, PDO::PARAM_STR);
			$update_user_do->bindParam(':bio', $bio, PDO::PARAM_STR);
                        $update_user_do->bindParam(':address1', $address1, PDO::PARAM_STR);
                        $update_user_do->bindParam(':address2', $address2, PDO::PARAM_STR);
                        $update_user_do->bindParam(':phone', $phone, PDO::PARAM_STR);
			$update_user_do->bindParam(':userid', $userid, PDO::PARAM_INT);
                        $update_user_do->bindParam(':publish_checkouts', $publish_checkouts, PDO::PARAM_INT);
                        $update_user_do->bindParam(':publish_contributions', $publish_contributions, PDO::PARAM_INT);
			$update_user_do->execute();
		} catch(PDOException $e) {
			$log->logError($e." - ".basename(__FILE__));
		}

		$err = "<div class=\"alert alert-success\"><strong>"._("Profile Updated.")."</strong><br/>"._("Your profile has been updated successfully.")."</div>";
	}

	/*
	displaying the user info edit profile page.
	*/
	edit_profile();
} else {
	echo "<meta http-equiv=\"refresh\" content=\"0;url={$website}/".USER_DIRECTORY."/login\" />";
}

include("../includes/footer.php");
?>