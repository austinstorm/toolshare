<?php
include("includes/prepend.php");
include("includes/header.php");
//if (isset($_REQUEST['authorid'])){
//    $_REQUEST['itemtypeid'] = 2;
//}
//if (isset($_REQUEST['publisher'])){
//    $_REQUEST['publisher'] = html_entity_decode($_REQUEST['publisher']);
//    $_REQUEST['itemtypeid'] = 2;
//}
//if (isset($_REQUEST['manufacturer'])){
//    $_REQUEST['manufacturer'] = html_entity_decode($_REQUEST['manufacturer']);
//    $_REQUEST['itemtypeid'] = 1;
//}
////If it's a tag we want both tables (possibly)
//if (isset($_REQUEST['tagid'])){
//    $_REQUEST['itemtypeid'] = 2;
//}
$aItems = $oUtil->listItems($_REQUEST);
$aTools = array();
$aBooks = array();
$aOthers = array();
$aTopics = array();
foreach ($aItems as $aItem) {
    if (isset($aItem['topicID'])){        
        $oTopic = new forum_topic($aItem['topicID']);
        $aTopics[] = $oTopic;
    }
    else {
        $oItem = new item($aItem['itemid']);

        if ($oItem->itemtypeid==1){
            $aTools[] = $oItem;
        }   
        elseif ($oItem->itemtypeid==2){
            $aBooks[] = $oItem;
        }
        else $aOthers[] = $oItem;
    }
}
echo $oUtil->getBreadcrumbs();
?>
<div class="page-header">
    <h2>
        <?php
        if (isset($_REQUEST['tagid'])){
            $oTag = new tag($_REQUEST['tagid']);
            echo 'Tag: ' . $oTag->tag;
        }
        elseif (isset($_REQUEST['tag'])){
            echo 'Tag: ' . ucfirst($_REQUEST['tag']);
        }
        elseif (isset($_REQUEST['authorid'])){
            $oAuthor = new author($_REQUEST['authorid']);
            echo 'Author: ' . $oAuthor->name;
        }
        elseif (isset($_REQUEST['publisher'])){
            echo 'Publisher: ' . $_REQUEST['publisher'];
        }
        elseif (isset($_REQUEST['manufacturer'])){
            echo 'Manufacturer: ' . $_REQUEST['manufacturer'];
        }
        else echo "Items";
        ?>
    </h2>
</div>
<?php
if (isset($_REQUEST['tagid']) || isset($_REQUEST['search'])){
?>
    <ul class="nav nav-tabs" id="searchResults">
      <li class="active"><a href="#books_tab">Books (<?=count($aBooks)?>)</a></li>
      <li><a href="#tools_tab">Tools (<?=count($aTools)?>)</a></li>
      <li><a href="#others_tab">Others (<?=count($aOthers)?>)</a></li>
      <li><a href="#topics_tab">Forum Topics (<?=count($aTopics)?>)</a></li>
    </ul>
    <div id="searchResultsContent" class="tab-content">
<?php
}
?>

<?php
if (count($aBooks)){
?>
    
    <div class="tab-pane active" id="books_tab">
    

<h3>Books</h3>
    <table class="table table-bordered table-striped">
            <thead>
                <tr>
                        <th></th>
                        <th>Title / Year</th>
                        <th>Details / Added</th>
                        <th><i class="icon-comment" alt="Comments"></i></th>
                        <th><i class="icon-tag" alt="Checkouts"></i></th>
                        <th>Contributor</th>
                </tr>
            </thead>
		<tbody>
                
                    <?php
                    $iRowCount = 1;
                    $iRowsPerPage = 20;
                    $iTotalBookPages = ceil(count($aBooks)/$iRowsPerPage);
                    if (count($aBooks) > $iRowsPerPage){
                        $bPaginate = true;
                    }
                    else $bPaginate = false;
                    foreach ($aBooks as $oItem) {
                        $aAuthors = $oItem->setAuthors();
                        $aTags = $oItem->setTags();
                        $aOtherFields = $oItem->setOtherFields();
                        $aItemID = array('itemid' => $oItem->itemid);
                        $aComments = $oUtil->getComments($aItemID);
                        $aCheckouts = $oItem->getCheckouts();
                        if (user::isValidID($oItem->contributorid)){
                            $oContributor = new User($oItem->contributorid);
                        }
                    ?>
                        <tr class="tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>">
                            <td>
                                <img class="thumbnail" src="<?php echo (empty($oItem->image) ? 'http://placehold.it/45x45' : $oItem->image);  ?>" height="45" alt="<?=$oItem->title?>">
                            </td>
                            <td><strong><a href="view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a></strong> 
                                <?php foreach ($aAuthors as $aAuthor) {
                                if ($aAuthor === reset($aAuthors) && $aAuthor['name']!=='')
						echo 'by ';?>
                                <a href="list_items.php?authorid=<?=$aAuthor['authorid']?>"><?=$aAuthor['name']?></a>, 
                                <?php }?>
                                (<a href="list_items.php?publisher=<?=str_replace('&','%26', $aOtherFields['publisher'])?>"><?=$aOtherFields['publisher']?></a>, <?=$aOtherFields['year']?>) <br />

                                    <?php foreach($aTags as $aTag){
                                        echo ( "<a href='list_items.php?tagid={$aTag['tagid']}' class='label'>
                                                ".$aTag['tag']."</a> ");            
                                    }?>
                                <?php if ($oItem->isNew()) {?><span class="label label-success">New</span><?php }?>


                            </td>
                            <td><?=$aOtherFields['format']?>, <?=$aOtherFields['num_pages']?> pages<br /><abbr class="timeago" title="<?=$oUtil->timeagoFormat($oItem->added)?>"><?=$oItem->added?></abbr>
</td>
                            <td><?=count($aComments)?></td>
                            <td><?=count($aCheckouts) ? count($aCheckouts) : 0?></td>
                            <td>
                                <?php if ($oContributor && $oContributor->publish_contributions){?><a href="profile/<?=$oItem->contributorid?>"><?=$oContributor->first_name.' '.$oContributor->last_name?></a>
                                <?php } else echo 'Anonymous'?>
                            </td>
                        </tr>
                    <?php 
                    $iRowCount++;
                    }?>
                    
            </tbody>
    </table>
<?php
if ($bPaginate){
    $i = 1;
?>
    <div class="pagination" num_pages="<?=$iTotalBookPages ?>">
      <ul>
        <li class="disabled"><a href="#">&laquo; Prev</a></li>
        <?php
        while ($i <= $iTotalBookPages) {
        ?>
        <li class="<?= $i == 1 ? 'active' : ''?>"><a href="#"><?=$i?></a></li>
        <?php
        $i++;
        }
        ?>
        <li class=""><a href="#">Next &raquo; </a></li>
      </ul>
    </div>

<?php
}
?>
    </div><!-- /#books -->
<?php
 }

if (count($aTools)) {
?>


<div class="tab-pane" id="tools_tab">


<h3>Tools</h3>
    <table class="table table-bordered table-striped">
            <thead>
                <tr>
                        <th></th>
                        <th>Title / Size</th>
                        <th>Details / Added</th>
                        <th><i class="icon-comment" alt="Comments"></i></th>
                        <th><i class="icon-tag" alt="Checkouts"></i></th>
                        <th>Contributor</th>
                </tr>
            </thead>
		<tbody>
                    <?php
                    $iRowCount = 1;
                    $iRowsPerPage = 20;
                    $iTotalToolPages = ceil(count($aTools)/$iRowsPerPage);
                    if (count($aTools) > $iRowsPerPage){
                        $bPaginate = true;
                    }
                    else $bPaginate = false;
                    
                    foreach ($aTools as $oItem) {
                        $aTags = $oItem->setTags();
                        $aOtherFields = $oItem->setOtherFields();
                        $aItemID = array('itemid' => $oItem->itemid);
                        $aComments = $oUtil->getComments($aItemID);
                        $aCheckouts = $oItem->getCheckouts();
                        if (user::isValidID($oItem->contributorid)){
                            $oContributor = new User($oItem->contributorid);
                        }
                    ?>
                    <tr class="tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>">
                        <td>
                            <img class="thumbnail" src="<?php echo (empty($oItem->image) ? 'http://placehold.it/45x45' : $oItem->image);  ?>" height="45" alt="<?=$oItem->title?>">
                        </td>
                        <td><strong><a href="view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a></strong> 
                            (<a href="list_items.php?manufacturer=<?=str_replace('&', '%26', $aOtherFields['manufacturer'])?>"><?=$aOtherFields['manufacturer']?></a>, <?=$aOtherFields['size']?>) <br />
                            
                                    <?php foreach($aTags as $aTag){
                                        echo ( "<a href='list_items.php?tagid={$aTag['tagid']}' class='label'>
                                                ".$aTag['tag']."</a> ");            
                                    }?>
                            <?php if ($oItem->isNew()) {?><span class="label label-success">New</span><?php }?>



                        </td>
                        <td><?=$aOtherFields['model_number']?><br /><abbr class="timeago" title="<?=$oUtil->timeagoFormat($oItem->added)?>"><?=$oItem->added?></abbr></td>
                        <td><?=count($aComments)?></td>
                        <td><?=count($aCheckouts)?></td>
                        <td>
                                <?php if ($oContributor && $oContributor->publish_contributions){?><a href="profile/<?=$oItem->contributorid?>"><?=$oContributor->first_name.' '.$oContributor->last_name?></a>
                                <?php } else echo 'Anonymous'?>
                        </td>
                    </tr>
                    <?php 
                    $iRowCount++;
                    }?>
                
            </tbody>
    </table>
<?php
if ($bPaginate){
    $i = 1;
?>
    <div class="pagination" num_pages="<?=$iTotalToolPages ?>">
      <ul>
        <li class="disabled"><a href="#">&laquo; Prev</a></li>
        <?php
        while ($i <= $iTotalToolPages) {
        ?>
        <li class="<?= $i == 1 ? 'active' : ''?>"><a href="#"><?=$i?></a></li>
        <?php
        $i++;
        }
        ?>
        <li class=""><a href="#">Next &raquo; </a></li>
      </ul>
    </div>

<?php
}
?>
    </div><!-- /#tools -->
<?php }?>
<?php
if (count($aOthers)) {
?>

<div class="tab-pane" id="others_tab">


<h3>Others</h3>
    <table class="table table-bordered table-striped">
            <thead>
                <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Added</th>
                        <th><i class="icon-comment" alt="Comments"></i></th>
                        <th><i class="icon-tag" alt="Checkouts"></i></th>
                        <th>Contributor</th>
                </tr>
            </thead>
		<tbody>
                    <?php
                    $iRowCount = 1;
                    $iRowsPerPage = 20;
                    $iTotalOtherPages = ceil(count($aOthers)/$iRowsPerPage);
                    if (count($aOthers) > $iRowsPerPage){
                        $bPaginate = true;
                    }
                    else $bPaginate = false;
                    foreach ($aOthers as $oItem) {
                        $aTags = $oItem->setTags();
                        $aOtherFields = $oItem->setOtherFields();
                        $aItemID = array('itemid' => $oItem->itemid);
                        $aComments = $oUtil->getComments($aItemID);
                        $aCheckouts = $oItem->getCheckouts();
                        if (user::isValidID($oItem->contributorid)){
                            $oContributor = new User($oItem->contributorid);
                        }
                    ?>
                    <tr class="tablerow page<?=ceil(($iRowCount)/$iRowsPerPage)?>">
                        <td>
                            <img class="thumbnail" src="<?php echo (empty($oItem->image) ? 'http://placehold.it/45x45' : $oItem->image);  ?>" height="45" alt="<?=$oItem->title?>">
                        </td>
                        <td><strong><a href="view_item.php?itemid=<?=$oItem->itemid?>"><?=$oItem->title?></a></strong> 
                            <br />
                            
                                    <?php foreach($aTags as $aTag){
                                        echo ( "<a href='list_items.php?tagid={$aTag['tagid']}' class='label'>
                                                ".$aTag['tag']."</a> ");            
                                    }?>
                            <?php if ($oItem->isNew()) {?><span class="label label-success">New</span><?php }?>



                        </td>
                        <td><abbr class="timeago" title="<?=$oUtil->timeagoFormat($oItem->added)?>"><?=$oItem->added?></abbr></td>
                        <td><?=count($aComments)?></td>
                        <td><?=count($aCheckouts)?></td>
                        <td>
                                <?php if ($oContributor && $oContributor->publish_contributions){?><a href="profile/<?=$oItem->contributorid?>"><?=$oContributor->first_name.' '.$oContributor->last_name?></a>
                                <?php } else echo 'Anonymous'?>
                        </td>
                    </tr>
                    <?php 
                    $iRowCount++;
                    }?>                   
                
            </tbody>
    </table>
<?php
    if ($bPaginate){
    $i = 1;
?>
    <div class="pagination" num_pages="<?=$iTotalOtherPages ?>">
      <ul>
        <li class="disabled"><a href="#">&laquo; Prev</a></li>
        <?php
        while ($i <= $iTotalOtherPages) {
        ?>
        <li class="<?= $i == 1 ? 'active' : ''?>"><a href="#"><?=$i?></a></li>
        <?php
        $i++;
        }
        ?>
        <li class=""><a href="#">Next &raquo; </a></li>
      </ul>
    </div>

<?php
}
?>
</div><!-- /#others -->    
<?php }?>

<?php
if (count($aTopics)) {
    if ($userid){
    $oUser = new user($userid);
    $strPreviousLogin = $oUser->getPreviousLogin();
    }
?>

<div class="tab-pane" id="topics_tab">


<h3>Forum Topics</h3>
    <table class="table table-bordered table-striped">
            <thead>
                <tr>
                        <th></th>
                        <th>Topic</th>
                        <th>Replies</th>
                        <th>Topic author</th>
                </tr>
            </thead>
		<tbody>
                    <?php
                    foreach($aTopics as $oTopic) {
                        $aTags = $oTopic->setTags();
                        $aPostInfo = $oUtil->getPostInfo($oTopic->topicID);
                        $oLatestPoster =  new User($aPostInfo['latest_id']);
                        $oAuthor = new User($aPostInfo['author_id']);
                    ?>
                        <tr>
                            <td>
                                <?php
                                if ($strPreviousLogin < $aPostInfo['latest_added']){
                                ?>
                                <img src="includes/icons/folder_table.png">
                                <?php }
                                else {
                                ?>                                
                                <img src="includes/icons/folder.png">
                                <?php }?>
                            </td>
                            <td><strong>
        						<a href="view_topic.php?topicID=<?=$oTopic->topicID?>"><?=$oTopic->topic_name?></a>
	        					</strong>
                                <?php foreach($aTags as $aTag){
                                    echo ( "<a class='label label-info' href='list_items.php?tagid={$aTag['tagid']}'>
                                            ".$aTag['tag']."</a> ");            
                                }?>
                                <div style="float:right;">by <a href="view_user.php?userid=<?=$oLatestPoster->id?>"><?=$oLatestPoster->first_name.' '.$oLatestPoster->last_name?></a> <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aPostInfo['latest_added'])?>"><?=$oUtil->timeagoFormat($aPostInfo['latest_added'])?></abbr></div>

	        				</td>
                            <td><?=$aPostInfo['replies']?></td>
                            <td><a href="view_user.php?userid=<?=$oAuthor->id?>"><?=$oAuthor->first_name.' '.$oAuthor->last_name?></a></td>
                        </tr>
                    <?php }?>
                    
            </tbody>
    </table>
</div><!-- /#topics -->    
<?php }?>

</div>

<script>
$(document).ready(function(){
    var aBooks = <?=json_encode($aBooks)?>;
    var aTools = <?=json_encode($aTools)?>;
    
    $('.tablerow').hide();
    $('.page1').show();
    $('.previous_page').hide();
    
    $('.pagination a').click(function(e){
        var tab = $(this).parents('.tab-pane');
        e.preventDefault();
        if ($(this).parent().hasClass('disabled')){
            return;
        }
        else {
            var text = $(this).text();
            if (text.search('Next') !== -1){
                var page = parseInt(tab.find('.pagination').find('.active').text());
                var new_page = parseInt(page + 1);
                goToPage(new_page, tab);
                return;
            }

            else {
                if (text.search('Prev') !== -1){
                var page = parseInt(tab.find('.pagination').find('.active').text());
                var new_page = parseInt(page - 1);
                goToPage(new_page, tab);
                return;
                }
                else {
                    goToPage(parseInt(text), tab);
                }
            }
        }
    });
    
    function goToPage(page_number, tab){
        tab.find('.tablerow').hide();
        tab.find('.page' + page_number).show();
        tab.find('.pagination ul li').removeClass('active');
        tab.find('.pagination ul li:nth-child('+ parseInt(page_number + 1) +')').addClass('active');
        if (page_number == 1){
            tab.find('.pagination li:first').addClass('disabled');
        }
        else {
            tab.find('.pagination li:first').removeClass('disabled');
        }
        
        if (page_number == tab.find('.pagination').attr('num_pages')){
            tab.find('.pagination li:last').addClass('disabled');
        }
        else {
            tab.find('.pagination li:last').removeClass('disabled');
        }
    }    
    
    if (aBooks.length !== 0){
        $('#books').addClass("active");        
    }
        
    if (aTools.length !== 0){
        $('#tools').addClass("active");
    }
    

});
$('#searchResults a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');

})

</script>
<?php
include("includes/footer.php");
?>
