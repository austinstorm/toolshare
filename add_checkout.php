<?php
include("includes/prepend.php");
$iUserID = $_REQUEST['userid'];
$oUser = new user($iUserID);
foreach ($_REQUEST['itemids'] as $ID){
    if (item::isValidID($ID)){
        $aItemIDs[] = $ID;
    }
}
if ($iUserID){
    foreach ($aItemIDs as $iItemID) {
        if (item::isValidID($iItemID)){
            $aCheckoutInfo = array(
                'userid' => $iUserID,
                'itemid' => $iItemID,
                'checkout' => 'NOW()',
                'due' => $oUtil->getDueDate()
            );
            $oItem = new item($iItemID);
            if ($oItem->status !== 'out'){
                checkout::insert($aCheckoutInfo, 'checkout');
                $oItem->updateItemStatus('out');
                $iReservationID = $oItem->getReservationID();
                if ($iReservationID){
                    $oReservation = new Reservation($iReservationID);
                    if ($oReservation->userid == $iUserID){
                        $oReservation->meetReservation();
                    }
                }
                $oUser->addPoints('item checkout');
            }            
        }        
    }
    checkout_email($oUser->email, $aItemIDs);
}
echo json_encode(array('success' => 'success'));
?>
