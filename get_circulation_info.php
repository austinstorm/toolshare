<?php
include("includes/prepend.php");
if (isset($_REQUEST['itemid'])){
    $oItem = new Item($_REQUEST['itemid']);
    $oCheckout = new Checkout($oItem->getCheckoutID());
    $iUserID = $oCheckout->userid; 
}
else $iUserID = $_REQUEST['userid'];
$oUser = new User($iUserID);
$aUser = array(
    'id' => $oUser->id,
    'first_name' => $oUser->first_name,
    'last_name' => $oUser->last_name,
    'join' => $oUser->join,
    'photo_id_type' => $oUser->photo_id_type,
    'id_number' => $oUser->id_number,
    'address_proof' => $oUser->address_proof,
    'agreement_signed' => $oUser->agreement_signed
    );
$aCheckouts = $oUser->getCheckoutsAndItems();
foreach ($aCheckouts as &$aCheckout){
    $oItem = new Item($aCheckout['itemid']);
    $aCheckout['reservationid'] = $oItem->getReservationID();
}

$aFineInfo = $oUser->calculateFines();
echo json_encode(array(
    'aUser' => $aUser,
    'aCheckouts' => $aCheckouts,
    'aFineInfo' => $aFineInfo
));
?>
