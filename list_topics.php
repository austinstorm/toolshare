<?php
include("includes/prepend.php");
include("includes/header.php");
$aForumTopics = $oUtil->listForumTopics($_REQUEST['categoryID']);
$oCategory = new forum_category($_REQUEST['categoryID']);
echo $oUtil->getBreadcrumbs();
if ($userid){
    $oUser = new user($userid);
    $strPreviousLogin = $oUser->getPreviousLogin();
}
?>


<div class="page-header">
    <h2>Forums </h2>
</div>

<div class="row-fluid">
    <div class="span9">
<h3><?=$oCategory->category_name?></h3>
    <table class="table table-bordered table-striped">
            <thead>
                <tr>
                        <th></th>
                        <th>Topic</th>
                        <th>Replies</th>
                        <th>Topic author</th>
                </tr>
            </thead>
		<tbody>
                    <?php
                    foreach($aForumTopics as $aTopic) {
                        $oTopic = new forum_topic($aTopic['topicID']);
                        $aTags = $oTopic->setTags();
                        $aPostInfo = $oUtil->getPostInfo($aTopic['topicID']);
                        $oLatestPoster =  new User($aPostInfo['latest_id']);
                        $oAuthor = new User($aPostInfo['author_id']);
                    ?>
                        <tr>
                            <td>
                                <?php
                                if ($strPreviousLogin < $aPostInfo['latest_added']){
                                ?>
                                <img src="includes/icons/folder_table.png">
                                <?php }
                                else {
                                ?>                                
                                <img src="includes/icons/folder.png">
                                <?php }?>
                            </td>
                            <td><strong>
        						<a href="view_topic.php?topicID=<?=$aTopic['topicID']?>"><?=$aTopic['topic_name']?></a>
	        					</strong>
                                <?php foreach($aTags as $aTag){
                                    echo ( "<a class='label label-info' href='list_items.php?tagid={$aTag['tagid']}'>
                                            ".$aTag['tag']."</a> ");            
                                }?>
                                <div style="float:right;">by <a href="view_user.php?userid=<?=$oLatestPoster->id?>"><?=$oLatestPoster->first_name.' '.$oLatestPoster->last_name?></a> <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aPostInfo['latest_added'])?>"><?=$oUtil->timeagoFormat($aPostInfo['latest_added'])?></abbr></div>

	        				</td>
                            <td><?=$aPostInfo['replies']?></td>
                            <td><a href="view_user.php?userid=<?=$oAuthor->id?>"><?=$oAuthor->first_name.' '.$oAuthor->last_name?></a></td>
                        </tr>
                    <?php }?>
                    
            </tbody>
    </table>
    </div>
    <div class="span3">
        <a href="new_topic.php" class="btn btn-large btn-info" style="display: block; ">New Discussion</a>
        <br>

    </div>
</div>      

<script>
$(document).ready(function(){
    $('#forums').addClass("active");
});
</script>

<?php
include("includes/footer.php");
?>
