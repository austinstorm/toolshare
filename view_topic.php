<?php
include("includes/prepend.php");
include("includes/header.php");
$oTopic = new forum_topic($_REQUEST['topicID']);
$aTags = $oTopic->setTags();
$aCommenters = $oTopic->getDistinctCommenters();
echo $oUtil->getBreadcrumbs();
?>

    <div class="page-header">
      <h2><?=$oTopic->topic_name?></h2>
    </div>
<div class="row-fluid">
	<div class="span9">

<?php
include('display_comment_form.php');
?>		

	</div>
	<div class="span3">
		<a href="new_topic.php" class="btn btn-large btn-info" style="display: block; ">New Discussion</a>
		<br>
    <table class="table table-condensed">
          <thead>
            <tr>
              <th>In This Discussion</th>
            </tr>
          </thead>
          <tbody>
              <?php 
              foreach ($aCommenters as $aCommenter){
                  $oCommenter = new User($aCommenter['userid']);
                  ?>
              <tr>
                <td><a href="view_user.php?id=<?=$aComment['userid']?>"><?=$oCommenter->first_name.' '.$oCommenter->last_name?></a> 
                    <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aCommenter['added'])?>"><?=$oUtil->timeagoFormat($aCommenter['added'])?></abbr>
                </td>
              </tr>
              <?php
              }
              ?>
          </tbody>
        </table>
    <table class="table table-condensed">
          <thead>
            <tr>
              <th>Tags</th>
            </tr>
          </thead>
          <tbody>
              <tr>
                <td><?php foreach($aTags as $aTag){
            echo ( "<a class='label label-info' href='list_items.php?tagid={$aTag['tagid']}'>
                    ".$aTag['tag']."</a> ");            
        }?>
                </td>
              </tr>
          </tbody>
        </table>
	</div>
</div>		
<script>
$(document).ready(function(){
    $('#forums').addClass("active");
});
</script>

<?php
include("includes/footer.php");
?>
