<?php
include("includes/prepend.php");
include("includes/header.php");
if ($oItem){
$aAuthors = $oItem->setAuthors();
foreach($aAuthors as $aAuthor) {
    $aAuthorNames[] = $aAuthor['name'];
}
$strAuthors = implode(", ", $aAuthorNames);
$aTags = $oItem->setTags();
foreach($aTags as $aTag) {
    $aTagNames[] = $aTag['tag'];
}
$jsonTags = json_encode($aTagNames);
$aOtherFields = $oItem->setOtherFields();
echo $oUtil->getBreadcrumbs();
?>


<form id="item_form" class="form-horizontal" action="update_item.php">
    <div class="alert alert-error" style="display:none"></div>
    <fieldset>
    <legend>Edit an Item</legend>
    <div class="control-group">
        <label class="control-label" for="title">Title</label>
        <div class="controls">
        <input value="<?=$oItem->title?>" name="title" type="text" class="input-xlarge" id="title">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="image_input">Image</label>
        <div class="controls">
        <input name="image" value="<?=$oItem->image?>" type="text" id="image_input" class="input-xlarge" >
        <p class="help-block">In full URL form.</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="input02">Tags</label>
        <div class="controls">
        <textarea name="tags" type="text" class="input-xlarge" id="tags"></textarea>
        <p class="help-block">Press enter to add a tag</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="input03">Description</label>
        <div class="controls">
                <textarea name="description" class="input-xlarge span6" id="textarea" rows="3"><?=$oItem->description?></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="location">Location</label>
        <div class="controls">
        <input name="location" value="<?=$oItem->location?>" type="text" class="input-medium" id="location">
        </div>
    </div>
     <div class="control-group">
        <label class="control-label" for="contributorid">Contributor</label>
        <div class="controls">
        <input name="contributorid" value="<?=$oItem->contributorid?>" type="text" class="input-medium required" id="contributorid">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="item_value">Value</label>
        <div class="controls">
        <input name="item_value" value="<?=$oItem->item_value?>" type="text" class="input-medium required" id="item_value">
        </div>
    </div>
    <?php
            $aTypeFields = $oUtil->getItemTypeFields($oItem->itemtypeid);
            foreach ($aTypeFields as $aTypeField){
                switch ($aTypeField['InputType']) {
                    case 'textfield':
                        if ($aTypeField['Name'] == 'authors'){
                            $strValue = $strAuthors;
                        }
                        else $strValue = $aOtherFields[$aTypeField["Name"]];
                        $htmlInput = '<input name="'.$aTypeField["Name"].'" value="'.$strValue.'" type="text" class="input-medium" id="'.$aTypeField["Name"].'">';
                        break;
                    case 'textblock':
                        $htmlInput = '<textarea name="'.$aTypeField["Name"].'" class="input-xlarge span6" id="'.$aTypeField["Name"].'" rows="3">'.$aOtherFields[$aTypeField["Name"]].'</textarea>';
                        break;
                    case 'dropdown':
                        $aOptions = explode(', ', $aTypeField['Options']);
                        $strOptions = '';
                        foreach ($aOptions as $strOption){
                            $strSelected = '';
                            if ($strOption== $aOtherFields[$aTypeField["Name"]]){
                                $strSelected = 'selected="selected"';
                            }
                            $strOptions .= '<option '.$strSelected.'>'.$strOption.'</option>';
                        }
                        $htmlInput = '<select name="format" class="span2" id="format">'.$strOptions.'</select>';
                        break;                    
                }
            ?>
            <div class="control-group">
                <label class="control-label" for="<?=$aTypeField['Name']?>"><?=$aTypeField['DisplayName']?></label>
                <div class="controls">
                <?=$htmlInput?>
                </div>
            </div>
            <?php
            }
            ?>
 
        <div class="form-actions">
                <button type="submit" class="btn btn-primary">Edit Item</button>
                <button class="btn">Cancel</button>
        </div>
    </fieldset>
    <input type="hidden" name="itemid" value="<?=$_REQUEST['itemid']?>">
    <input type="hidden" name="itemtypeid" value="<?=$oItem->itemtypeid?>">
</form>
<?php
}
else {
    echo "Invalid ID";
}
include("includes/footer.php");
?>
<script type="text/javascript">
$(document).ready(function() {     
    $('#tags').textext({
        plugins : 'tags prompt focus autocomplete ajax',
        prompt : 'Add one...',
        tagsItems : <?=$jsonTags?>,
        ajax : {
            url : 'get_tags_list.php',
            dataType : 'json',
            cacheResults : true
        }
    });
    
    $('#item_form').ajaxForm({
        beforeSubmit: validate,
        dataType: 'json',
        success: function(res){
            window.location.href = 'view_item.php?itemid=' + res.itemid
        } }); 
    
    function validate(formData, jqForm, options) { 
    // jqForm is a jQuery object which wraps the form DOM element 
    // 
    // To validate, we can access the DOM elements directly and return true 
    // only if the values of both the username and password fields evaluate 
    // to true 
    var form = jqForm[0];
    if (!form.title.value) { 
        addError('Please enter a value for Title'); 
        return false; 
    }
//    if (form.publisher){
//        if (!form.publisher.value) { 
//            addError('Please enter a value for Publisher'); 
//            return false; 
//        }
//    }
//    if (form.manufacturer){
//        if (!form.manufacturer.value) { 
//            addError('Please enter a value for Manufacturer'); 
//            return false; 
//        }
//    }
    if (!form.location.value) { 
        addError('Please enter a value for Location'); 
        return false; 
    }
    return true;
    }
});

function addError(error) {
    $('.alert').show().html(error);
    $(window).scrollTop(0);

}
</script>