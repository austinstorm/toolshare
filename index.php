<?php
include("includes/prepend.php");
include("includes/header.php");

?>

<?php if($sesslife == true) {
    $aActivity = $oUtil->getGlobalActivity();
    ?>
<!-- logged in -->

    <div class="page-header">
        <h2>Home <small></small>
        </h2>
    </div>

<div class="row-fluid">
	<div class="span9">
<?php
include('display_activity_feed.php');
?>

</div><!-- /.span9 -->
	<div class="span3">
    <h3>Checked Out</h3>
        <table class="table table-striped">
          <thead>
            <tr>
              <td>Item</td>
              <td>Due</td>
            </tr>
          </thead>
          <tbody>
              <?php
            $oUser = new user($userid);  
            $aCheckouts = $oUser->getCheckoutsAndItems();
            foreach ($aCheckouts as $aCheckout){  
              ?>
            <tr>
              <td><?=$aCheckout['title']?></td>
              <td><?=date('M j', strtotime($aCheckout['due']))?></td>
            </tr>
            <?php }?>
          </tbody>
        </table>
	</div><!-- /.span3 -->
</div><!-- /.row-fluid -->
<?php    
}
else {
    ?>
	<!-- BROCHURE -->

<style type="text/css">
/* Jumbotrons
-------------------------------------------------- */
.jumbotron {
  min-width: 980px;
  margin: 20px -20px 0 -20px;
}
.jumbotron .inner {
  background: transparent url(includes/img/grid-18px.png) top center;
  padding: 45px 0;
  -webkit-box-shadow: inset 0 10px 30px rgba(0,0,0,.3);
     -moz-box-shadow: inset 0 10px 30px rgba(0,0,0,.3);
/*          box-shadow: inset 0 10px 30px rgba(0,0,0,.3);
*/}
.jumbotron .container {
  background-color: transparent;
}
.jumbotron h1,
.jumbotron p {
  margin-bottom: 9px;
  color: #fff;
  text-align: center;
  text-shadow: 0 1px 1px rgba(0,0,0,.3);
}
.jumbotron h1 {
  font-size: 54px;
  font-family: 'Carter One';
  line-height: 1;
  text-shadow: 0 1px 2px rgba(0,0,0,.5);
}
.jumbotron p {
  font-weight: 300;
}
.jumbotron .lead {
  font-size: 20px;
  line-height: 27px;
}
.jumbotron p a {
  color: #fff;
  font-weight: bold;
}
.masthead {
  background-color: #049cd9;
  background-repeat: no-repeat;
  background-image: -webkit-gradient(linear, left top, left bottom, from(#004D9F), to(#049cd9));
  background-image: -webkit-linear-gradient(#004D9F, #049cd9);
  background-image: -moz-linear-gradient(#004D9F, #049cd9);
  background-image: -o-linear-gradient(top, #004D9F, #049cd9);
  background-image: -khtml-gradient(linear, left top, left bottom, from(#004D9F), to(#049cd9));
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#004D9F', endColorstr='#049cd9', GradientType=0); /* IE8 and down */
}
/* Quickstart section for getting le code
-------------------------------------------------- */
.quickstart {
  min-width: 980px;
  margin: -20px -20px 0 -20px;

  background-color: #f5f5f5;
  background-repeat: repeat-x;
  border-top: 1px solid #fff;
  border-bottom: 1px solid #eee;
}
.quickstart .container {
  margin-bottom: 0;
  background-color: transparent;
}
.quickstart .row {
  margin: 0 -20px;
}
.quickstart [class*="span"] {
  width: 285px;
  height: 117px;
  margin-left: 0;
  padding: 17px 20px 26px;

}
.quickstart [class*="span"]:last-child {
  width: 286px;
}
.quickstart h6,
.quickstart p {
  line-height: 21px;
  text-align: left;
  margin-bottom: 9px;
  color: #333;
}
.quickstart .current-version,
.quickstart .current-version a {
  color: #999;
}
.quickstart h6 {
  color: #999;
}
.quickstart textarea {
  display: block;
  width: 275px;
  height: auto;
  margin: 0 0 9px;
  line-height: 21px;
  white-space: nowrap;
  overflow: hidden;
}


</style>

	<div class="jumbotron masthead">
	<div class="inner">
        <div class="container">
          <h1>the Moscow Toolbox</h1>
          <p class="lead">
            A local circulating library focused on the creative arts and entrepreneurship<br>
            promoting access to information, tools & mentorship on the Palouse.<br>
          </p>
          <p>We are currently conducting a limited <strong>beta test</strong> of the software. <a href="<?php echo $website."/".USER_DIRECTORY; ?>/register">Sign up</a> to help!</p>
        </div><!-- /container -->
    </div>
	</div>
	  <div class="quickstart">
      <div class="container">
        <div class="row">
          <div class="span4">
            <h6>More than a library</h6>
            <p>The strength of the library isn't the collection, it's the people. Connect & share, get rewarded for what you contribute, and directly influence the future of the library.</p>
          </div>
          <div class="span4">
            <h6>Our passion</h6>
            <p>Sometimes access is even better than ownership. Spend less time and money and more time honing your craft. Help us make Moscow into a thriving creative community.</p>
          </div>
          <div class="span4">
            <h6>Become a member</h6>
            <p><a href="<?php echo $website."/".USER_DIRECTORY; ?>/register" class="btn btn-primary">Sign up now &raquo;</a></p>
            <p class="current-version">Currently in a limited beta.</p>
          </div>
        </div><!-- /row -->
      </div>
    </div>

<!--    <div class="container">
      <div class="row">
        <div class="span8 offset2">
          <div class="page-header">
            <h2></h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="span2 offset2">Icon</div>
        <div class="span6"><p><strong>The Moscow Toolbox</strong> is a cooperative tool lending library. We carry a variety of tools
          for home repair, gardening, bicycle maintenance, and media production, which are loaned to our members
          free of charge. In addition, we have an extensive online community where members can support and assist
          each other in their projects.<p>
          <p>We have a vision of our community empowered by the tools and skills needed to transform their homes and
            communities, a vision of interdependent community, sustainability and creativity.
        </div>
      </div>
      <div class="row">
        <div class="span2 offset2">Icon</div>
        <div class="span6"><p>We are organized as a co-operative, which reflects our commitment to local
          community ownership. This structure makes it easier to empower and educate our members, to give them
          a sense of ownership over the organization and its future. It ties our success to the commitment of
          the community we seek to serve. We operate under non-profit practices.<p>
          <p>There are currently no paid positions at the Toolbox. Instead of staff, a commited Board of Directors,
            Volunteers, and Members keep our organization running. We highly encourage members to get involved,
            and a great way to do that is to become a volunteer.
        </div>
      </div>
    </div><!-- /container -->
<!--
    <div class="page-header">
    	<h2>Community Built, Community Led<small></small></h2>
    </div>
        <div class="row">
        	<div class="span1 offset1">
        		Icon
        	</div>
        	<div class="span8">
      	 <p>The West Seattle Tool Library is a community-led project to provide pay-what-you-can community access to a wide range of tools, training, and relevant advice.

By providing this service, the West Seattle Tool Library aims to inspire its community to participate in community projects, such as park restorations, and pursue sustainability through fun projects like backyard gardens, home energy improvements, and water harvesting.


        	</div>
        </div>
-->
    <!-- /BROCHURE -->

<?php
}
?>
<script>
$(document).ready(function(){ 
    $('#home').addClass("active");
});
</script>

<?php
include("includes/footer.php");
?>
