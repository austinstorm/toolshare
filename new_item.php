<?php
include("includes/prepend.php");
include("includes/header.php");
echo $oUtil->getBreadcrumbs();
$aItemTypes = $oUtil->getItemTypes();
?>

<div class="page-header">
    <h2>Add an Item</h2>
</div>
<div class="row-fluid">
    <div class="span8">

    <form id="item_form" class="form-horizontal" action="add_item.php">
        <div class="alert alert-error" style="display:none"></div>
        <fieldset>
        <div class="control-group">
            <h3>General Information</h3>
        </div>
        <div class="control-group">
            <label class="control-label" for="title">Title</label>
            <div class="controls">
            <input name="title" type="text" class="input-xlarge required" id="title">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="image_input">Image</label>
            <div class="controls">
            <input name="image" type="text" class="input-xlarge" id="image_input">
            <p class="help-block">In full URL form.</p>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="tags">Tags</label>
            <div class="controls">
            <textarea name="tags" type="text" class="input-xlarge" id="tags"></textarea>
            <p class="help-block">Press enter to add a tag</p>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="textarea">Description</label>
            <div class="controls">
                <textarea name="description" class="input-xlarge span12" id="textarea" rows="10"></textarea>
                <p class="help-block">
                    <pre>Item description uses <a href="http://daringfireball.net/projects/markdown/syntax" target="_blank">Markdown</a> syntax for formatting.</pre>
                </p>
            </div>
        </div>
        <div class="control-group">
            <h3>Libary Details</h3>
        </div>
        <div class="control-group">
            <label class="control-label" for="location">Location</label>
            <div class="controls">
            <input name="location" type="text" class="input-medium required" id="location">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="contributorid">Contributor</label>
            <div class="controls">
            <input name="contributorid" type="text" class="input-medium required" id="contributorid">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="item_value">Value</label>
            <div class="controls">
            <input name="item_value" type="text" class="input-medium required" id="item_value">
            </div>
        </div>
        <div class="control-group">
            <h3>Item Specific</h3>
        </div>
        <div class="control-group">
            <label class="control-label" for="type_select">Item Type</label>
            <div class="controls">
            <select name="itemtypeid" class="span2" id="type_select">
                <?php 
                foreach ($aItemTypes as $aItemType){
                    echo '<option value="'.$aItemType['ItemTypeID'].'">'.$aItemType['Name'].'</option>';
                }?>                
            </select>
            </div>
        </div>
<?php
    foreach ($aItemTypes as $aItemType) {
        ?>
            <div id="<?=lcfirst($aItemType['Name']).'_fields'?>" class="item_type_div" item_type_id="<?=$aItemType['ItemTypeID']?>">
            <?php
            $aTypeFields = $oUtil->getItemTypeFields($aItemType['ItemTypeID']);
            foreach ($aTypeFields as $aTypeField){
                switch ($aTypeField['InputType']) {
                    case 'textfield':
                        $htmlInput = '<input name="'.$aTypeField["Name"].'" type="text" class="input-medium" id="'.$aTypeField["Name"].'">';
                        break;
                    case 'textblock':
                        $htmlInput = '<textarea name="'.$aTypeField["Name"].'" class="input-xlarge span6" id="'.$aTypeField["Name"].'" rows="3"></textarea>';
                        break;
                    case 'dropdown':
                        $aOptions = explode(', ', $aTypeField['Options']);
                        $strOptions = '';
                        foreach ($aOptions as $strOption){
                            $strOptions .= '<option>'.$strOption.'</option>';
                        }
                        $htmlInput = '<select name="format" class="span2" id="format">'.$strOptions.'</select>';
                        break;                    
                }
            ?>
            <div class="control-group">
                <label class="control-label" for="<?=$aTypeField['Name']?>"><?=$aTypeField['DisplayName']?></label>
                <div class="controls">
                <?=$htmlInput?>
                </div>
            </div>
            <?php
            }
            ?>
            </div>
    <?php
    }
?>

            <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Add Item</button>
                    <button class="btn">Cancel</button>
            </div>
        </fieldset>
        <input type="hidden" name="status" value="in">
        <input type="hidden" name="added" value="NOW()">
    </form>
    </div><!-- /span8 -->

    <div class="span4" id="isbnwidget">
        <div>
            <p><strong>ISBN Lookup</strong></p>
            <form id="isbnLookup" class="form-inline">
                <fieldset>
                <p>Use this form to fill in information automatically for books. <strong>Remember to verify and correct the results!</strong></p>
                        <input type="text" placeholder="ISBN" name="isbn" width="15" id="isbnLookupBox"  />
                        <button class="btn" type="button" id="isbnSearch"><a>Search</a></button>
                    <span id="isbnLoading"></span>
                    <span id="isbnMessage"></span>
                </fieldset>
            </form>
        </div>
    </div><!-- /#isbnwidget -->
    <div class ="span4" id="imagepreview">
        <p><strong>Image Preview</strong></p>
        <img src="http://placehold.it/275x400" width="265" class="thumbnail" alt="No image found">
    </div><!-- /#imagepreview -->
</div><!-- /.row-fluid -->


<?php
include("includes/footer.php");
?>
<script type="text/javascript">
$(document).ready(function() {  
    $('#tags').textext({
        plugins : 'tags prompt focus autocomplete ajax',
        prompt : 'Add one...',
        ajax : {
            url : 'get_tags_list.php',
            dataType : 'json',
            cacheResults : true
        }
    });
    
    $('#image_input').blur(function(){
        $('#imagepreview img').attr('src', $(this).val());
    });
    
    $('#type_select').val(2);
    
    $('#add').addClass("active");

    $("#type_select").change(function () {
        if ($(this).val() == 2){
            $('#isbnwidget').show()
        }
        else {$('#isbnwidget').hide()}
        $('.item_type_div').hide();
        $('[item_type_id='+$(this).val()+']').show();
    })
    .change();
    
    $('#item_form').ajaxForm({
        beforeSubmit: validate,
        dataType: 'json',
        success: function(res){
            window.location.href = 'view_item.php?itemid=' + res.itemid
        }
    }); 
    
    function validate(formData, jqForm, options) { 
    // jqForm is a jQuery object which wraps the form DOM element 
    // 
    // To validate, we can access the DOM elements directly and return true 
    // only if the values of both the username and password fields evaluate 
    // to true 
 
    var form = jqForm[0];
    if (!form.title.value) { 
        addError('Please enter a value for Title'); 
        return false; 
    } 
//    if (form.publisher){
//        if (!form.publisher.value) { 
//            addError('Please enter a value for Publisher'); 
//            return false; 
//        }
//    }
//    if (form.manufacturer){
//        if (!form.manufacturer.value) { 
//            addError('Please enter a value for Manufacturer'); 
//            return false; 
//        }
//    }
    if (!form.location.value) { 
        addError('Please enter a value for Location'); 
        return false; 
    }
    return true;
    }
});

function addError(error) {    
    $('.alert').show().html(error);
    $(window).scrollTop(0);

}
</script>

<script type="text/javascript">
$(function()
{
    function doIsbnLookup() {
        var isbn = $("#isbnLookupBox").val();
        $('#type_select').val(2);
        $('.item_type_div').hide();
        $('[item_type_id=2]').show();
        console.log(isbn);
        $.ajax({
            method: "get",
            url: "isbnlookup.php",
            data: "isbn=" + isbn + "&authkey=2e4fa4a8f2c64f17d97e5b3712405ab2d7c57225",
            dataType: "json",
            beforeSend: function() {
                $("#isbnLoading").html('<img src="http://bibliotik.org/static/icons/loading.gif" />');
            },
            complete: function() {
                $("#isbnLoading").html('');
            },
            success: function(json) {
                var strISBN = ('ISBN:' + isbn);
                var aAuthors = new Array();
                if (!$(json).length) {
                    $("#isbnMessage").text('Invalid ISBN');
                } else {
                    var res = (json[strISBN]);
                    var i = 0;
                    $(res.authors).each(function(){
                        aAuthors[i] = this.name;
                        i++;
                    });
                    if ($("#title").val() == ''){
                        $("#title").val(res.title);
                    }
                    if ($("#authors").val() == ''){
                        $("#authors").val(aAuthors.join(', '));
                    }
                    if ($("#publisher").val() == ''){
                        $("#publisher").val(res.publishers[0].name);
                    }
                    if ($("#isbn").val() == ''){
                        $("#isbn").val(isbn);
                    }
                    if ($("#year").val() == ''){
                        $("#year").val(res.publish_date);
                    }
                    if ($("#num_pages").val() == ''){
                        $("#num_pages").val(res.number_of_pages);
                    }
                    if ($("#image_input").val() == ''){
                        $("#image_input").val(res.cover.large);
                        $('#imagepreview img').attr('src', $("#image_input").val());
                    } 
                }
            },
            error: function() {
                $("#isbnMessage").text("Connection Error");
            }
        });
    }

    $("#isbnLookupBox").keypress(function(e) {        
        if (e.which !== 13) {
            return;
        }
        e.preventDefault();
        $("#isbnMessage").text("");
        doIsbnLookup();
    });
    $("#isbnSearch").click(function() {
        $("#isbnMessage").text("");
        doIsbnLookup();
    });
});
</script>


    