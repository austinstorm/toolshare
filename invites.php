<?php
include("includes/prepend.php");
include("includes/header.php");
echo $oUtil->getBreadcrumbs();
?>    
<div class="page-header">
    <h2>Invitations</h2>
</div>  

<p>Membership to this site is by invitation only. You can use <strong>invite codes</strong> to invite your friends to this site. 
    If you don't have any more invite codes, no worries - more invite codes are issued periodically - check the forum for details.
</p>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Invite Code</th>
            <th>Email</th>
            <th>Issued</th>
            <th>Redeemed</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>2</td>
            <td><strong>hek3ns23</strong></td>
            <td class="red">joe@gmailsmith.com</td>
            <td>23 minutes and 5 seconds ago</td>
            <td>2 minutes and 5 seconds ago</td>
            <td>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td><strong>wen30cs</strong></td>
            <td class="red">joe@gmailsmith.com</td>
            <td>23 minutes and 5 seconds ago</td>
            <td>Never</td>
            <td>
                <a href="#" class="btn btn-small btn-danger">Revoke</a>
            </td>
        </tr>
        <tr>
            <td>8</td>
            <td><strong>wen30cs</strong></td>
            <td class="red" colspan="3">  <input type="text" placeholder="Enter an email"></td>
            <td>
                <a href="#" class="btn btn-small btn-primary">Issue Invitation</a>
            </td>
        </tr>
        <tr>
            <td>9</td>
            <td><strong>wen30cs</strong></td>
            <td class="red" colspan="3">  <input type="text" placeholder="Enter an email"></td>
            <td>
                <a href="#" class="btn btn-small btn-primary">Issue Invitation</a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="red center" colspan="5">You have no invitations remaining to extend.</td>
        </tr>

    </tbody>
</table>


<script type="text/javascript">

</script>
<?php 
include("includes/footer.php");
?>

    