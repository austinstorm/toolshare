<?php
include("includes/prepend.php");
include("includes/header.php");
echo $oUtil->getBreadcrumbs();

$aUsers = $oUtil->listUsers();
?>
<div class="page-header">
    <h2>
        Users
    </h2>
</div>
    <table class="table table-bordered table-striped">
            <thead>
                <tr>
                        <th>Name</th>
                        <th>Class</th>
                        <th>Last Seen</th>
                        <th>Joined</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($aUsers as $aUser){
                    $oUser = new user($aUser['id']);
                    ?>
                <tr>
                    <td>
                        <a href="<?=HTML_ROOT?>/profile/<?=$aUser['id']?>"><?=$aUser['first_name'].' '.$aUser['last_name']?></a>
                        <?php if ($oUser->isNew()) {?><span class="label label-success">New</span><?php }?>
                    </td>
                    <td>
                        <?php
                        $iUserType = $aUser['is_admin'];
                        switch ($iUserType){
                            case 1:
                                echo "Admin";
                                break;
                            case 0:
                                echo "User";
                                break;
                        }
                        ?>
                    </td>
                    <td>
                        <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aUser['access'])?>"><?=$aUser['access']?></abbr>
                    </td>     
                    <td>
                        <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aUser['join'])?>"><?=$aUser['join']?></abbr>
                    </td>  
                </tr>
                
                <?php
                }
                ?>
            </tbody>
    </table>
<?php
include("includes/footer.php");
?>