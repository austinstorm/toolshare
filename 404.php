<?php
include("includes/prepend.php");
include("includes/header.php");
?>

	<h1>404. <small>That's an error.</small></h1>

	<p>The requested page could not be found. Please leave a message in the forums and we'll try to fix it.</p>

	<p style="text-align:center;"><img src="includes/img/404.png"></p>

<?php
include("includes/footer.php");
?>
