<?php
include("includes/prepend.php");
include("includes/header.php");
echo $oUtil->getBreadcrumbs();
?>
<div class="page-header">
    <h2>Requests
        <button class="btn btn-primary" style="float: right; margin-top: 7px;">Create a Request</button>
    </h2>
</div>
<table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Title / Item</th>
                <th>Bounty</th>
                <th>Votes</th>
                <th><img src="http://bibliotik.org/static/icons/comments.png" alt="Comments"></th>
                <th>Filled</th>
                <th>Requester</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <img class="thumbnail" src="http://content.artofmanliness.com/uploads/2009/09/masons_hammer.jpg" height="45" alt="">
                </td>
                <td><strong><a href="view_request.php">Mason's Hammer</a></strong> (<a href="tag.html">Estwing</a>) <br>
                    <span class="label">hammer</span>
                    <span class="label">masonry</span>
                    <span class="label">bricklaying</span>
                    <span class="label">geology</span>
                </td>
                <td>15 points</td>
                <td>1</td>
                <td>0</td>
                <td>Never</td>
                <td><a href="user.html">austinstorm</a><br>14 hours ago</td>
            </tr>
            <tr>
                <td>
                    <img class="thumbnail" src="http://www.tedorland.com/books/images/art_fear.gif" height="45" alt="">
                </td>
                <td><strong><a href="view_request.php">Art &amp; Fear</a></strong> by <a href="tag.html">David Bayles</a>, <a href="tag.html">Ted Orland</a> (<a href="tag.html">Image Continuum Press</a>, 2004) <br>
                    <span class="label">art</span>
                    <span class="label">process</span>
                    <span class="label">inspiration</span>
                    <span class="label">creativity</span>



                </td>
                <td>25 points</td>
                <td>2</td>
                <td>1</td>
                <td>23 hours ago</td>
                <td><a href="user.html">austinstorm</a><br>1 week ago</td>
            </tr>
        </tbody>
    </table>

<?php
include("includes/footer.php");
?>
