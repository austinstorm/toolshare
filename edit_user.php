<?php
include("includes/prepend.php");
include("includes/header.php");
if($oUser) {
    echo $oUtil->getBreadcrumbs();
?>

<form id="user_form" class="form-horizontal" action="update_user.php">
    <div class="alert alert-error" style="display:none"></div>
    <fieldset>
    <legend>Edit a User</legend>
    <div class="control-group">
        <label class="control-label" for="first_name">First Name</label>
        <div class="controls">
        <input name="first_name" value="<?=$oUser->first_name?>" type="text" class="input-medium" id="first_name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="last_name">Last Name</label>
        <div class="controls">
        <input name="last_name" value="<?=$oUser->last_name?>" type="text" class="input-medium" id="last_name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="address1">Address 1</label>
        <div class="controls">
        <input name="address1" value="<?=$oUser->address1?>" type="text" class="input-large" id="address1">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="address2">Address 2</label>
        <div class="controls">
        <input name="address2" value="<?=$oUser->address2?>" type="text" class="input-large" id="address2">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="phone">Phone</label>
        <div class="controls">
        <input name="phone" value="<?=$oUser->phone?>" type="text" class="input-large" id="phone">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="email">Email Address</label>
        <div class="controls">
        <input name="email" value="<?=$oUser->email?>" type="text" class="input-large" id="email">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="password">Password</label>
        <div class="controls">
        <input name="password" value="<?=$oUser->password?>" type="password" class="input-large" id="password">
        </div>
    </div>
    <div class="control-group">
        <p>This site uses <a href="http://www.gravatar.com" target="blank">Gravatar</a> for your avatar. Head over there to set or change your avatar! Make sure to use the same email there that you used here.</p>
    </div>
        <div class="form-actions">
                <button type="submit" class="btn btn-primary">Edit User</button>
                <button class="btn">Cancel</button>
        </div>
    </fieldset>
    <input type="hidden" name="userid" value="<?=$oUser->userid?>">
</form>
<?php
}
else echo("Invalid ID");
include("includes/footer.php");
?>
<script type="text/javascript">
$(document).ready(function() {     
    
    $('#user_form').ajaxForm({beforeSubmit: validate}); 
    
    function validate(formData, jqForm, options) { 
    // jqForm is a jQuery object which wraps the form DOM element 
    // 
    // To validate, we can access the DOM elements directly and return true 
    // only if the values of both the username and password fields evaluate 
    // to true 
 
    var form = jqForm[0];
    if (!form.first_name.value) {
        addError('Please enter a value for First Name'); 
        return false; 
    } 
    if (!form.last_name.value) { 
        addError('Please enter a value for Last Name'); 
        return false; 
    }
    if (!form.address1.value) { 
        addError('Please enter a value for Address 1'); 
        return false; 
    }
    if (!form.address2.value) { 
        addError('Please enter a value for Address 2'); 
        return false; 
    }
    if (!form.phone.value) { 
        addError('Please enter a value for Phone'); 
        return false; 
    }
    if (!form.email.value) { 
        addError('Please enter a value for Email'); 
        return false; 
    }
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(form.email.value)) {
        addError('Please provide a valid email address');
        return false;
    }

    if (!form.password.value) { 
        addError('Please enter a value for Password'); 
        return false; 
    }
    return true;
    }
});

function addError(error) {    
    $('.alert').show().html(error);
    $(window).scrollTop(0);

}    


</script>