<?php
include("includes/prepend.php");
$iItemID = $_REQUEST['itemid'];
if (item::isValidID($iItemID)){        
    $oItem = new item($iItemID);
    $iReservationID = $oItem->getReservationID();
    if ($iReservationID){
        $oReservation = new Reservation($iReservationID);
        $iUserID = $oReservation->userid;
        $oUser = new User($iUserID);
        $strUser = $oUser->id.'. '.$oUser->first_name.' '.$oUser->last_name;
    }
    else $strUser = '';
}
echo json_encode(array('name' => $oItem->title, 'user' => $strUser));
?>
