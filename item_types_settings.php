<?php
include("includes/prepend.php");
include("includes/header.php");
if (!$is_admin){
?>
<div class="page-header">
    <h2>Permissions Error</h2>
</div>
    <p>You must be logged in as an admin to view this page. If you think you have received this message in error, please leave a message in the forums.</p>
<?php 
} else {
    echo $oUtil->getBreadcrumbs();
    $aItemTypes = $oUtil->getItemTypes();
?>

<div class="page-header">
    <h2>Item Types</h2>
    <h3><a href="new_item_type.php">Add Item Type</a></h3>
</div>  
<div id="item_types">
    <ul>
        <?php
            foreach($aItemTypes as $aItemType){
                $aFields = $oUtil->getItemTypeFields($aItemType['ItemTypeID']);
                ?>
        <li>
            <?php 
            echo $aItemType['Name']            
            ?>            
            <ul>
                <li>
                    Description: <?=$aItemType['Description']?>
                </li>
                <li>
                    Checkout Period: <?=$aItemType['CheckoutPeriod']?>
                </li>
                <li>Fields:
                    <ul>
                        <?php
                        foreach($aFields as $aField){
                        ?>
                        <li>
                            <?=$aField['DisplayName']?>
                            <ul>
                                <li>
                                    Input Type: <?=$aField['InputType']?>
                                </li>
                                <?php
                                if ($aField['InputType'] == 'dropdown'){
                                    ?>
                                <li>
                                    Options: <?=$aField['Options']?>
                                </li>
                                <?php
                                }
                                ?>
                                <li>
                                    Help Text: <?= $aField['HelpText'] ? $aField['HelpText'] : 'None'?>
                                </li>
                            </ul>
                        </li>
                        <?php
                        }
                        ?>
                    </ul>
                </li>
            </ul>
        </li>
        <li><a href="edit_item_type.php?item_type=<?=$aItemType['ItemTypeID']?>">Edit <?=$aItemType['Name']?></a></li>
        </br>
        <?php
            }
        ?>
    </ul>
</div>

<?php }
include("includes/footer.php");
?>