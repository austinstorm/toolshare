<?php

/*
 * This is the comment form, should be included on pages that require comments.
 * Gets the type and ID from $_REQUEST and retrieves comments.
 * If logged in you can also add a comment
 */
$aComments = $oUtil->getComments($_REQUEST);
?>
<?php
if (count($aComments)){
foreach($aComments as $aComment) {
    $oCommenter = new User($aComment['userid']);
?> 
<div class="row-fluid comments">
    <div class="span1">
                <?php
                $grav_email = $oCommenter->email;
                $default = $website."/images/anonuser_50px.gif";
                $gravatar = new Gravatar($grav_email, $default);
                $gravatar->size = 50;
                ?>
                <img src="<?php echo $gravatar->getSrc(); ?>">
            </div>
    <div class="span11">
        <strong style="font-size: 1.1em;"><a href="view_user.php?userid=<?=$oCommenter->id?>" class="url"><?=$oCommenter->first_name. ' '.$oCommenter->last_name?></a></strong>
        <p class="meta">
        <small><i class="icon-time"></i> 
            <abbr class="timeago" title="<?=$oUtil->timeagoFormat($aComment['added'])?>"><?=$aComment['added']?></abbr>
            <i class="icon-upload"></i> 
              <?php
              if ($aComment['topic_created'] == 1){
                  echo '+10 Points';
              }
              else echo '+5 Points';
              ?>
        </small>
        </p>
        <p><?=Markdown($aComment['comment'])?></p>
    </div>
</div>
<div class="row-fluid comments">
    <div class="span12">
        <hr class="tablerow comments">
    </div>
</div>

<?php } }
else {
    ?>
    <p class=""><strong>No comments yet</strong></p>

    <script>
      $(function () {
        $('#itemFeeds a:first').tab('show'); // Select first tab
      })
    </script>

<?php }
?>

<?php
if ($sesslife){
    if (isset($_REQUEST['itemid'])){
        $assignedtotable = 'item';
        $assignedtoid = $_REQUEST['itemid'];
    }
    if (isset($_REQUEST['topicID'])){
        $assignedtotable = 'post';
        $assignedtoid = $_REQUEST['topicID'];
    }
?>
<form class="" id="comment_form" action="add_comment.php" style="background: #f5f5f5; border: 1px solid #E3E3E3; padding-top: 10px;">
    <div class="row-fluid">
        <div class="span1">
        </div>
        <div class="span9">
            <textarea class="span12" rows="3" name="comment"></textarea>
            <input type="hidden" name="assignedtotable" value="<?=$assignedtotable?>"/>
            <input type="hidden" name="assignedtoid" value="<?=$assignedtoid?>" />
            <input type="hidden" name="userid" value="<?=$userid?>" />
                
        </div><!-- /.span9 -->
        <div class="span1">
            <input class="btn" name="submit" type="submit" value="Comment"/>
        </div>
    </div><!-- /.row-fluid -->
</form>
<?php    
}
else {
    ?>
    <form class="" id="comment_form" action="add_comment.php" style="background: #f5f5f5; border: 1px solid #E3E3E3; padding-top: 10px;">
        <p class="center">You must be <a href="<?php echo $website."/".USER_DIRECTORY; ?>/login">logged in</a> to comment.</p>
    </form>
<?php
}
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#comment_form').ajaxForm({
            dataType: 'json',
            success: processResult
        });
        
        function processResult(data) {
            if (data.error){
                addError(data.error);
            }
            if (data.commentid) {
                window.location.reload()
            }
        }
        
        function addError(error) {    
            $('.alert').show().html(error);
            $(window).scrollTop(0);

        }   
        
    });
</script>