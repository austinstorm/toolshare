<?php
include("includes/prepend.php");
include("includes/header.php");
if (!$is_admin){
?>
<div class="page-header">
    <h2>Permissions Error</h2>
</div>
    <p>You must be logged in as an admin to view this page. If you think you have received this message in error, please leave a message in the forums.</p>
<?php 
} else {
echo $oUtil->getBreadcrumbs();
?>

<div class="accordion" id="accordion2">
<div class="accordion-group">
  <div class="accordion-heading">
    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
      Circulation
    </a>
  </div>
  <div id="collapseThree" class="accordion-body in collapse" style="height: auto; ">
    <div class="accordion-inner">
        <div class="userinfo row">
          <div class="span4">
            <form>
            <legend>Choose a Member</legend>
            <fieldset>
                <div class="control-group">
                    <div class="controls input-append">
                        <input name="userid" type="text" class="input-large" id="member"><button class="btn" type="button">Search</button>
                          <span class="help-block">Member name or number.</span>
                    </div>
                </div>
            </fieldset>
            </form>
          </div>
          <div class="span2 center">
            <br><p>or...</p>
          </div>
          <div class="span4">
            <form>
            <legend>Scan an Item</legend>
            <fieldset>
                <div class="control-group">
                    <div class="controls input-append">
                        <input name="itemid" type="text" class="input-large" id="item"><button class="btn" type="button">Search</button>
                          <span class="help-block">Scan an already checked-out item.</span>
                    </div>
                </div>
            </fieldset>
            </form>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="accordion-group">
  <div class="accordion-heading">
    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
        Verify User: <span class="member_name"></span>
    </a>
  </div>
  <div id="collapseOne" class="accordion-body collapse" style="height: 0px; ">
    <div class="accordion-inner">
        <h3><span class="member_name"></span> <small>joined <span class="join"></span></small></h3>
        <span class="fine_info"></span>
        <span class="fine_detail_link"></span>
        <div id="fine_details"></div>
        <div id="user_agreement_form"></div>
        <p>Other stuff I'm sure we'll want here.</p>
        <a data-toggle="collapse" data-parent="#accordion2" class="btn btn-info" href="#collapseTwo">Next Tab</a>
    </div>
  </div>
</div>
<div class="accordion-group">
  <div class="accordion-heading">
    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
        Check-in <span class="item_count"></span> / Check-out
    </a>
  </div>
  <div id="collapseTwo" class="accordion-body collapse" style="height: 0px; ">
    <div class="accordion-inner">
        <h3>Check-in Items</h3>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Item ID</th>
                    <th>Name</th>
                    <th>Due</th>
                    <th>Renewed</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="checkin_table">
                
            </tbody>
        </table>

        <h3>Check-out Items</h3>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Item ID</th>
                    <th>Name</th>
                    <th>Due</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input class="input-mini checkout_item" type="text" placeholder="" name="itemid[]"></td>
                    <td class="name"></td>
                    <td class="due"></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <button class="btn btn-large btn-block btn-primary" id="checkout_items" type="button">Check-out Items</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
  </div>
</div>
<div class="accordion-group">
  <div class="accordion-heading">
    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
      Circulation Receipt
    </a>
  </div>
  <div id="collapseFour" class="accordion-body collapse" style="height: 0px; ">
    <div class="accordion-inner">
        <h3>John Smith <small>joined 2 days, 16 hours ago</small></h3>
        <p>Other stuff I'm sure we'll want here.</p>
    </div>
  </div>
</div>

</div>
<?php }?>

<?php
include("includes/footer.php");
?>
<script type="text/javascript">
$(document).ready(function() {

    $('#admin').addClass("active");
    
    $('#member').focus();
    
    
    
    $('#checkout_form').ajaxForm({
        beforeSubmit: validate,
        success: function(){window.location.reload()}
    }); 
    

    
    function validate(formData, jqForm, options) { 
    // jqForm is a jQuery object which wraps the form DOM element 
    // 
    // To validate, we can access the DOM elements directly and return true 
    // only if the values of both the username and password fields evaluate 
    // to true 
 
    var form = jqForm[0];
    if (!form.userid.value) {
        addError('Please enter a value for User ID'); 
        return false; 
    }
    return true;
    }

    function addError(error) {    
        $('.alert').show().html(error);
        $(window).scrollTop(0);

    }
});    
    $("#member").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            e.preventDefault();
            getInfo();                     
        }
    });
    
    $("#item").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            e.preventDefault();
            getInfo();                     
        }
    });
    
    $(".checkout_item").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            e.preventDefault();
            $(this).parents('tr').attr('checkout_itemid', $(this).val());
            var $clone = $(".checkout_item:last").parents('tr').clone(true);
            $('.checkout_item:last').parents('tr').after($clone);
            $('.checkout_item:last').parents('tr').removeAttr('checkout_itemid');
            $('.checkout_item:last').val('');
            $('.checkout_item:last').focus();
            getItemInfo($(this).val());
        }
    });
    
    function renew(itemid) {
        $.ajax({
                type: "POST",
                url: 'renew_item.php',
                dataType: 'json',
                data: 
                    {
                    itemid: itemid
                    },
                success: function(res) {
                    if (res.success == 'success'){
                        $('#renews').text(res.renews);
                        $('#due_date').text(res.due_date);
                    }
                }    
            });
    };
    
    function checkIn(itemid) {
        $.ajax({
                type: "POST",
                url: 'checkin_item.php',
                dataType: 'json',
                data: 
                    {
                    itemid: itemid
                    },
                success: function(res) {
                    if (res.success == 'success'){
                        $('#' + itemid).hide();
                        var number = $('.item_count').text()
                        number = parseInt(number.substr(1));
                        var new_number = number - 1;
                        $('.item_count').text('('+new_number+')')
                    }
                }    
            });
    };
    
        function getInfo() {
        var userid = $('#member').val();
        var itemid = $('#item').val();
        if (userid > 0){
            $.ajax({
                type: "POST",
                url: 'get_circulation_info.php',
                dataType: 'json',
                data: 
                    {
                    userid: userid
                    },
                success: function(res) {
                    if (res){
                        fillInfo(res);
                    }
                }    
            });
        }
        else {
            $.ajax({
                type: "POST",
                url: 'get_circulation_info.php',
                dataType: 'json',
                data: 
                    {
                    itemid: itemid
                    },
                success: function(res) {
                    if (res){
                        fillInfo(res);
                    }
                }    
            });
        }
    }
    
    function fillInfo(aInfo) {
        var aUser = aInfo.aUser;
        var aCheckouts = aInfo.aCheckouts;
        var aFineInfo = aInfo.aFineInfo;
        $('.member_name').text(aUser.first_name + ' ' + aUser.last_name);
        $('.fine_info').text('Total fines: $' + aFineInfo.totalFines + ' Total paid: $' + aFineInfo.totalPaid + ' Total forgiven: $' + aFineInfo.totalForgiven + ' Total outstanding: $' + aFineInfo.outstanding);
        $('.fine_detail_link').html('<a href="#" class="show_fine_detail" userid="' + aUser.id + '">Show Details</a>');
        $('.show_fine_detail').click(function(){
            $('#fine_details').load('fine_details.php?userid='+aUser.id);
        })
        $('.join').text(aUser.join);
        $('.item_count').text('(' + aCheckouts.length + ')');
        var checkin_table = '';
        $.each(aCheckouts, function(){
            checkin_table = checkin_table + '<tr id='+ this.itemid +'>'
                    + '<td>' + this.itemid+'</td>'
                    + '<td><strong>' + this.title + '</strong> (part1, part2)</td>'
                    + '<td id="due_date" class="warning">' + this.due + '</td>'
                    + '<td id="renews">'+ this.renews +'</td>'
                    + '<td>'
                    +    '<button itemid="' + this.itemid + '" class="btn btn-primary btn-small checkin_button" type="button">Check In</button>';
                if (!this.reservationid){
                    checkin_table= checkin_table + '<button itemid="' + this.itemid + '" class="btn btn-warning btn-small renew_button" type="button">Renew</button>';
                }
                    checkin_table = checkin_table + '</td>'
                + '</tr>';
        })
        $('.checkin_table').html(checkin_table);
        
        $(".checkin_button").click(function(){
            checkIn($(this).attr('itemid'));
        });
        
        $(".renew_button").click(function(){
            renew($(this).attr('itemid'));
        });
        
        $('#collapseOne').collapse({
            show: true
        })
        $('#collapseThree').collapse({
            hide: true
        })
        
        if (aUser.agreement_signed !== '1'){
            $('a[href="#collapseTwo"]').addClass("disabled disabled_collapse");
            $('.disabled_collapse').attr('href', '#');
            
            $('#user_agreement_form').html(
                '<span id="agreement_status">This user has not signed the membership agreement:</span>' +
                '<form id="user_agreement">Photo ID Type<input type="text" name="photo_id_type"></br>ID Number<input type="text" name="id_number"></br>Address Proof<input type="text" name="address_proof"></br>Agreement Signed<input type="checkbox" name="agreement_signed" value="1"></br><input name="submit" type="submit" value="Submit"><input type="hidden" name="userid" value="'+aUser.id+'"></form>'
            );
                
            $('#user_agreement').submit(function(e){
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: 'sign_user_agreement.php',
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function(res) {
                        if (res.success == "success"){
                            $('.disabled_collapse').attr("href", "#collapseTwo").removeClass("disabled");
                            $('#agreement_status').text('User agreement signed.');
                            $('#user_agreement').hide();
                        }
                    }    
               });
                
            }) 
        }
    }
    
    function getItemInfo(itemid){
        $.ajax({
                type: "POST",
                url: 'get_item_info.php',
                dataType: 'json',
                data: 
                    {
                    itemid: itemid
                    },
                success: function(res) {
                    if (res){
                        $('[checkout_itemid = ' + itemid + ']').children('.name').text(res.name)
                        console.log(res.user);
                        if (res.user !== ''){
                            $('[checkout_itemid = ' + itemid + ']').find('.checkout_item').after('<span style="color: red">This item is reserved by: ' + res.user + '</span>')
                        }
                    }
                }    
            });
    }
    
    
    $('#checkout_items').click(function(){
        var strData = '';
        $('.checkout_item').each(function(){
            strData = strData + 'itemids[]=' + $(this).val() + '&';            
        })
        strData = strData + 'userid=' + $('#member').val(); 
        $.ajax({
                type: "POST",
                url: 'add_checkout.php',
                dataType: 'json',
                data: strData,
                success: function(res) {
                    if (res.success == 'success'){
                        document.location.reload(true);                        
                    }
                }    
            });
    });

    
</script>
